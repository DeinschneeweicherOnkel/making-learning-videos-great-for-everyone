import math
import os
import sys
import json
import argparse
import copy
import cv2
import difflib
import numpy as np

# boxes can be generated with pytesseract if a trained dataset is available.
# NOT SUPPORTED ANY LONGER!!! if you want to use it, just change the TEXT_MODE to "with_pytesseract" and
# also take a look at the get_box_data() function
# import pytesseract
# from pytesseract import Output
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
TEXT_MODE = "without_pytesseract"

VIDEO = None
VIDEO_WIDTH = None
VIDEO_HEIGHT = None

# time at which an action has to end
PAUSE_IN_SECONDS = 10

VIDEO_FPS = None
INTERVAL = None

# stores already seen lines, used for e.g. reveal-actions
KNOWN_LINES_CACHE = [0]

TIME_ACCURACY = None

USER_ARGS = []
VIDEO_START_TIME = 0
VIDEO_END_TIME = None

CROP = False
CROP_AREA_LEFT = 0
CROP_AREA_RIGHT = 0
CROP_AREA_TOP = 0
CROP_AREA_BOTTOM = 0

BACKGROUND_COLOR = None
WHITE_BACKGROUND = None

REF_IMAGES_FOLDER = None
REF_IMAGES = []

#####################################################################################
#####################################################################################
# GRAPHICAL FUNCTIONS
#####################################################################################
#####################################################################################

# stores video-indices and its corresponding frames.
# original -> the frame was not modified at all.
# for_text -> optimized for text-recognition
FRAME_CACHE_ORIGINAL = {}
FRAME_CACHE_FOR_TEXT = {}


# returns frame at certain position of a video and saves it into cache.
# index specifies the index of the frame in the video.
# mode can be either "text/box" or "select". "text/box" operates on a modified picture with a certain threshold,
# "select" operates on the original colors
def get_frame(index, mode="text/box"):
    frame = None
    if index not in FRAME_CACHE_ORIGINAL:
        VIDEO.set(cv2.CAP_PROP_POS_FRAMES, index)
        ret, frame = VIDEO.read()

        if not ret:
            return None

        if CROP:
            frame = frame[
                CROP_AREA_TOP: VIDEO_HEIGHT - CROP_AREA_BOTTOM,
                CROP_AREA_LEFT: VIDEO_WIDTH - CROP_AREA_RIGHT,
            ]

        # checks whether frame is a webcam snippet by testing 4 samples in each corner of the screen
        # -> samples has to have equal color (current used threshold for equality is > 4)
        if (
            BACKGROUND_COLOR is not None
            and (
                abs(int(frame[10, 0, 0]) - BACKGROUND_COLOR[0]) > 4
                or abs(int(frame[10, 0, 1]) - BACKGROUND_COLOR[1]) > 4
                or abs(int(frame[10, 0, 2]) - BACKGROUND_COLOR[2]) > 4
            )
        ) or (
            BACKGROUND_COLOR is None
            and (
                abs(
                    int(frame[10, 0, 0]) - frame[VIDEO_HEIGHT - 10, VIDEO_WIDTH - 20, 0]
                )
                > 4
                or abs(
                    int(frame[10, 0, 1]) - frame[VIDEO_HEIGHT - 10, VIDEO_WIDTH - 20, 1]
                )
                > 4
                or abs(
                    int(frame[10, 0, 2]) - frame[VIDEO_HEIGHT - 10, VIDEO_WIDTH - 20, 2]
                )
                > 4
                or abs(int(frame[10, 0, 0]) - frame[10, VIDEO_WIDTH - 20, 0]) > 4
                or abs(int(frame[10, 0, 1]) - frame[10, VIDEO_WIDTH - 20, 1]) > 4
                or abs(int(frame[10, 0, 2]) - frame[10, VIDEO_WIDTH - 20, 2]) > 4
                or abs(int(frame[10, 0, 0]) - frame[VIDEO_HEIGHT - 10, 10, 0]) > 4
                or abs(int(frame[10, 0, 1]) - frame[VIDEO_HEIGHT - 10, 10, 1]) > 4
                or abs(int(frame[10, 0, 2]) - frame[VIDEO_HEIGHT - 10, 10, 2]) > 4
            )
        ):
            frame = None
        # for very intensive colors ones can apply this in order to not threshold them
        # frame[np.where((frame >= [0, 0, 220]).all(axis=2))] = [255, 255, 255]
        # frame[np.where((frame >= [0, 220, 0]).all(axis=2))] = [255, 255, 255]
        # frame[np.where((frame >= [220, 0, 0]).all(axis=2))] = [255, 255, 255]
        if frame is not None:
            FRAME_CACHE_ORIGINAL[index] = frame.copy()
        else:
            FRAME_CACHE_ORIGINAL[index] = None

    if FRAME_CACHE_ORIGINAL[index] is None:
        return None
    if mode == "select":
        return FRAME_CACHE_ORIGINAL[index].copy()

    # https://nanonets.com/blog/ocr-with-tesseract/
    else:
        if index in FRAME_CACHE_FOR_TEXT:
            return FRAME_CACHE_FOR_TEXT[index].copy()

        if frame is None:
            frame = FRAME_CACHE_ORIGINAL[index].copy()
        frame[np.where((frame >= [0, 0, 195]).all(axis=2))] = [255, 255, 255]
        frame[np.all(frame < 100, axis=2)] = [0, 0, 0]
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # make frame binary grayscale frame
        _, frame = cv2.threshold(frame, 100, 255, cv2.THRESH_BINARY)

        # blurr frame in order to get rid of artifacts (e.g. scrollbar)
        frame = cv2.medianBlur(frame, 5)

        # save frame into cache for optimized text-recognition
        FRAME_CACHE_FOR_TEXT[index] = frame.copy()

    """if DEBUG_FLAG:
        debug_function(frame)"""

    return frame.copy()


# finds potential chars on frame and saves information about their locations
def get_char_positions(frame_ori):
    frame = frame_ori.copy()
    contours, hierarchy = cv2.findContours(
        frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )
    char_positions = []
    # first get basic information of the positions with the help of contours
    for c in contours:
        widths = []
        heights = []
        for cc in c:
            widths.append(cc[0][0])
            heights.append(cc[0][1])

        left = min(widths)
        right = max(widths)
        top = min(heights)
        bottom = max(heights)

        char_positions.append(
            {"left": left, "bottom": bottom, "right": right, "top": top}
        )

    char_positions = sorted(char_positions, key=lambda x: x["left"])

    # delete chars which are cropped either left or right by the screen
    i = 0
    while True:
        if i >= len(char_positions):
            break
        if char_positions[i]["left"] <= 0 or char_positions[i]["right"] >= VIDEO_WIDTH:
            char_positions.remove(char_positions[i])
        else:
            i += 1

    """if DEBUG_FLAG:
        debug_function(frame_ori, char_positions)"""

    # get mean height of all chars to approximate the font-size in current frame
    offset = None
    # width_mean_of_all_boxes = 0
    height_mean_of_all_boxes = 0
    for cp in char_positions:
        # width_mean_of_all_boxes += cp["right"] - cp["left"]
        height_mean_of_all_boxes += cp["bottom"] - cp["top"]

    # width_mean_of_all_boxes /= len(char_positions)
    height_mean_of_all_boxes /= len(char_positions) * 2

    # determine how many lines of text the current frame has and where they are
    line_dimensions = []
    for cp in char_positions:
        new_line_dimension = True
        if len(line_dimensions) == 0:
            line_dimensions.append(cp)
            offset = height_mean_of_all_boxes
            continue
        for ld in line_dimensions:
            if (
                cp["bottom"] < ld["bottom"] + offset
                and cp["top"] + offset > ld["top"]
                or cp["top"] < ld["bottom"] + offset
                and cp["bottom"] + offset > ld["top"]
            ):
                new_line_dimension = False
                break
        if new_line_dimension:
            line_dimensions.append(cp)

    """if DEBUG_FLAG:
        debug_function(frame_ori, line_dimensions)"""

    # stretch top and bottom of line-numbers a little bit
    offset = line_dimensions[0]["bottom"] - line_dimensions[0]["top"]
    for ld in line_dimensions:
        ld["top"] = (
            ld["top"] - int(offset / 4) if ld["top"] - int(offset / 4) >= 0 else 0
        )
        ld["bottom"] = (
            ld["bottom"] + int(offset / 4) if ld["bottom"] + int(offset / 4) >= 0 else 0
        )
    line_dimensions = sorted(line_dimensions, key=lambda x: x["top"])

    """if DEBUG_FLAG:
        debug_function(frame_ori, line_dimensions)"""

    # for every char assign corresponding line (beginning at zero)
    for cp in char_positions:
        current_li_index = 0
        current_dif = None
        cp["rowStart"] = None
        for ld in line_dimensions:
            if (
                current_dif is None
                or abs(cp["top"] - ld["top"]) + abs(cp["bottom"] - ld["bottom"])
                < current_dif
            ):
                cp["rowStart"] = current_li_index
                current_dif = abs(cp["top"] - ld["top"]) * 1.5
            current_li_index += 1

    char_positions = sorted(char_positions, key=lambda x: (x["rowStart"], x["left"]))

    """if DEBUG_FLAG:
        debug_function(frame_ori, char_positions)"""

    # for every line determine its height
    min_top = {}
    max_bottom = {}
    for c in char_positions:
        if c["rowStart"] not in min_top or min_top[c["rowStart"]] > c["top"]:
            min_top[c["rowStart"]] = c["top"]
        if c["rowStart"] not in max_bottom or max_bottom[c["rowStart"]] < c["bottom"]:
            max_bottom[c["rowStart"]] = c["bottom"]

    # for every char assign height of corresponding line
    for c in char_positions:
        c["top"] = min_top[c["rowStart"]]
        c["bottom"] = max_bottom[c["rowStart"]]

    """if DEBUG_FLAG:
        debug_function(frame_ori, char_positions)"""

    # merge contours if necessary. E.g. the dot of the i with the i itself,
    # by now the dot an own character and would be interpreted as a . probably
    c_index = 0
    while True:
        if c_index >= len(char_positions):
            break
        c = char_positions[c_index]
        cc_index = 0

        while True:
            if cc_index >= len(char_positions):
                break
            cc = char_positions[cc_index]
            if (
                c_index != cc_index
                and c["rowStart"] == cc["rowStart"]
                and (
                    c["right"] < cc["right"]
                    and c["right"] > cc["left"]
                    or c["left"] < cc["right"]
                    and c["right"] > cc["left"]
                )
            ):
                cc["right"] = max(c["right"], cc["right"])
                cc["left"] = min(c["left"], cc["left"])
                cc["top"] = min(c["top"], cc["top"])
                cc["bottom"] = max(c["bottom"], cc["bottom"])
                del char_positions[c_index]
                c_index -= 1
                break
            cc_index += 1
        c_index += 1

    """if DEBUG_FLAG:
        debug_function(frame_ori, char_positions)"""

    # look at the contours of the current chars and get their real dimension.
    # (by now all chars of the same line has the same dimension)
    for ch in char_positions:
        contours, hierarchy = cv2.findContours(
            frame[ch["top"]: ch["bottom"], ch["left"]: ch["right"]].copy(),
            cv2.RETR_TREE,
            cv2.CHAIN_APPROX_SIMPLE,
        )

        width = []
        height = []

        for c in contours:
            for cc in c:
                width.append(cc[0][0])
                height.append(cc[0][1])

        if not height or not width:
            char_positions.remove(ch)
            continue
        top = min(height)
        bottom = max(height)
        ch["right"] = max(ch["right"] + 1, 0)
        ch["left"] = max(ch["left"], 0)
        ch["top"] = max(ch["top"] + top, 0)
        ch["bottom"] = max(ch["top"] + bottom - top + 1, 0)

    """if DEBUG_FLAG:
        debug_function(frame_ori, char_positions)"""

    # remove cut line top
    ref_line = None
    for ch in char_positions:
        if ch["rowStart"] == 1:
            ref_line = ch
            break

    if (
        ref_line
        and abs(
            (char_positions[0]["bottom"] - char_positions[0]["top"])
            - (ref_line["bottom"] - ref_line["top"])
        )
        > (ref_line["bottom"] - ref_line["top"]) / 3
        or char_positions[0]["top"] <= 5
    ):
        while True:
            if char_positions[0]["rowStart"] == 0:
                char_positions.remove(char_positions[0])
                continue
            else:
                break

    # remove cut line bottom
    ref_line = None
    start_line = None
    last_line = char_positions[-1]["rowStart"]
    ch_i = len(char_positions) - 1

    while True:
        if ch_i < 0:
            break
        if not start_line and char_positions[ch_i]["rowStart"] == last_line - 1:
            start_line = char_positions[ch_i + 1]
        elif char_positions[ch_i]["rowStart"] == last_line - 2:
            ref_line = char_positions[ch_i + 1]
            break
        ch_i -= 1

    if (
        ref_line
        and abs(
            (start_line["bottom"] - start_line["top"])
            - (ref_line["bottom"] - ref_line["top"])
        )
        > (ref_line["bottom"] - ref_line["top"]) / 3
        or char_positions[-1]["bottom"] >= VIDEO_HEIGHT - 5
    ):
        char_positions.reverse()
        i = 0
        while True:
            if char_positions[i]["rowStart"] == last_line:
                char_positions[i]["remove"] = True
            else:
                break
            i += 1
        char_positions.reverse()

    """if DEBUG_FLAG:
        debug_function(frame_ori, char_positions)"""

    return char_positions


# with the graphical information of (potential) chars, determine its corresponding letter/number/symbol
def get_boxes_graphical(video_index):
    frame = get_frame(video_index)

    char_positions = get_char_positions(frame)

    finished_box_data = []

    for b in char_positions:
        current_char_frame = frame[
            b["top"]: b["bottom"], b["left"]: b["right"]
        ].copy()
        current_white_pixels = None
        current_char = None

        for ref_image in REF_IMAGES:
            ref = copy.deepcopy(ref_image["image"].copy())
            filename = ref_image["name"]

            # check whether aspect ratio between reference and current char could be match
            tolerance1 = min(ref.shape[0], current_char_frame.shape[0]) * 0.18
            tolerance2 = min(ref.shape[1], current_char_frame.shape[1]) * 0.18
            factor = ref.shape[0] / current_char_frame.shape[0]
            if (
                abs(current_char_frame.shape[0] * factor - ref.shape[0]) > tolerance1
                or abs(current_char_frame.shape[1] * factor - ref.shape[1]) > tolerance2
            ):
                continue

            # if aspect ratio matches, enlarge the smaller image to the size of the greater image
            if ref.shape[0] > current_char_frame.shape[0]:
                bigger_element = copy.deepcopy(ref)
                smaller_element = copy.deepcopy(current_char_frame)
            else:
                bigger_element = copy.deepcopy(current_char_frame)
                smaller_element = copy.deepcopy(ref)

            resized_element = cv2.resize(
                smaller_element, (bigger_element.shape[1], bigger_element.shape[0])
            )

            # bitwise both images and count white pixels (= pixels which do not overlap)
            white_pixels = cv2.countNonZero(
                cv2.bitwise_xor(resized_element, bigger_element)
            )
            # find image with the smallest amount of white pixels
            if current_white_pixels is None or current_white_pixels > white_pixels:
                current_white_pixels = white_pixels
                current_char = filename

        finished_box_data.append(
            {
                "char": current_char,
                "left": b["left"],
                "bottom": b["bottom"],
                "right": b["right"],
                "top": b["top"],
                "rowStart": b["rowStart"],
            }
        )

        # if char is considered to be removed in the future, mark them.
        # the reason for not removing them immediately is that they may contain relevant meta-info
        if "remove" in b:
            finished_box_data[-1]["remove"] = True

        """if DEBUG_FLAG:
            print("Found char", current_char, "\n\n")
            cv2.rectangle(frame, (b["left"], b["top"]), (b["right"], b["bottom"]), (255, 0, 0), 1)
            cv2.imshow("", frame)
            cv2.imshow("", current_char_frame)
            cv2.waitKey(0)"""

    return finished_box_data


# returns mean color of certain box.
# graphical information of the box can be accessed with the video_index and the box-information (dimensions)
def get_color_of_box(video_index, box):
    frame = get_frame(video_index)[
        box["top"]: box["bottom"], box["left"]: box["right"]
    ]

    contours, hierarchy = cv2.findContours(
        frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )

    mask = np.zeros(frame.shape, np.uint8)
    cv2.drawContours(mask, contours, -1, 255, -1)

    color_frame = get_frame(video_index, "select")[
        box["top"]: box["bottom"], box["left"]: box["right"]
    ]
    mean = cv2.mean(color_frame, mask=mask)

    return mean


# the distance from line-numbers to text very often differs to the size of a common whitespace in text.
# Because of this, whitespaces between line-numbers and
# text would get estimated wrong and this function tries to correct that.
# The idea is to simple increase the width of the line-numbers-box such that
# whitespaces are not estimated wrong any-longer
def separate_lines_from_text(index, boxes, pivot):
    b_i = 0
    ref_color = None
    ref_line = None
    current_max = None
    indices = []
    while True:
        if b_i >= len(boxes):
            break
        if "rowStart" not in boxes[b_i]:
            b_i += 1
            continue
        if ref_line is None:
            # all digits of a line-number has to have equal color.
            # determine them here to compare future chars with it
            ref_color = get_color_of_box(index, boxes[b_i])
            ref_line = boxes[b_i]["rowStart"]
            b_i += 1
            continue

        cur = get_color_of_box(index, boxes[b_i])
        if (
            "remove" not in boxes[b_i]
            and boxes[b_i]["char"].isdigit()
            and boxes[b_i]["rowStart"] == ref_line
            and abs(ref_color[0] - cur[0]) < 10
            and abs(ref_color[1] - cur[1]) < 10
            and abs(ref_color[2] - cur[2]) < 10
        ):
            b_i += 1
        else:
            if not current_max or boxes[b_i - 1]["right"] > current_max:
                current_max = boxes[b_i - 1]["right"]
            indices.append(b_i - 1)
            b_i -= 1
            while b_i < len(boxes) and (
                "rowStart" not in boxes[b_i] or ref_line == boxes[b_i]["rowStart"]
            ):
                b_i += 1
            ref_line = None

    # increase the width of the last digit of each line-number
    for i in indices:
        factor = 3
        offset = int(pivot["width"] / factor)
        if i + 1 < len(boxes) and boxes[i]["rowStart"] == boxes[i + 1]["rowStart"]:
            while boxes[i + 1]["left"] < current_max + offset + 4 and offset > 0:
                factor += 0.1
                offset = int(pivot["width"] / factor)
        if offset > 0:
            boxes[i]["right"] = current_max + offset
        else:
            boxes[i]["right"] = boxes[i]["right"] + int(pivot["width"] / 3)

    """if DEBUG_FLAG:
        debug_function(get_frame(index, "select"), boxes)"""

    return boxes


# flag to enable logic for adding newline at end of text if Linecache contains lines afterwards.
# by default disabled since it is faulty (because of how the KNOWN_LINES_CACHE works)
CHECK_LAST_NEWLINE = False

# stores video-indices and its corresponding box-data.
# blank -> blank box-data-information without whitespaces, newlines and further modifications.
# default -> modified-box-data with whitespaces, newlines and further modifications
BOX_DATA_CACHE_BLANK = {}
BOX_DATA_CACHE_DEFAULT = {}


# saves information to the cache and returns finished box-data with the information of:
# - where the chars in the frame (of certain video_index) are.
# - which chars these are.
# blank_box_data returns the box data without whitespaces and newlines.
# remove_line_number specifies whether the line-numbers should be removed from the boxes when returning them
def get_box_data(video_index, blank_box_data=False, remove_line_numbers=False):
    if video_index not in BOX_DATA_CACHE_BLANK:
        frame = get_frame(video_index)

        if frame is None:
            BOX_DATA_CACHE_BLANK[video_index] = None
            BOX_DATA_CACHE_DEFAULT[video_index] = None
            return None

        # TRAINED WITH
        # https://www.youtube.com/watch?v=V2chutR7RZo
        # https://colab.research.google.com/github/AniqueManiac/new-font-training-with-tesseract-in-google-colab/blob/main/TrainAnewFontWithTesseract.ipynb#scrollTo=1wPmBN9IM7Fx

        # boxes can be generated with pytesseract if a trained dataset is available.
        # NOT SUPPORTED ANY LONGER!!! if you want to use it, just change the TEXT_MODE to "with_pytesseract" and
        # also define the path to pytesseract at the beginning of this file (its commented out)
        if TEXT_MODE == "with_pytesseract":
            custom_config_text = (
                r"--oem 3 --psm 6" '--tessdata-dir "/usr/share/tessdata/"'
            )
            box_data = pytesseract.image_to_boxes(
                frame, lang="roboto_mono", config=custom_config_text
            )
            boxes = []
            for box in box_data.splitlines():
                boxes.append(
                    {
                        "char": box.split(" ")[0],
                        "left": int(box.split(" ")[1]),
                        "bottom": 1620 - int(box.split(" ")[2]),
                        "right": int(box.split(" ")[3]),
                        "top": 1620 - int(box.split(" ")[4]),
                    }
                )
                """cv2.rectangle(frame, (boxes[-1]["left"], boxes[-1]["top"]),
                              (boxes[-1]["right"], boxes[-1]["bottom"]), (255, 255, 255), 1)"""
        else:
            boxes = get_boxes_graphical(video_index)

            """if DEBUG_FLAG:
                debug_function(get_frame(video_index, "select"), boxes)"""

            # remove boxes where no char was assigned
            b_i = 0
            while True:
                if b_i >= len(boxes):
                    break
                if boxes[b_i]["char"] is None:
                    boxes.remove(boxes[b_i])
                    continue
                b_i += 1

            for b in boxes:
                if b["char"] is not None:
                    b["char"] = b["char"].replace("⁄", "/")

            # if there are 2 '' after each other, interpret them as a single "
            b_i = 0
            while True:
                if b_i + 1 >= len(boxes):
                    break
                if boxes[b_i]["char"] == "'" and boxes[b_i + 1]["char"] == "'":
                    boxes[b_i]["right"] = max(
                        boxes[b_i]["right"], boxes[b_i + 1]["right"]
                    )
                    boxes[b_i]["left"] = min(boxes[b_i]["left"], boxes[b_i + 1]["left"])
                    boxes[b_i]["top"] = min(boxes[b_i]["top"], boxes[b_i + 1]["top"])
                    boxes[b_i]["bottom"] = max(
                        boxes[b_i]["bottom"], boxes[b_i + 1]["bottom"]
                    )
                    boxes[b_i]["char"] = '"'
                    del boxes[b_i + 1]
                    b_i -= 1
                b_i += 1

        BOX_DATA_CACHE_BLANK[video_index] = boxes

    if blank_box_data:
        return copy.deepcopy(BOX_DATA_CACHE_BLANK[video_index])

    elif video_index in BOX_DATA_CACHE_DEFAULT:
        boxes = copy.deepcopy(BOX_DATA_CACHE_DEFAULT[video_index])

    else:
        boxes = copy.deepcopy(BOX_DATA_CACHE_BLANK[video_index])

        pivot = get_pivot(video_index)

        cursor_positions = get_cursor_position(video_index)

        boxes = separate_lines_from_text(video_index, boxes, pivot)

        current_line = boxes[0]["rowStart"]
        lit = 0

        # introduce newlines if vertical distance is high enough.
        # (only relevant if line-numbers are not visible, e.g. zoom events)
        while True:
            if lit >= len(boxes):
                break
            max_bottom = None
            max_top = None
            while True:
                if lit >= len(boxes):
                    break
                if current_line != boxes[lit]["rowStart"]:
                    break
                if max_bottom is None or max_bottom < boxes[lit]["bottom"]:
                    max_bottom = boxes[lit]["bottom"]
                lit += 1

            if max_bottom is None:
                lit += 1
                continue
            current_line += 1
            lit_pre = lit

            while True:
                if lit >= len(boxes):
                    break
                if current_line != boxes[lit]["rowStart"]:
                    break
                if max_top is None or max_top > boxes[lit]["top"]:
                    max_top = boxes[lit]["top"]
                lit += 1

            if max_top is None:
                lit += 1
                continue

            if max_top - max_bottom > pivot["height"] * 1.6:
                for i in range(
                    0, math.floor((max_top - max_bottom) / (pivot["height"] * 1.6))
                ):
                    boxes.insert(
                        lit_pre,
                        {
                            "char": "\n",
                            "left": 4,
                            "bottom": int(
                                boxes[lit_pre - 1]["bottom"] + pivot["height"] * (i + 2)
                            ),
                            "right": 4 + int(pivot["width"] / 2),
                            "top": int(
                                boxes[lit_pre - 1]["bottom"] + pivot["height"] * (i + 1)
                            ),
                            "rowStart": boxes[i]["rowStart"],
                        },
                    )
            lit = lit_pre

        current_lines_visible = lines_visible(video_index)

        # main logic for adding whitespaces and newlines to the text.
        # this is mainly done by interpreting the distances between boxes
        for c in cursor_positions:
            cursor_left = c["left"]
            cursor_bottom = c["bottom"]
            cursor_right = c["right"]
            cursor_top = c["top"]
            i = 0
            while True:
                if i >= len(boxes) - 1:
                    break

                while (
                    not current_lines_visible
                    and i == 0
                    and boxes[i]["left"] - int(pivot["width"] / 3) - int(pivot["width"])
                    > 0
                ):
                    boxes.insert(
                        0,
                        {
                            "char": " ",
                            "left": boxes[i]["left"]
                            - int(pivot["width"] / 3)
                            - int(pivot["width"]),
                            "bottom": boxes[i]["bottom"],
                            "right": boxes[i]["left"] - int(pivot["width"] / 3),
                            "top": boxes[i]["top"],
                            "rowStart": boxes[i]["rowStart"],
                        },
                    )

                while (
                    not current_lines_visible
                    and boxes[i + 1]["char"] != "\n"
                    and boxes[i]["rowStart"] != boxes[i + 1]["rowStart"]
                    and boxes[i + 1]["left"]
                    - int(pivot["width"] / 3)
                    - int(pivot["width"])
                    > 0
                ):
                    boxes.insert(
                        i + 1,
                        {
                            "char": " ",
                            "left": boxes[i + 1]["left"]
                            - int(pivot["width"] / 3)
                            - int(pivot["width"]),
                            "bottom": boxes[i + 1]["bottom"],
                            "right": boxes[i + 1]["left"] - int(pivot["width"] / 3),
                            "top": boxes[i + 1]["top"],
                            "rowStart": boxes[i + 1]["rowStart"],
                        },
                    )

                if (
                    boxes[i + 1]["left"] - boxes[i]["right"] > pivot["width"]
                    and "remove" not in boxes[i + 1]
                    and not do_boxes_intersect(
                        boxes[i + 1],
                        {
                            "char": " ",
                            "left": boxes[i]["right"] + int(pivot["width"] / 3),
                            "bottom": boxes[i]["bottom"],
                            "right": boxes[i]["right"]
                            + int(pivot["width"])
                            + int(pivot["width"] / 3),
                            "top": boxes[i]["top"],
                            "rowStart": boxes[i]["rowStart"],
                        },
                    )
                ):
                    boxes.insert(
                        i + 1,
                        {
                            "char": " ",
                            "left": boxes[i]["right"] + int(pivot["width"] / 3),
                            "bottom": boxes[i]["bottom"],
                            "right": boxes[i]["right"]
                            + int(pivot["width"])
                            + int(pivot["width"] / 3),
                            "top": boxes[i]["top"],
                            "rowStart": boxes[i]["rowStart"],
                        },
                    )

                # fill up the space between char and cursor-position with whitespaces
                elif cursor_left is not None and (
                    boxes[i + 1]["right"] < boxes[i]["left"]
                    and cursor_top < boxes[i]["top"]
                    and cursor_bottom > boxes[i]["bottom"]
                    and cursor_left - pivot["width"] > boxes[i]["right"]
                ):
                    while boxes[i]["right"] + pivot["width"] < cursor_left:
                        boxes.insert(
                            i + 1,
                            {
                                "char": " ",
                                "left": boxes[i]["right"] + 4,
                                "bottom": boxes[i]["bottom"],
                                "right": int(boxes[i]["right"] + pivot["width"] - 4),
                                "top": boxes[i]["top"],
                                "rowStart": boxes[i]["rowStart"],
                            },
                        )
                        i += 1

                elif (
                    cursor_left is not None
                    and cursor_bottom > boxes[i]["bottom"]
                    and cursor_top < boxes[i]["top"]
                    and cursor_left - pivot["width"] > boxes[i]["right"]
                    and boxes[i + 1]["top"] > boxes[i]["bottom"]
                ):
                    i_tmp = i
                    while not boxes[i_tmp]["char"].isdigit():
                        i_tmp -= 1

                    while (
                        cursor_bottom > boxes[i]["bottom"]
                        and cursor_top < boxes[i]["top"]
                        and cursor_left - pivot["width"] * 1.4 > boxes[i]["right"]
                        and boxes[i + 1]["top"] > boxes[i]["bottom"]
                        and not do_boxes_intersect(
                            boxes[i + 1],
                            {
                                "left": boxes[i]["right"] + 2,
                                "bottom": boxes[i_tmp]["bottom"] + pivot["height"] / 8,
                                "right": int(boxes[i]["right"] + pivot["width"]),
                                "top": boxes[i_tmp]["top"] - pivot["height"] / 8,
                            },
                        )
                    ):
                        boxes.insert(
                            i + 1,
                            {
                                "char": " ",
                                "left": boxes[i]["right"] + 2,
                                "bottom": boxes[i_tmp]["bottom"],
                                "right": int(boxes[i]["right"] + pivot["width"]),
                                "top": boxes[i_tmp]["top"],
                                "rowStart": boxes[i]["rowStart"],
                            },
                        )
                        i += 1

                if (
                    boxes[i + 1]["left"] + get_pivot(video_index)["width"] / 1.5
                    < boxes[i]["right"]
                    and boxes[i]["char"] != "\n"
                ):
                    if (
                        cursor_left is not None
                        and boxes[i]["top"] > cursor_top
                        and boxes[i]["bottom"] < cursor_bottom
                        and boxes[i]["right"] < cursor_left
                    ):
                        boxes.insert(
                            i + 1,
                            {
                                "char": "\n",
                                "left": cursor_right + 4,
                                "bottom": boxes[i]["bottom"],
                                "right": int(cursor_right + pivot["width"] + 4),
                                "top": boxes[i]["top"],
                                "rowStart": boxes[i]["rowStart"],
                            },
                        )

                    else:
                        boxes.insert(
                            i + 1,
                            {
                                "char": "\n",
                                "left": boxes[i]["right"] + 4,
                                "bottom": boxes[i]["bottom"],
                                "right": int(boxes[i]["right"] + pivot["width"] - 4),
                                "top": boxes[i]["top"],
                                "rowStart": boxes[i]["rowStart"],
                            },
                        )

                    i += 1
                i += 1

        # if box was marked with remove, remove them finally
        i = 0
        while True:
            if i >= len(boxes):
                break
            if "remove" in boxes[i]:
                boxes.remove(boxes[i])
            else:
                i += 1

        # add newline at end of text if Linecache contains lines afterwards.
        # by default disabled since it is faulty (because of how the KNOWN_LINES_CACHE works)
        if CHECK_LAST_NEWLINE and boxes[-1]["char"] != "\n":
            i = len(boxes) - 1
            while True:
                if i < 0:
                    i = None
                    break
                if boxes[i]["char"] == "\n":
                    i += 1
                    break
                i -= 1

            if i is not None and boxes[i]["char"].isdigit():
                ref = get_color_of_box(video_index, boxes[i])
                number = 0
                cur = ref
                while (
                    i < len(boxes)
                    and boxes[i]["char"].isdigit()
                    and abs(ref[0] - cur[0]) < 10
                    and abs(ref[1] - cur[1]) < 10
                    and abs(ref[2] - cur[2]) < 10
                ):
                    number = number * 10 + int(boxes[i]["char"])
                    i += 1
                    if i < len(boxes):
                        cur = get_color_of_box(video_index, boxes[i])

                if [i for i in KNOWN_LINES_CACHE if i > number]:
                    boxes.append(
                        {
                            "char": "\n",
                            "left": boxes[-1]["right"] + 4,
                            "bottom": boxes[-1]["bottom"],
                            "right": int(boxes[-1]["right"] + pivot["width"] - 4),
                            "top": boxes[-1]["top"],
                        }
                    )
        BOX_DATA_CACHE_DEFAULT[video_index] = copy.deepcopy(boxes)

    # remove line-numbers if flag was set.
    # e.g.
    # 1 abc
    # 2 def
    # ->
    # abc
    # def
    if remove_line_numbers and boxes is not None:
        new_box_data = []
        begin_of_line = True
        current_line_color = None
        for b in boxes:
            if begin_of_line and current_line_color is None:
                current_line_color = get_color_of_box(video_index, b)

            if begin_of_line and b["char"].isdigit():
                current_color = get_color_of_box(video_index, b)
                if (
                    abs(current_line_color[0] - current_color[0]) < 10
                    and abs(current_line_color[1] - current_color[1]) < 10
                    and abs(current_line_color[2] - current_color[2]) < 10
                ):
                    continue
            new_box_data.append(b)
            if b["char"] == "\n":
                begin_of_line = True
                current_line_color = None
                continue
            begin_of_line = False
        boxes = new_box_data

    """if DEBUG_FLAG:
        debug_function(get_frame(video_index, "select"), boxes)"""

    return boxes


# stores video-indices and its corresponding cursor(s) position
CURSOR_POSITION_CACHE = {}


# saves information about the position of cursor(s) in a cache and returns position(s)
def get_cursor_position(video_index, amount_of_cursors_needed=None):
    if video_index in CURSOR_POSITION_CACHE:
        return copy.deepcopy(CURSOR_POSITION_CACHE[video_index])

    # get box-data without whitespaces and newlines
    boxes = get_box_data(video_index, True, False)
    frame = FRAME_CACHE_ORIGINAL[video_index].copy()

    # https://nanonets.com/blog/ocr-with-tesseract/
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    _, frame = cv2.threshold(frame, 100, 255, cv2.THRESH_BINARY)

    frame = cv2.medianBlur(frame, 3)
    frame = cv2.dilate(frame, np.ones((3, 3), np.uint8), iterations=1)

    # mask chars such they are not visible any-longer
    for b in boxes:
        if b["char"] != "\n" and b["char"] != " ":
            cv2.rectangle(
                frame,
                (b["left"] - 4, b["top"] - 4),
                (b["right"] + 4, b["bottom"] + 4),
                (255, 255, 255),
                -1,
            ) if WHITE_BACKGROUND else cv2.rectangle(
                frame,
                (b["left"] - 4, b["top"] - 4),
                (b["right"] + 4, b["bottom"] + 4),
                (0, 0, 0),
                -1,
            )

    frame2 = FRAME_CACHE_ORIGINAL[video_index].copy()
    _, frame2 = cv2.threshold(frame2, 100, 255, cv2.THRESH_BINARY)
    frame2 = cv2.dilate(frame2, np.ones((3, 3), np.uint8), iterations=1)
    ref_color = (0, 255, 0)
    for b in boxes:
        if b["char"] != "\n" and b["char"] != " ":
            cv2.rectangle(
                frame2,
                (b["left"] - 2, b["top"] - 4),
                (b["right"] + 2, b["bottom"] + 4),
                ref_color,
                -1,
            )

    # since chars are masked, only the cursor(s) is visible anymore.
    # contours will offer position of the cursor(s)
    contours, hierarchy = cv2.findContours(
        frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
    )

    cursor_positions = []
    for c in contours:
        widths = []
        heights = []
        for cc in c:
            widths.append(cc[0][0])
            heights.append(cc[0][1])

        left = min(widths)
        bottom = max(heights)
        right = max(widths)
        top = min(heights)

        cursor_positions.append(
            {"left": left, "bottom": bottom, "right": right, "top": top}
        )

    # merge cursors if they belong together but were cut (e.g. overlay of chars)
    merged_cursors = []
    if len(cursor_positions) > 1:
        for c in cursor_positions:
            merge_found = False
            current_bottom = c["bottom"]
            if current_bottom + 1 < frame2.shape[0]:
                current_bottom = current_bottom + 1
            while (frame2[current_bottom, c["left"]] == ref_color).all():
                current_bottom += 1

            for cc in cursor_positions:
                if c == cc:
                    continue
                if current_bottom == cc["top"]:
                    merged_cursors.append(
                        {
                            "left": min(c["left"], cc["left"]),
                            "bottom": cc["bottom"],
                            "right": max(c["right"], cc["right"]),
                            "top": c["top"],
                        }
                    )
                    merge_found = True
                    if cc in merged_cursors:
                        merged_cursors.remove(cc)
                    break

            if not merge_found:
                merged_cursors.append(c)

        cursor_positions = merged_cursors

    # SANITY CHECK
    if len(cursor_positions) == 0 or amount_of_cursors_needed is not None:
        if len(cursor_positions) != amount_of_cursors_needed:
            # print("Cursors were not found correctly!!!\
            # Found:", cursor_positions, "Needed:", amount_of_cursors_needed, "Index:", index)
            return [{"left": None, "bottom": None, "right": None, "top": None}]

    """if DEBUG_FLAG:
        debug_function(FRAME_CACHE_ORIGINAL[video_index], boxes)
        debug_function(FRAME_CACHE_ORIGINAL[video_index], cursor_positions)"""

    CURSOR_POSITION_CACHE[video_index] = cursor_positions
    return cursor_positions


#####################################################################################
#####################################################################################
# TEXT FUNCTIONS
#####################################################################################
#####################################################################################
# returns line-number of a given video_index and index of lines
def get_text_of_certain_linenumber(video_index, line):
    text = get_text(video_index)
    if text is None:
        return None
    lines = get_line_numbers_from_index(video_index)

    found = find_xth_occurrence(text, line - lines[0], "\n")
    found2 = find_xth_occurrence(text, line - lines[0] + 1, "\n")

    if found is not None:
        if found2 is None:
            found2 = len(text)
        return text[found + 1: found2 + 1]

    print(
        "get_text_of_certain_linenumber() - Linenumber was not found!",
        video_index,
        line,
    )
    return None


# returns linenumber box and video_index
def get_line_number_from_boxdata(boxes, box_index, video_index):
    text = ""
    for b in boxes:
        text += b["char"]
    line_index = 0
    current_len = 0
    for line in text.splitlines():
        if current_len + len(line) + 1 >= box_index:
            break
        current_len += len(line) + 1
        line_index += 1

    # for zoomed in events where line-numbers are not visible anymore
    if video_index in COL_OFFSET_CACHE:
        current_len -= COL_OFFSET_CACHE[video_index]
    return (
        get_line_numbers_from_index(video_index)[line_index],
        box_index - current_len - 1,
    )


# stores video-indices and its corresponding col-offset.
# col-offset means how many chars are cut on the left side of the screen.
# only happens at zoomed in events when line-numbers are not visible anymore.
# e.g.
# 1abcdef
# 2ghijkl
# ->
# cdef
# ijkl
# col-offset = 2
COL_OFFSET_CACHE = {}


# if zoom occurred and line-numbers are not known
# interpret line-numbers of current frame based on previous events,
# saves the guess of current line-numbers in cache and returns it
def get_line_numbers_from_index_with_zoom(
    current_index, text_ref=None, text=None, ref_lines=None
):
    # print("get_line_numbers_from_index_with_zoom()", current_index)
    ref_index = 0

    # get index and text of most previous frame with known line-numbers
    if text_ref is None and text is None:
        ref_index = current_index - 1
        if get_frame(current_index) is None:
            return []

        while True:
            if ref_index < 0:
                print(
                    "Reference index for zoom was not found! Perhaps video started zoomed in?"
                )
                end_routine(-1)
            if ref_index in LINE_NUMBERS_CACHE:
                break
            ref_index -= 1
        text_ref = get_text(ref_index)
        text = get_text(current_index)

    while text_ref[-1] == " ":
        text_ref = text_ref[: len(text_ref) - 1]
    while text[-1] == " ":
        text = text[: len(text) - 1]

    # find (unique) match between current text and most previous text with known linenumber
    # use this match to estimate what the line-numbers if current frame are
    # with the help of https://www.tutorialspoint.com/sequencematcher-in-python-for-longest-common-substring
    s = difflib.SequenceMatcher(None, text_ref, text)

    match = s.find_longest_match(0, len(text_ref), 0, len(text))

    ref_line_index = 0
    char_counter = 0
    for line in text_ref.splitlines():
        if char_counter + len(line) + 1 > match.a:
            break
        char_counter += len(line) + 1
        ref_line_index += 1

    if ref_lines is None:
        ref_line = get_line_numbers_from_index(ref_index)[ref_line_index]
    else:
        ref_line = ref_lines[
            ref_line_index
        ]

    lines = []
    lines_before_match = text[: match.b].count("\n")
    lines_after_match = text[match.b: len(text) - 1].count("\n") + 1

    # fill lines[] with now estimated known lines
    for i in range(ref_line - lines_before_match, ref_line + lines_after_match):
        lines.append(i)

    if ref_index not in COL_OFFSET_CACHE:
        COL_OFFSET_CACHE[ref_index] = 0

    # calculate col-offset
    if text_ref != text:
        if len(text_ref[: match.a].splitlines()) > 0 and (
            len(text[: match.b].splitlines()) <= 0
            or len(text[: match.b].splitlines()) > 0
            and len(text_ref[: match.a].splitlines()[-1])
            > len(text[: match.b].splitlines()[-1])
        ):
            current_col_offset = len(text_ref[: match.a + 1].splitlines()[-1]) - 1
        elif (
            len(text_ref[: match.a].splitlines()) <= 0
            and len(text[: match.b].splitlines()) <= 0
        ):
            current_col_offset = 0
        else:
            current_col_offset = -len(text[: match.b + 1].splitlines()[-1]) + 1
        COL_OFFSET_CACHE[current_index] = (
            COL_OFFSET_CACHE[ref_index] + current_col_offset
        )
    else:
        COL_OFFSET_CACHE[current_index] = COL_OFFSET_CACHE[ref_index]

    return lines


LINE_NUMBERS_VISIBLE_CACHE = {}


# checks whether lines in frame of video_index are visible (zoom occurred).
# saves True if visible or False if not visible in the cache and returns it
def lines_visible(video_index):
    if video_index in LINE_NUMBERS_VISIBLE_CACHE:
        return copy.deepcopy(LINE_NUMBERS_VISIBLE_CACHE[video_index])
    box_data = get_box_data(video_index, True, False)

    if not box_data:
        LINE_NUMBERS_VISIBLE_CACHE[video_index] = False
        return False

    line_numbers = []
    current_line = None
    current_line_color = None
    begin_of_line = True
    current_line_in_data = None
    for b in box_data:
        if b["rowStart"] != current_line_in_data:
            if current_line is not None:
                line_numbers.append(current_line)
                current_line = None
            begin_of_line = True
            current_line_color = None
            current_line_in_data = b["rowStart"]

        if begin_of_line and b["char"].isdigit():
            if current_line_color is None:
                current_line_color = get_color_of_box(video_index, b)
                current_line = int(b["char"])

            else:
                # all digits of a line-number has to have equal color
                current_color = get_color_of_box(video_index, b)
                if (
                    abs(current_line_color[0] - current_color[0]) < 10
                    and abs(current_line_color[1] - current_color[1]) < 10
                    and abs(current_line_color[2] - current_color[2]) < 10
                ):
                    current_line = (current_line * 10) + int(b["char"])

        elif not b["char"].isdigit():
            begin_of_line = False

        if not begin_of_line and current_line is not None:
            line_numbers.append(current_line)
            current_line = None

    if current_line is not None:
        line_numbers.append(current_line)

    if not line_numbers:
        LINE_NUMBERS_VISIBLE_CACHE[video_index] = False
        return False

    if line_numbers:
        current = line_numbers[0]
        for i in line_numbers:
            if current != i:
                LINE_NUMBERS_VISIBLE_CACHE[video_index] = False
                return False
            current += 1

    LINE_NUMBERS_VISIBLE_CACHE[video_index] = True
    if video_index not in LINE_NUMBERS_CACHE:
        LINE_NUMBERS_CACHE[video_index] = line_numbers

    return True


# stores video-indices and its corresponding lines-numbers.
# e.g.
# index 8 -> lines: 4,5,6,7,8
LINE_NUMBERS_CACHE = {}


# saves line-numbers of frame with video_index in cache and returns it
def get_line_numbers_from_index(video_index):
    # print("get_line_numbers_from_index()", video_index)
    if video_index in LINE_NUMBERS_CACHE:
        return copy.deepcopy(LINE_NUMBERS_CACHE[video_index])

    if not lines_visible(video_index):
        lines = get_line_numbers_from_index_with_zoom(video_index)
        LINE_NUMBERS_CACHE[video_index] = lines

    return copy.deepcopy(LINE_NUMBERS_CACHE[video_index])


# stores video-indices and its corresponding cursor-indices.
# e.g.
# index 14 -> cursor-indices: 4,8
CURSOR_INDEX_CACHE = {}


# saves and returns textual-context index of cursor(s).
# e.g.
# | = cursor
# ABC|DEFG -> cursor-index would be 3
def get_cursor_index(video_index, amount_of_cursors_needed=None):
    if video_index in CURSOR_INDEX_CACHE:
        return copy.deepcopy(CURSOR_INDEX_CACHE[video_index])

    boxes = get_box_data(video_index, False, True)

    cursor_positions = get_cursor_position(video_index, amount_of_cursors_needed)

    # with cursor-position, look where this position would fit in textual context
    real_indices = []
    for cursor in cursor_positions:
        left = cursor["left"]
        bottom = cursor["bottom"]
        top = cursor["top"]
        i = 0
        real_index = None
        for b in boxes:
            if real_index is not None and b["top"] > bottom:
                break
            elif (
                real_index is None
                and left < b["right"]
                and top < b["top"]
                and bottom > b["bottom"]
            ):
                real_index = i

            i += 1
            if b["right"] < left:
                real_index = i
        if real_index is None:
            real_index = len(boxes)
        real_indices.append(real_index)

    real_indices.sort()
    CURSOR_INDEX_CACHE[video_index] = real_indices
    return copy.deepcopy(real_indices)


# stores video-indices and its corresponding text.
# e.g.
# index 9 -> 4ABC\n5DEF\n6GHIJ
TEXT_CACHE_WITH_LINE_NUMBERS = {}
# also store the same information only without linenumbers.
# e.g.
# index 9 -> ABC\nDEF\nGHIJ
TEXT_CACHE_WITHOUT_LINE_NUMBERS = {}


# saves text of frame with video-index in cache and returns it
def get_text(video_index, remove_line_numbers=True):
    if remove_line_numbers and video_index in TEXT_CACHE_WITHOUT_LINE_NUMBERS:
        return copy.deepcopy(TEXT_CACHE_WITHOUT_LINE_NUMBERS[video_index])

    elif not remove_line_numbers and video_index in TEXT_CACHE_WITH_LINE_NUMBERS:
        return copy.deepcopy(TEXT_CACHE_WITH_LINE_NUMBERS[video_index])

    boxes = get_box_data(video_index, False, remove_line_numbers)

    if boxes is None:
        return None

    text = ""
    for box in boxes:
        if box["char"] is not None:
            text += box["char"]

    if remove_line_numbers:
        TEXT_CACHE_WITHOUT_LINE_NUMBERS[video_index] = text
    else:
        TEXT_CACHE_WITH_LINE_NUMBERS[video_index] = text

    return text


#####################################################################################
#####################################################################################
# SELECT FUNCTIONS
#####################################################################################
#####################################################################################

# stores video-indices and its corresponding frames.
# for_sel -> optimized for select-recognition
FRAME_CACHE_FOR_SEL = {}
# stores video-indices and selected chars of its corresponding frame
CACHE_SEL = {}


# saves chars which are selected inside a frame of video_index in a cache and returns them
def check_what_selected(video_index):
    if video_index > VIDEO_END_TIME:
        video_index = VIDEO_END_TIME

    if video_index in CACHE_SEL:
        return copy.deepcopy(CACHE_SEL[video_index])

    frame = get_frame(video_index, "select")

    boxes = get_box_data(video_index, False, False)

    if boxes is None:
        return []

    selected_chars = []
    index = 0
    boxes_ref = get_box_data(video_index, True)
    background = [0, 0, 0]

    if video_index not in FRAME_CACHE_FOR_SEL:
        frame_ref = get_frame(video_index, "select")
        # apply threshold to frame
        frame_ref[np.where((frame_ref >= [0, 0, 200]).all(axis=2))] = [255, 255, 255]
        frame_ref[np.all(frame_ref < 100, axis=2)] = background
        frame_ref = cv2.medianBlur(frame_ref, 5)
        FRAME_CACHE_FOR_SEL[video_index] = copy.deepcopy(frame_ref)
    else:
        frame_ref = copy.deepcopy(FRAME_CACHE_FOR_SEL[video_index])

    """if DEBUG_FLAG:
        debug_function(frame, boxes)"""
    if video_index not in CACHE_SEL:
        for b in boxes:
            for bb in boxes_ref:
                if bb["rowStart"] == b["rowStart"]:
                    b["top"] = bb["top"] - int(get_pivot(video_index)["height"] / 4)
                    b["bottom"] = bb["bottom"] + int(get_pivot(video_index)["height"] / 4)
                    break

            # find more relevant area of char (in order a char-box is inaccurate by its coordinates)
            left = b["left"] + int(get_pivot(video_index)["width"] / 16)
            right = b["right"] - int(get_pivot(video_index)["width"] / 16)
            while left < right:
                if (
                    frame_ref[b["top"] + int((b["bottom"] - b["top"]) / 2), left]
                    == background
                ).all():
                    left += 1
                else:
                    break
            while left < right:
                if (
                    frame_ref[b["top"] + int((b["bottom"] - b["top"]) / 2), right]
                    == background
                ).all():
                    right -= 1
                else:
                    break
            if left >= right:
                index += 1
                continue

            box = frame[b["top"] - 2: b["bottom"] + 2, left:right]

            total_amount_of_pixels = (b["bottom"] - b["top"] + 4) * (right - left)

            # decide whether char is selected
            if b["char"] and np.sum(box == BACKGROUND_COLOR) < total_amount_of_pixels / 50:
                linenumber, char_index = get_line_number_from_boxdata(
                    boxes, index, video_index
                )
                selected_chars.append(
                    {"char": b["char"], "rowStart": linenumber, "char_index": char_index}
                )
            index += 1

        CACHE_SEL[video_index] = selected_chars

    return copy.deepcopy(CACHE_SEL[video_index])


# returns selections which are completely new
def birth_of_selections(sets_with_key, current_index):
    new_born_selections = []
    for s in sets_with_key:
        if s[-1] is None:
            s.pop()
            new_born_selections.append(
                {
                    "type": "select",
                    "begin-frameindex": do_bisec_select(
                        current_index - INTERVAL,
                        INTERVAL,
                        TIME_ACCURACY,
                        s,
                        "select",
                        "begin",
                    ),
                    "end-frameindex": None,
                    "selected_chars": s,
                    "rowStart": s[0]["rowStart"],
                    "colStart": s[0]["char_index"] + 1,
                }
            )

    return new_born_selections


# returns unselections which are completely new and close current select-actions
def birth_of_unselections(current_actions, current_sets, current_index):
    finished_actions = []

    for current_action in current_actions:
        if current_action["type"] == "select":
            corresponding_set = None
            for current_set in current_sets:
                if current_set[-1] == current_action["key"]:
                    corresponding_set = current_set
                    break
            if corresponding_set is None:
                print("Error in birth_of_unselections()", current_index)
                end_routine(-1)

            unselected_chars = []
            selected_chars = []
            for char in current_action["selected_chars"]:
                if char not in corresponding_set:
                    unselected_chars.append(char)
                else:
                    selected_chars.append(char)

            if unselected_chars:
                current_action["end-frameindex"] = do_bisec_select(
                    current_index,
                    -INTERVAL,
                    TIME_ACCURACY,
                    current_action["selected_chars"],
                    "select",
                    "end",
                )
                finished_actions.append(current_action)

                current_action["type"] = "unselect"
                current_action["selected_chars"] = selected_chars
                current_action["unselected_chars"] = unselected_chars
                current_action["rowStart"] = selected_chars[0]["rowStart"]
                current_action["colStart"] = selected_chars[0]["char_index"] + 1

    add_timestamp_to_action(finished_actions)
    return finished_actions


# returns unselections which are complete/ended and close current select-actions if necessary
def death_of_unselection(current_actions, current_index):
    finished_actions = []

    index = 0
    while True:
        if index >= len(current_actions):
            break

        if (
            current_actions[index]["type"] == "unselect"
            and current_actions[index]["key"] is None
        ):
            current_actions[index]["end-frameindex"] = do_bisec_select(
                current_index,
                -INTERVAL,
                TIME_ACCURACY,
                current_actions[index]["unselected_chars"],
                "unselect",
                "end",
            )

            finished_actions.append(current_actions[index])
            current_actions.remove(current_actions[index])
            index -= 1

        elif (
            current_actions[index]["type"] == "select"
            and current_actions[index]["key"] is None
        ):
            current_actions[index]["end-frameindex"] = do_bisec_select(
                current_index,
                -INTERVAL,
                TIME_ACCURACY,
                current_actions[index]["selected_chars"],
                "select",
                "end",
            )
            finished_actions.append(current_actions[index])
            finished_actions.append(
                {
                    "type": "unselect",
                    "begin-frameindex": do_bisec_select(
                        current_index - INTERVAL,
                        INTERVAL,
                        TIME_ACCURACY,
                        current_actions[index]["selected_chars"],
                        "unselect",
                        "begin",
                    ),
                    "end-frameindex": do_bisec_select(
                        current_index,
                        -INTERVAL,
                        TIME_ACCURACY,
                        current_actions[index]["selected_chars"],
                        "unselect",
                        "end",
                    ),
                    "selected_chars": "",
                    "unselected_chars": current_actions[index]["selected_chars"],
                    "rowStart": current_actions[index]["selected_chars"][0]["rowStart"],
                    "colStart": current_actions[index]["selected_chars"][0][
                        "char_index"
                    ]
                    + 1,
                }
            )

            current_actions.remove(current_actions[index])
            index -= 1
        index += 1

    add_timestamp_to_action(finished_actions)
    return finished_actions


# with the information of selected chars,
# sets -> chars which belong to one select-action / chars which are contiguous
def create_sets(selected_chars, current_index):
    sets = []
    current_set = []

    for chars in selected_chars:
        if not current_set:
            current_set.append(chars)
            continue

        line_offset = 1
        if (
            chars["rowStart"] == current_set[-1]["rowStart"]
            and chars["char_index"] == current_set[-1]["char_index"] + 1
        ):
            current_set.append(chars)

        # for selection with multiple lines
        elif (
            (
                current_set[-1]["rowStart"] + line_offset == chars["rowStart"]
                or current_set[-1]["rowStart"] + line_offset + 1 == chars["rowStart"]
            )
            and chars["char_index"] == 0
            and len(
                get_text_of_certain_linenumber(current_index, current_set[-1]["rowStart"])
            )
            - 1
            == current_set[-1]["char_index"]
        ):
            current_set.append(chars)
            if current_set[-1]["rowStart"] + line_offset + 1 == chars["rowStart"]:
                line_offset += 1

        else:
            sets.append(current_set)
            current_set = [chars]

    if current_set:
        sets.append(current_set)

    return sets


# add key attribute to sets
# key -> says whether sets belong together (to say whether current selections belongs to a previous select-action)
def generate_keys(current_actions, current_sets):
    for current_action in current_actions:
        current_action["key"] = None

    for current_set in current_sets:
        current_set.append(None)

    current_key = 0
    found = False
    for current_action in current_actions:
        for char_action in current_action["selected_chars"]:
            for current_set in current_sets:
                for char_of_current_set in current_set:
                    if char_action == char_of_current_set:
                        current_action["key"] = current_key
                        current_set[-1] = current_key
                        found = True
                        break
                if found:
                    found = False
                    break
        current_key += 1


# with the help of the key, update actions, e.g. merge actions which belong together
def update_existing_actions(current_actions, current_sets):
    for current_action in current_actions:
        for current_set in current_sets:
            if current_action["key"] == current_set[-1]:
                current_set.pop()
                current_action["selected_chars"] = current_set
                if current_action["type"] == "unselect":
                    new_unselected_chars = []
                    for char in current_action["unselected_chars"]:
                        if char not in current_set:
                            new_unselected_chars.append(char)
                    current_action["unselected_chars"] += new_unselected_chars


# if typing-actions happening, select-actions may get shifted by the amount of added/removed chars
# this function adds an offset to the select-actions in order to fix this
def calculate_offset_for_selections(
    current_index, current_select_actions, current_ending=None
):
    global VIDEO_START_TIME
    global VIDEO_END_TIME
    global TIME_ACCURACY
    start_ori = VIDEO_START_TIME
    end_ori = VIDEO_END_TIME
    accuracy_ori = TIME_ACCURACY

    if current_ending is None:
        VIDEO_START_TIME = current_index - INTERVAL
    else:
        start_ori = VIDEO_START_TIME
        VIDEO_START_TIME = current_ending

    VIDEO_END_TIME = current_index
    if current_index > end_ori:
        VIDEO_END_TIME = end_ori

    TIME_ACCURACY = INTERVAL

    current_type_actions = find_add_remove_reveal_actions(False)
    print("Current add/remove-actions:", current_type_actions)

    for c in current_type_actions:
        c["colStart"] -= 1
    VIDEO_START_TIME = start_ori
    VIDEO_END_TIME = end_ori
    TIME_ACCURACY = accuracy_ori

    # remove actions
    # if a remove happened before selection (in a textual sense), the textual indices of current selections
    # gets reduced by the amount of chars which were removed before
    for current_type_action in reversed(current_type_actions):
        for current_select_action in current_select_actions:
            if current_type_action["type"] == "remove":
                new_lines = current_type_action["what"].count("\n")
                end_of_action_row = current_type_action["rowStart"] + new_lines
                if new_lines == 0:
                    end_of_action_col = current_type_action["colStart"] + len(
                        current_type_action["what"]
                    )
                else:
                    end_of_action_col = len(
                        current_type_action["what"].splitlines()[-1]
                    )

                for char in current_select_action["selected_chars"]:
                    if end_of_action_row < char["rowStart"]:
                        char["rowStart"] -= new_lines
                    elif end_of_action_row <= char["rowStart"]:
                        char_offset = char["char_index"] - end_of_action_col
                        char["rowStart"] -= new_lines
                        char["char_index"] = (
                            current_type_action["colStart"] + char_offset
                        )

                if current_type_action["type"] == "unselect":
                    for char in current_select_action["unselected_chars"]:
                        if end_of_action_row < char["rowStart"]:
                            char["rowStart"] -= new_lines
                        elif end_of_action_row <= char["rowStart"]:
                            char_offset = char["char_index"] - end_of_action_col
                            char["rowStart"] -= new_lines
                            char["char_index"] = (
                                current_type_action["colStart"] + char_offset
                            )
    # add actions
    # if an add happened before selection (in a textual sense), the textual indices of current selections
    # gets increased by the amount of chars which were added before
    for current_type_action in current_type_actions:
        for current_select_action in current_select_actions:
            if current_type_action["type"] == "add":
                new_lines = current_type_action["what"].count("\n")
                if new_lines == 0:
                    end_of_action_col = current_type_action["colStart"] + len(
                        current_type_action["what"]
                    )
                else:
                    end_of_action_col = len(
                        current_type_action["what"].splitlines()[-1]
                    )

                for char in current_select_action["selected_chars"]:
                    if current_type_action["rowStart"] < char["rowStart"]:
                        char["rowStart"] += new_lines
                    elif current_type_action["rowStart"] <= char["rowStart"]:
                        char_offset = (
                            char["char_index"] - current_type_action["colStart"]
                        )
                        char["rowStart"] += new_lines
                        char["char_index"] = end_of_action_col + char_offset

                if current_type_action["type"] == "unselect":
                    for char in current_select_action["unselected_chars"]:
                        if current_type_action["rowStart"] < char["rowStart"]:
                            char["rowStart"] += new_lines
                        elif current_type_action["rowStart"] <= char["rowStart"]:
                            char_offset = (
                                char["char_index"] - current_type_action["colStart"]
                            )
                            char["rowStart"] += new_lines
                            char["char_index"] = end_of_action_col + char_offset

    return current_select_actions


# for a given set, check whether all chars are contained in another set
def check_if_set_contains_all_chars(current_set, origin_set):
    for char in origin_set:
        if char not in current_set:
            return False

    return True


# for a given set, check whether at least one char is contained in another set
def check_if_set_contains_char(current_set, origin_set):
    for char in origin_set:
        if char in current_set:
            return True

    return False


#####################################################################################
#####################################################################################
# BISEC FUNCTIONS
#####################################################################################
#####################################################################################
# -> Bisec stands for bisection and tries to find a certain video-index by an approach
# which uses a bisection-algorithm

# do bisec operation for add/remove action
# finds exact begin and end of action -> as exact as time_accuracy says (e.g. time_accuracy = 1 ->
# difference between the real video-index and the calculated video-index must not be greater than 1).
# by default, time_accuracy is half the VIDEO_FPS, since this should be enough in most cases to
# determine the exact timestamp in seconds
def do_bisec(
    video_index, interval, time_accuracy, mode="text", webcam=False, print_stuff=True
):
    if print_stuff:
        print("Do bisection to get exact border of action (Border Accuracy):")

    before_string = None
    if mode == "text":
        before_string = get_text(video_index)
    elif mode == "linenumbers":
        before_string = get_line_numbers_from_index(video_index)

    if print_stuff:
        print("0%", end=" ", flush=True)

    while True:
        current_string = None
        if mode == "text":
            current_string = get_text(video_index + interval)
        elif mode == "linenumbers":
            current_string = get_line_numbers_from_index(video_index + interval)

        if mode == "text" and current_string != before_string:
            break
        elif mode == "linenumbers" and (
            not current_string or current_string[0] != before_string[0]
        ):
            break

        video_index += interval

    if abs(interval) <= time_accuracy:
        if print_stuff:
            print("100%")
        return video_index

    current_progress = 100 / (INTERVAL / math.ceil(math.log(INTERVAL, 2)))
    progress_step = current_progress

    while True:
        interval = int(math.ceil(abs(interval) / 2) * (interval / abs(interval)))
        if print_stuff:
            if current_progress < 100:
                print(str(round(current_progress, 1)) + "%", end=" ", flush=True)
            else:
                print(str(round(current_progress, 1)) + "%")
        current_progress += progress_step

        if mode == "text":
            current_string = get_text(video_index + interval)
        elif mode == "linenumbers":
            current_string = get_line_numbers_from_index(video_index + interval)

        if webcam and current_string is None:
            video_index += interval

        elif current_string is None:
            if abs(interval) <= time_accuracy:
                if print_stuff and current_progress < 100:
                    print("100%")
                return video_index
            break

        elif (
            mode == "text" and before_string == current_string
        ):  # and not check_what_selected(current_index + interval):
            video_index += interval
        elif (
            mode == "linenumbers"
            and current_string
            and before_string[0] == current_string[0]
        ):
            video_index += interval

        if abs(interval) <= time_accuracy:
            break
    if print_stuff and current_progress < 100:
        print("100%")
    return video_index


# do bisec operation for select action.
# finds exact begin and end of action -> as exact as time_accuracy says (e.g. time_accuracy = 1 ->
# difference between the real video-index and the calculated video-index must not be greater than 1).
# by default, time_accuracy is half the VIDEO_FPS, since this should be enough in most cases to
# determine the exact timestamp in seconds
def do_bisec_select(
    current_index,
    interval,
    time_accuracy,
    chars_to_compare_with,
    bisec_type,
    what,
    print_stuff=False,
):

    if print_stuff:
        print("Do bisection to get exact border of selection (Border Accuracy):")
        print("0%", end=" ", flush=True)
    mode = bisec_type + what

    if (
        abs(interval) <= time_accuracy
        or mode == "selectend"
        and check_if_set_contains_all_chars(
            check_what_selected(current_index), chars_to_compare_with
        )
    ):
        if print_stuff:
            print("100%")
        return current_index

    current_progress = 100 / (INTERVAL / math.ceil(math.log(INTERVAL, 2)))
    progress_step = current_progress
    while True:
        interval = int(math.ceil(abs(interval) / 2) * (interval / abs(interval)))
        if print_stuff:
            if current_progress < 100:
                print(str(round(current_progress, 1)) + "%", end=" ", flush=True)
            else:
                print(str(round(current_progress, 1)) + "%")

        current_progress += progress_step
        current_selected = check_what_selected(current_index + interval)

        if (
            mode == "selectbegin"
            and (
                not current_selected
                or not check_if_set_contains_char(
                    current_selected, chars_to_compare_with
                )
            )
            or mode == "selectend"
            and (
                not current_selected
                or not check_if_set_contains_all_chars(
                    current_selected, chars_to_compare_with
                )
            )
            or mode == "unselectbegin"
            and current_selected
            and check_if_set_contains_all_chars(current_selected, chars_to_compare_with)
            or mode == "unselectend"
            and not check_if_set_contains_char(current_selected, chars_to_compare_with)
        ):
            current_index += interval

        if abs(interval) <= time_accuracy:
            if print_stuff:
                print("100%")
            break

    if interval < 0 and bisec_type == "select":
        current_index -= 1

    return current_index


# do bisec operation for reveal action
# finds exact begin and end of action
def do_bisec_reveal(reveal_action, new_actions, text1, text2, lines_text_2, fake_cache):

    if (
        INTERVAL <= TIME_ACCURACY
        or reveal_action["frameindex"] <= VIDEO_START_TIME * VIDEO_FPS
    ):
        return reveal_action["frameindex"]

    interval = -INTERVAL
    current_index = reveal_action["frameindex"]

    while True:
        interval = int(math.floor(interval / 2))

        current_reveals = reveal_manager_fake_cache(
            current_index + interval,
            new_actions,
            text1,
            text2,
            lines_text_2,
            copy.deepcopy(fake_cache),
        )

        for cr in current_reveals:
            if reveal_action["what"] == cr["what"]:
                current_index += interval

        if abs(interval) <= TIME_ACCURACY:
            break

    return current_index


# do bisec operation for zoom events
# finds exact begin and end of zoom event -> as exact as time_accuracy says (e.g. time_accuracy = 1 ->
# difference between the real video-index and the calculated video-index must not be greater than 1).
# by default, time_accuracy is 1 the exact border of a zoom event can be highly relevant for not missing any
# important information (e.g. typed chars before the zoom event)
def do_bisec_zoom(video_index, interval, time_accuracy=1):
    # print("Do zoom-bisection to get exact border of action (Border Accuracy):")

    while True:
        interval = int(math.ceil(abs(interval) / 2) * (interval / abs(interval)))
        if not did_zoom_happen(
            video_index, video_index + interval
        ):  # and not check_what_selected(current_index + interval):
            video_index += interval

        if abs(interval) <= time_accuracy:
            break

    return video_index


#####################################################################################
#####################################################################################
# HELPER FUNCTIONS
#####################################################################################
#####################################################################################

# stores indices and its corresponding pivot-element
PIVOT_CACHE = {}


# saves and returns a pivot-element.
# pivot-element stands for a char which represents all the other chars of a given frame (in dimension aspects).
# the algorithm can be extended to a more educated algorithm if necessary
def get_pivot(video_index):
    if video_index in PIVOT_CACHE:
        return copy.deepcopy(PIVOT_CACHE[video_index])

    boxes = get_box_data(video_index, True, False)
    pivot = None
    for p in boxes:
        if p["char"].isdigit() and int(p["char"]) != 1:
            pivot = {
                "width": math.floor((p["right"] - p["left"]) * 0.94),
                "height": p["bottom"] - p["top"],
            }
            break

    if pivot is None:
        for p in boxes:
            if p["char"].isdigit():
                pivot = {
                    "width": math.floor((p["right"] - p["left"]) * 1.75),
                    "height": p["bottom"] - p["top"],
                }
                break

    if pivot is None:
        for p in boxes:
            if p["char"].isupper():
                pivot = {
                    "width": math.floor((p["right"] - p["left"])),
                    "height": p["bottom"] - p["top"],
                }
                break

    if pivot is None:
        print("Pivot was not found!")
        end_routine(-1)
    PIVOT_CACHE[video_index] = pivot
    return pivot


# checks whether 2 boxes intersect with each other.
# returns True if boxes intersect and False if they do not
def do_boxes_intersect(box1, box2):
    for w in range(box1["left"], box1["right"]):
        for h in range(box1["top"], box1["bottom"]):
            if (
                w < box2["right"]
                and w > box2["left"]
                and h > box2["top"]
                and h < box2["bottom"]
            ):
                return True
    return False


# checks whether zoom between 2 frames occurred.
# returns True if zoom happened or False if not
def did_zoom_happen(video_index1, video_index2):
    box_data1 = get_box_data(video_index1)
    box_data2 = get_box_data(video_index2)

    # if size between two same chars differs more than 2 pixel, zoom occurred
    threshold_zoom = 2
    for b1 in box_data1:
        for b2 in box_data2:
            if b1["char"] == b2["char"]:
                height_diff = abs(
                    (b2["top"] - b2["bottom"]) - (b1["top"] - b1["bottom"])
                )
                if height_diff > threshold_zoom:
                    return True
                else:
                    return False

    return False


# function for make the cache most efficient as possible.
# basically just modifies the video_index to have a cache hit if possible
def cache_hit_optimization(video_index, mode):
    video_index_ori = video_index
    counter = 0
    while True:
        if video_index in BOX_DATA_CACHE_DEFAULT:
            return video_index
        if mode == "plus":
            video_index += 1
        else:
            video_index -= 1

        counter += 1
        if counter >= INTERVAL:
            video_index = video_index_ori
            counter = 0
            break

    while True:
        if video_index in BOX_DATA_CACHE_BLANK:
            return video_index
        if mode == "plus":
            video_index += 1
        else:
            video_index -= 1

        counter += 1
        if counter >= INTERVAL:
            print("Cache-Hit Optimization failed!")
            if mode == "plus":
                return video_index - INTERVAL
            else:
                return video_index + INTERVAL


# returns the xth occurrence of a given char within a given text.
# e.g.
# text = "ABABDEFJAI"
# occurrence = 3
# char = A
# -> would return 8
def find_xth_occurrence(text, occurrence, char):
    if occurrence == 0:
        return -1
    i = 0
    current_occurrence = 0
    for c in text:
        if c == char:
            current_occurrence += 1
            if current_occurrence == occurrence:
                return i
        i += 1

    return None


# translate video-index to time-stamp and appends info to action
def add_timestamp_to_action(actions, reveal=False):
    for i in actions:
        if not reveal:
            seconds = int(math.floor(i["begin-frameindex"] / VIDEO_FPS))
            i["begin-second"] = seconds % 60
            i["begin-minute"] = math.floor(seconds / 60)
            i["begin-hour"] = math.floor(seconds / 3600)

            if "end-frameindex" in i:
                seconds = int(math.ceil(i["end-frameindex"] / VIDEO_FPS))
            else:
                seconds = int(math.ceil(i["begin-frameindex"] / VIDEO_FPS))

            i["end-second"] = seconds % 60
            i["end-minute"] = math.floor(seconds / 60)
            i["end-hour"] = math.floor(seconds / 3600)
        else:
            seconds = int(math.floor(i["frameindex"] / VIDEO_FPS))
            i["second"] = seconds % 60
            i["minute"] = math.floor(seconds / 60)
            i["hour"] = math.floor(seconds / 3600)


# end routine of the program
def end_routine(exit_value):
    VIDEO.release()
    cv2.destroyAllWindows()
    exit(exit_value)

#####################################################################################
#####################################################################################
# ACTION FUNCTIONS
#####################################################################################
#####################################################################################


# returns "blank actions" (= operations) between 2 given texts.
# done with basic diff approach
def get_operations_between_two_texts(
    text1, line1, text2, line2, current_operation, actions, current_actions, video_index
):
    s = difflib.SequenceMatcher(None, text1, text2)

    current_ops = s.get_opcodes()
    for tag, i1, i2, j1, j2 in current_ops:
        if tag == "equal":
            current_ops.remove((tag, i1, i2, j1, j2))
    for tag, i1, i2, j1, j2 in current_ops:
        if tag == "insert" and current_operation == "remove":
            actions += finalize_actions(video_index, current_actions)
            current_operation = "add"
        elif tag == "delete" and current_operation == "add":
            actions += finalize_actions(video_index, current_actions)
            current_operation = "remove"

    """if DEBUG_FLAG:
        print("Unmodified Operations")
        for tag, i1, i2, j1, j2 in current_ops:
            print('{:7}   a[{}:{}] --> b[{}:{}] {!r:>8} --> {!r}'.format(
                tag, i1, i2, j1, j2, text1[i1:i2], text2[j1:j2]))"""

    # transfer replace action to either insert or delete
    if line1 == line2:
        i = 0
        while True:
            if i >= len(current_ops):
                break
            if current_ops[i][0] == "replace":
                if (
                    current_ops[i][2] - current_ops[i][1]
                    <= current_ops[i][4] - current_ops[i][3]
                ):
                    current_ops[i] = (
                        "insert",
                        current_ops[i][3],
                        current_ops[i][3],
                        current_ops[i][3],
                        current_ops[i][4],
                    )
                elif (
                    current_ops[i][2] - current_ops[i][1]
                    > current_ops[i][4] - current_ops[i][3]
                ):
                    current_ops[i] = (
                        "delete",
                        current_ops[i][3],
                        current_ops[i][4],
                        current_ops[i][3],
                        current_ops[i][3],
                    )
                i += 1
                continue

            # if newline(s) occurred and amount of lines stays the same, this means the view in the video is
            # too small to cover all the text -> current operations may need to be deleted
            j1 = current_ops[i][3]
            j2 = current_ops[i][4]
            new_lines = text2[j1:j2].count("\n")
            while new_lines > 0 and len(current_ops) > i:
                lit = len(current_ops) - 1
                i1 = current_ops[lit][1]
                i2 = current_ops[lit][2]
                contains_new_lines = text1[i1:i2].count("\n")
                if contains_new_lines <= new_lines:
                    current_ops.remove(current_ops[lit])
                    break
                new_tuple = (
                    current_ops[lit][0],
                    current_ops[lit][1],
                    len(text1[i1:i2].rsplit("\n", contains_new_lines)[0]),
                    current_ops[lit][3],
                    current_ops[lit][4],
                )
                current_ops[lit] = new_tuple
                new_lines -= contains_new_lines
            i += 1

    """if DEBUG_FLAG:
        print("Modified Operations")
        for tag, i1, i2, j1, j2 in current_ops:
            print('{:7}   a[{}:{}] --> b[{}:{}] {!r:>8} --> {!r}'.format(
                tag, i1, i2, j1, j2, text1[i1:i2], text2[j1:j2]))"""

    return current_ops, current_operation, actions, current_actions


# if action has reached its end (e.g. due PAUSE_IN_SECONDS), it is finalized here
def finalize_actions(video_index, current_actions, only_time_limit=False):
    actions_to_finalize = current_actions.copy()
    finalized_actions = []
    if only_time_limit:
        actions_to_finalize.clear()
        i = 0
        while i < len(current_actions):
            if (
                video_index - current_actions[i]["time_limit"]
                >= PAUSE_IN_SECONDS * VIDEO_FPS
            ):
                actions_to_finalize.append(current_actions[i])
                current_actions.remove(current_actions[i])
                i -= 1
            i += 1
    else:
        current_actions.clear()

    while len(actions_to_finalize) > 0:
        if actions_to_finalize[0]["type"] == "remove":
            txt = get_text(video_index - INTERVAL)
            lines = get_line_numbers_from_index(video_index - INTERVAL)
            actions_to_finalize[0]["rowStart"] = lines[
                txt[: actions_to_finalize[0]["from"]].count("\n")
            ]

            length = 0
            for line in txt.splitlines():
                if length + len(line) + 1 <= actions_to_finalize[0]["from"]:
                    length += len(line) + 1
                else:
                    break
            actions_to_finalize[0]["colStart"] = (
                actions_to_finalize[0]["from"] - length + 1
            )

        actions_to_finalize[0]["end-frameindex"] = actions_to_finalize[0]["time_limit"]

        del actions_to_finalize[0]["from"]
        del actions_to_finalize[0]["to"]
        del actions_to_finalize[0]["time_limit"]
        if actions_to_finalize[0]["unsure"]:
            actions_to_finalize[0]["note:"] = actions_to_finalize[0]["unsure"]
        del actions_to_finalize[0]["unsure"]

        finalized_actions.append(actions_to_finalize[0])
        actions_to_finalize.remove(actions_to_finalize[0])

    add_timestamp_to_action(finalized_actions)
    return finalized_actions


# birth of new actions.
# might get complex if it is not clear where action really begins.
# -> e.g. FuenfSieben -> FuenfSechsSieben -> could be an add action of "echsS" or "Sechs".
# - first problem-solving-approach:
# by bisection (compare current text with the text of 1 frame afterwards) -> bisec with time-accuracy=1.
# - second problem-solving-approach: if bisection did not help, take cursor index into account
def create_new_actions(
    video_index,
    text1,
    line1,
    text2,
    line2,
    current_ops,
    gladded,
    current_operation,
    actions,
    current_actions,
):
    new_actions = []
    unsure = True
    did_bisec = False
    cursor_was_not_found = False
    while unsure:
        unsure = False

        for tag, i1, i2, j1, j2 in current_ops:
            if tag == "insert":
                new_actions.append(
                    {
                        "type": "add",
                        "from": j1,
                        "to": j2,
                        "what": text2[j1:j2],
                        "time_limit": video_index,
                        "unsure": False,
                    }
                )

            elif tag == "delete":
                new_actions.append(
                    {
                        "type": "remove",
                        "from": i1,
                        "to": i2,
                        "what": text1[i1:i2],
                        "time_limit": video_index,
                        "unsure": False,
                    }
                )
            elif tag == "replace":
                new_actions.append(
                    {
                        "type": "remove",
                        "from": i1,
                        "to": i2,
                        "what": text1[i1:i2],
                        "time_limit": video_index,
                        "unsure": False,
                    }
                )
                new_actions.append(
                    {
                        "type": "add",
                        "from": j1,
                        "to": j2,
                        "what": text2[j1:j2],
                        "time_limit": video_index,
                        "unsure": False,
                    }
                )

            if tag == "insert" or tag == "delete" or tag == "replace":
                length = len(new_actions[-1]["what"])

                if (tag == "replace" or tag == "insert") and (
                    text2[j1:j2] == text2[j1 + length: j2 + length]
                    or text2[j1:j2] == text2[j1 - length: j2 - length]
                    or text2[j1:j2][::-1] == text2[j1 - 1: j2 - 1]
                    or text2[j1 - 1: j1] == text2[j2 - 1: j2]
                    or text2[j1: j1 + 1] == text2[j2: j2 + 1]
                ):
                    if cursor_was_not_found:
                        new_actions[-1]["unsure"] = "Cursors were not found"
                    elif did_bisec:
                        new_actions[-1]["unsure"] = "Solved with bisec"
                    unsure = True

                elif (tag == "replace" or tag == "delete") and (
                    text1[i1:i2] == text1[i1 + length: i2 + length]
                    or text1[i1:i2] == text1[i1 - length: i2 - length]
                    or text1[i1:i2][::-1] == text1[i1 - 1: i2 - 1]
                    or text1[i1 - 1: i1] == text1[i2 - 1: i2]
                    or text1[i1: i1 + 1] == text1[i2: i2 + 1]
                ):
                    if tag == "delete":
                        if cursor_was_not_found:
                            new_actions[-1]["unsure"] = "Cursors were not found"
                        elif did_bisec:
                            new_actions[-1]["unsure"] = "Solved with bisec"
                        unsure = True

                    else:
                        if cursor_was_not_found:
                            new_actions[-1]["unsure"] = "Cursors were not found"
                        elif did_bisec:
                            new_actions[-1]["unsure"] = "Solved with bisec"
                        unsure = True

        if gladded or cursor_was_not_found:
            unsure = False

        # unsure occurred -> first try: bisec
        if unsure and not did_bisec:
            print("Unsure in create_new_actions()")
            if current_operation == "add":
                video_index = do_bisec(video_index - INTERVAL, INTERVAL, 1) + 1
            else:
                video_index = do_bisec(video_index - INTERVAL, INTERVAL, 1) + 1
            did_bisec = True
            new_actions.clear()
            text2 = get_text(video_index)
            (
                current_ops,
                current_operation,
                actions,
                current_actions,
            ) = get_operations_between_two_texts(
                text1,
                line1,
                text2,
                line2,
                current_operation,
                actions,
                current_actions,
                video_index,
            )
        # unsure occurred -> bisec did not help -> second try: cursor-index
        elif unsure and did_bisec:
            video_index -= 1
            cursor_indices = get_cursor_index(video_index, len(new_actions))
            if len(cursor_indices) == 0:
                video_index += 1
                text2 = get_text(video_index)
                (
                    current_ops,
                    current_operation,
                    actions,
                    current_actions,
                ) = get_operations_between_two_texts(
                    text1,
                    line1,
                    text2,
                    line2,
                    current_operation,
                    actions,
                    current_actions,
                    video_index,
                )
                new_actions.clear()
                cursor_was_not_found = True
                continue

            cursor_pos_index = 0
            offset = 0
            for na in new_actions:
                if len(cursor_indices) == 0:
                    na["unsure"] = "Cursors were necessary but not found correctly"
                elif na["type"] == "add":
                    na["from"] = cursor_indices[cursor_pos_index] + offset
                    na["to"] = na["from"] + len(na["what"])
                    na["what"] = text2[na["from"]: na["to"]]
                    na["unsure"] = "Solved with cursor"
                else:
                    na["to"] = cursor_indices[cursor_pos_index] - offset
                    na["from"] = na["to"] - len(na["what"])
                    na["what"] = text1[na["from"]: na["to"]]
                    na["unsure"] = "Solved with cursor"
                cursor_pos_index += 1
                offset += len(na["what"])

            video_index += 1
            unsure = False

    return (
        new_actions,
        video_index,
        text1,
        text2,
        current_operation,
        actions,
        current_actions,
    )


# checks whether final actions can be merged somehow.
# e.g. merging by adding newlines inbetween actions
def merge_detector(final_information, reveal=False):
    i = 0
    if reveal:
        while i < len(final_information):
            lit = 0
            while lit < len(final_information) and i < len(final_information):
                if i == lit:
                    lit += 1
                    continue

                # reveal
                if final_information[i]["type"] == "reveal" and lit - i == 1:
                    if final_information[lit]["type"] == "reveal":
                        if (
                            final_information[lit]["rowStart"]
                            == final_information[i]["rowStart"]
                            + final_information[i]["what"].count("\n")
                            + 1
                            and abs(
                                final_information[lit]["begin-frameindex"]
                                - final_information[i]["begin-frameindex"]
                            )
                            < PAUSE_IN_SECONDS * VIDEO_FPS
                        ):
                            final_information[i]["what"] = (
                                final_information[i]["what"]
                                + "\n"
                                + final_information[lit]["what"]
                            )
                            final_information[i][
                                "begin-frameindex"
                            ] = final_information[lit]["begin-frameindex"]
                            del final_information[lit]
                            continue
                        end_line = final_information[lit]["rowStart"] + final_information[
                            lit
                        ]["what"].count("\n")
                        if (
                            end_line >= final_information[i]["rowStart"]
                            and end_line
                            <= final_information[i]["rowStart"]
                            + final_information[i]["what"].count("\n")
                            and abs(
                                final_information[lit]["begin-frameindex"]
                                - final_information[i]["begin-frameindex"]
                            )
                            < PAUSE_IN_SECONDS * VIDEO_FPS
                        ):
                            cut_lines = (
                                final_information[i]["rowStart"]
                                - final_information[lit]["rowStart"]
                            )
                            to_remove_chars = find_xth_occurrence(
                                final_information[lit]["what"], cut_lines, "\n"
                            )
                            final_information[i]["what"] = (
                                final_information[lit]["what"][:to_remove_chars]
                                + "\n"
                                + final_information[i]["what"]
                            )
                            final_information[i]["rowStart"] = final_information[lit][
                                "rowStart"
                            ]
                            final_information[i][
                                "begin-frameindex"
                            ] = final_information[lit]["begin-frameindex"]
                            del final_information[lit]
                            continue

                        end_line = final_information[i]["rowStart"] + final_information[i][
                            "what"
                        ].count("\n")
                        if (
                            end_line >= final_information[lit]["rowStart"]
                            and end_line
                            <= final_information[lit]["rowStart"]
                            + final_information[lit]["what"].count("\n")
                            and abs(
                                final_information[lit]["begin-frameindex"]
                                - final_information[i]["begin-frameindex"]
                            )
                            < PAUSE_IN_SECONDS * VIDEO_FPS
                        ):
                            cut_lines = (
                                final_information[lit]["rowStart"]
                                - final_information[i]["rowStart"]
                            )
                            to_remove_chars = find_xth_occurrence(
                                final_information[i]["what"], cut_lines, "\n"
                            )
                            final_information[i]["what"] = (
                                final_information[i]["what"][:to_remove_chars]
                                + "\n"
                                + final_information[lit]["what"]
                            )
                            del final_information[lit]
                            continue
                lit += 1
            i += 1
    i = 0
    while i < len(final_information):
        lit = 0
        while lit < len(final_information) and i < len(final_information):
            if i == lit:
                lit += 1
                continue

            # add
            if (
                final_information[lit]["type"] == "add"
                and final_information[i]["type"] == final_information[lit]["type"]
            ):
                if (
                    len(final_information[i]["what"]) + final_information[i]["colStart"]
                    == final_information[lit]["colStart"]
                    and final_information[i]["rowStart"]
                    == final_information[lit]["rowStart"]
                    and abs(
                        final_information[i]["end-frameindex"]
                        - final_information[lit]["begin-frameindex"]
                    )
                    < PAUSE_IN_SECONDS * VIDEO_FPS
                ):
                    final_information[i]["end-frameindex"] = final_information[lit][
                        "end-frameindex"
                    ]
                    final_information[i]["what"] += final_information[lit]["what"]
                    del final_information[lit]
                    continue
                if (
                    final_information[i]["end-frameindex"]
                    < final_information[lit]["begin-frameindex"]
                    and final_information[i]["rowStart"]
                    + final_information[i]["what"].count("\n")
                    == final_information[lit]["rowStart"]
                    and final_information[lit]["colStart"] == 1
                    and abs(
                        final_information[i]["end-frameindex"]
                        - final_information[lit]["begin-frameindex"]
                    )
                    < PAUSE_IN_SECONDS * VIDEO_FPS
                ):
                    final_information[i]["end-frameindex"] = final_information[lit][
                        "end-frameindex"
                    ]
                    final_information[i]["what"] += final_information[lit]["what"]
                    del final_information[lit]
                    continue

                if (
                    len(final_information[i]["what"].splitlines()[-1])
                    + final_information[i]["colStart"]
                    == final_information[lit]["colStart"]
                    and final_information[i]["rowStart"]
                    + final_information[i]["what"].count("\n")
                    == final_information[lit]["rowStart"]
                    and abs(
                        final_information[i]["end-frameindex"]
                        - final_information[lit]["begin-frameindex"]
                    )
                    < PAUSE_IN_SECONDS * VIDEO_FPS
                ):
                    final_information[i]["end-frameindex"] = final_information[lit][
                        "end-frameindex"
                    ]
                    final_information[i]["what"] += final_information[lit]["what"]
                    del final_information[lit]
                    continue

                elif (
                    abs(
                        final_information[i]["end-frameindex"]
                        - final_information[lit]["begin-frameindex"]
                    )
                    < PAUSE_IN_SECONDS * VIDEO_FPS
                    and final_information[i]["rowStart"]
                    + final_information[i]["what"].count("\n")
                    + 1
                    == final_information[lit]["rowStart"]
                    and final_information[lit]["colStart"] == 1
                    and get_text_of_certain_linenumber(
                        final_information[i]["end-frameindex"],
                        final_information[i]["rowStart"]
                        + final_information[i]["what"].count("\n"),
                    )
                    == final_information[i]["what"].splitlines()[-1]
                ):
                    final_information[i]["end-frameindex"] = final_information[lit][
                        "end-frameindex"
                    ]
                    final_information[i]["what"] += (
                        "\n" + final_information[lit]["what"]
                    )
                    del final_information[lit]
                    continue
            elif (
                final_information[lit]["type"] == "remove"
                and final_information[i]["type"] == final_information[lit]["type"]
            ):
                if (
                    final_information[lit]["rowStart"]
                    + final_information[lit]["what"].count("\n")
                    == final_information[i]["rowStart"]
                    and len(final_information[lit]["what"].splitlines()[-1]) + 1
                    == final_information[i]["colStart"]
                    and abs(
                        final_information[lit]["begin-frameindex"]
                        - final_information[i]["end-frameindex"]
                    )
                    < PAUSE_IN_SECONDS * VIDEO_FPS
                ):
                    final_information[i]["what"] = (
                        final_information[lit]["what"] + final_information[i]["what"]
                    )
                    final_information[i]["end-frameindex"] = final_information[lit][
                        "end-frameindex"
                    ]
                    final_information[i]["rowStart"] = final_information[lit][
                        "rowStart"
                    ]
                    final_information[i]["colStart"] = final_information[lit][
                        "colStart"
                    ]
                    del final_information[lit]
                    i = 0
                    lit = 0
                    continue
            lit += 1
        i += 1

    return final_information


# create actions to write them into the .json file with all its desired (meta) information
def create_final_actions(actions_finished):
    print("Create Final Information:")
    actions_finished_with_meta_info = []

    action_property_args = []
    for arg in USER_ARGS:
        if arg[0][:16] == "action_property_":
            action_property_args.append([arg[0][16:], arg[1]])

    action_counter = 1
    for action in actions_finished:
        print("Finalize Action", action_counter, "/", len(actions_finished))
        action_counter += 1

        if action["type"] == "reveal":
            current_finished_action =\
                {"type": action["type"],
                 "frameindex": action["frameindex"],
                 "hour": action["hour"],
                 "minute": action["minute"],
                 "second": action["second"],
                 "rowStart": action["rowStart"],
                 "what": action["what"]}
        else:
            what = ""
            if action["type"] == "select" or action["type"] == "unselect":
                if action["type"] == "select":
                    for c in action["selected_chars"]:
                        what += c["char"]
                elif action["type"] == "unselect":
                    for c in action["unselected_chars"]:
                        what += c["char"]

            elif action["type"] == "add" or action["type"] == "remove":
                what = action["what"]

            current_finished_action =\
                {"type": action["type"],
                 "begin-frameindex": action["begin-frameindex"],
                 "end-frameindex": action["end-frameindex"],
                 "begin-hour": action["begin-hour"],
                 "begin-minute": action["begin-minute"],
                 "begin-second": action["begin-second"],
                 "end-hour": action["end-hour"],
                 "end-minute": action["end-minute"],
                 "end-second": action["end-second"],
                 "rowStart": action["rowStart"],
                 "colStart": action["colStart"],
                 "what": what}

        for arg in action_property_args:
            current_finished_action[arg[0]] = arg[1]

        print("Action finished:", current_finished_action)
        actions_finished_with_meta_info.append(current_finished_action)

    for a in actions_finished_with_meta_info:
        if a["type"] == "reveal":
            a["begin-frameindex"] = a["frameindex"]

    actions_finished_with_meta_info = sorted(actions_finished_with_meta_info, key=lambda d: d["begin-frameindex"])

    for a in actions_finished_with_meta_info:
        if a["type"] == "reveal":
            del a["begin-frameindex"]

    final_information_with_video_args = {}
    for arg in USER_ARGS:
        if arg[0][:15] == "video_property_":
            final_information_with_video_args[arg[0][15:]] = arg[1]

    final_information_with_video_args["actions"] = actions_finished_with_meta_info

    return final_information_with_video_args


#####################################################################################
#####################################################################################
# REVEAL AND CACHE FUNCTIONS
#####################################################################################
#####################################################################################


# updates the cache of known lines.
# also uses a dummy cache to do calculations for the real cache afterwards
def update_known_lines_cache(linenumber, mode, dummy_cache=None):
    if dummy_cache is None:
        if mode == "reveal":
            KNOWN_LINES_CACHE.append(linenumber)
        elif mode == "add":
            # find_index_of_operation
            index_to_operate = 0
            for current_element in KNOWN_LINES_CACHE:
                if current_element >= linenumber:
                    break
                index_to_operate += 1

            KNOWN_LINES_CACHE.insert(index_to_operate, linenumber)
            for i in range(index_to_operate + 1, len(KNOWN_LINES_CACHE)):
                KNOWN_LINES_CACHE[i] += 1

        elif mode == "remove":
            if linenumber in KNOWN_LINES_CACHE:
                KNOWN_LINES_CACHE.remove(linenumber)
                for i in range(linenumber, len(KNOWN_LINES_CACHE)):
                    KNOWN_LINES_CACHE[i] -= 1

    elif dummy_cache is not None:
        if mode == "reveal":
            dummy_cache.append(linenumber)
        elif mode == "add":
            # find_index_of_operation
            index_to_operate = 0
            for current_element in dummy_cache:
                if current_element >= linenumber:
                    break
                index_to_operate += 1

            dummy_cache.insert(index_to_operate, linenumber)
            for i in range(index_to_operate + 1, len(dummy_cache)):
                dummy_cache[i] += 1

        elif mode == "remove":
            if linenumber in dummy_cache:
                dummy_cache.remove(linenumber)
                for i in range(linenumber, len(dummy_cache)):
                    dummy_cache[i] -= 1


# main functionality for reveal actions.
# simple stores the known line-numbers in order to say whether line is already known.
# also takes into account newly typed/removed newlines
def reveal_manager(
    video_index, new_actions, text1, text2, lines_text_2, scrolling=False
):
    fake_cache = copy.deepcopy(KNOWN_LINES_CACHE)

    if not scrolling:
        # update KNOWN_LINES
        for na in new_actions:
            new_lines = 0
            if na["type"] == "add":
                new_lines = text2[na["from"]: na["to"]].count("\n")
            elif na["type"] == "remove":
                new_lines = text1[na["from"]: na["to"]].count("\n")
            if new_lines > 0:
                if na["type"] == "add":
                    begin_of_new_lines = get_line_numbers_from_index(video_index)[
                        text2[: na["from"]].count("\n")
                    ]

                    for nl in range(begin_of_new_lines, begin_of_new_lines + new_lines):
                        update_known_lines_cache(nl, na["type"])
                elif na["type"] == "remove":
                    line_numbers = get_line_numbers_from_index(video_index)
                    line_to_del = line_numbers[-1]
                    for nl in range(0, new_lines):
                        update_known_lines_cache(line_to_del, na["type"])
                        line_to_del -= 1

    # find revealed actions
    new_revealed_actions = []
    for line in lines_text_2:
        if line not in KNOWN_LINES_CACHE:
            text_of_line = get_text_of_certain_linenumber(video_index, line)
            if text_of_line is not None and len(text_of_line) > 0:
                new_revealed_actions.append(
                    {
                        "type": "reveal",
                        "frameindex": video_index,
                        "rowStart": line,
                        "what": text_of_line,
                    }
                )
            update_known_lines_cache(line, "reveal")
        KNOWN_LINES_CACHE.sort()

    # merge revealed actions
    nra_index = 0
    while True:
        if nra_index >= len(new_revealed_actions) - 1:
            break
        if (
            new_revealed_actions[nra_index]["rowStart"]
            + new_revealed_actions[nra_index]["what"].count("\n")
            == new_revealed_actions[nra_index + 1]["rowStart"]
        ):
            new_revealed_actions[nra_index] = {
                "type": "reveal",
                "frameindex": new_revealed_actions[nra_index]["frameindex"],
                "rowStart": new_revealed_actions[nra_index]["rowStart"],
                "what": new_revealed_actions[nra_index]["what"]
                + new_revealed_actions[nra_index + 1]["what"],
            }
            new_revealed_actions.remove(new_revealed_actions[nra_index + 1])
        else:
            nra_index += 1

    if not scrolling:
        for nra in new_revealed_actions:
            nra["frameindex"] = do_bisec_reveal(
                nra, new_actions, text1, text2, lines_text_2, fake_cache
            )

    add_timestamp_to_action(new_revealed_actions, True)

    return new_revealed_actions


# almost same as reveal_manager(),
# but operates on a dummy cache in order to make calculations while not influencing the real cache
def reveal_manager_fake_cache(
    video_index, new_actions, text1, text2, lines_text_2, fake_cache
):
    if video_index > VIDEO_END_TIME:
        return []
    # update KNOWN_LINES
    for na in new_actions:
        new_lines = 0
        if na["type"] == "add":
            new_lines = text2[na["from"]: na["to"]].count("\n")
        elif na["type"] == "remove":
            new_lines = text1[na["from"]: na["to"]].count("\n")
        if new_lines > 0:
            if na["type"] == "add":
                begin_of_new_lines = get_line_numbers_from_index(video_index)[
                    text2[: na["from"]].count("\n")
                ]
                for nl in range(begin_of_new_lines, begin_of_new_lines + new_lines):
                    update_known_lines_cache(nl, na["type"], fake_cache)
            elif na["type"] == "remove":
                line_numbers = get_line_numbers_from_index(video_index)
                line_to_del = line_numbers[-1]
                for nl in range(0, new_lines):
                    update_known_lines_cache(line_to_del, na["type"], fake_cache)
                    line_to_del -= 1

    # find revealed actions
    new_revealed_actions = []
    for line in lines_text_2:
        if line not in fake_cache:
            text_of_line = get_text_of_certain_linenumber(video_index, line)
            if text_of_line is not None and len(text_of_line) > 0:
                new_revealed_actions.append(
                    {
                        "type": "reveal",
                        "begin-frameindex": video_index,
                        "rowStart": line,
                        "what": text_of_line,
                    }
                )
            update_known_lines_cache(line, "reveal", fake_cache)
        fake_cache.sort()

    # merge revealed actions
    nra_index = 0
    while True:
        if nra_index >= len(new_revealed_actions) - 1:
            break
        if (
            new_revealed_actions[nra_index]["rowStart"]
            + new_revealed_actions[nra_index]["what"].count("\n")
            == new_revealed_actions[nra_index + 1]["rowStart"]
        ):
            new_revealed_actions[nra_index] = {
                "type": "reveal",
                "begin-frameindex": new_revealed_actions[nra_index]["begin-frameindex"],
                "rowStart": new_revealed_actions[nra_index]["rowStart"],
                "what": new_revealed_actions[nra_index]["what"]
                + new_revealed_actions[nra_index + 1]["what"],
            }
            new_revealed_actions.remove(new_revealed_actions[nra_index + 1])
        else:
            nra_index += 1

    return new_revealed_actions


#####################################################################################
#####################################################################################
# MAIN ROUTINES
#####################################################################################
#####################################################################################


# main functionality for add/remove actions
def find_add_remove_reveal_actions(also_reveal=True):
    pre_finished_actions = []
    current_actions = []
    global KNOWN_LINES_CACHE
    KNOWN_LINES_CACHE = [0]
    current_operation = "add"
    global VIDEO_START_TIME
    ori_begin = VIDEO_START_TIME
    video_index = VIDEO_START_TIME
    gladded = False
    webcam_or_scrolling_or_zoom_found = False
    webcam_or_scrolling_or_zoom_found_before = False
    next_video_index = None
    text_backup = None

    biseced_actions = []
    if also_reveal:
        print(
            "---Find Actions---\nCurrent video_index", video_index, "/", VIDEO_END_TIME
        )
    while True:
        if webcam_or_scrolling_or_zoom_found:
            webcam_or_scrolling_or_zoom_found = False
            webcam_or_scrolling_or_zoom_found_before = True
            pre_finished_actions += finalize_actions(video_index, current_actions)
            video_index = next_video_index
            next_video_index = None
            if gladded:
                gladded = False

        text1 = get_text(video_index)
        lines_text_1 = get_line_numbers_from_index(video_index)
        index_backup = video_index

        if webcam_or_scrolling_or_zoom_found_before or video_index == VIDEO_START_TIME:
            if also_reveal:
                pre_finished_actions += reveal_manager(
                    index_backup, None, None, None, lines_text_1, True
                )
            webcam_or_scrolling_or_zoom_found_before = False

        video_index += INTERVAL

        # Check whether end is reached
        if video_index >= VIDEO_END_TIME:
            if gladded:
                pre_finished_actions += finalize_actions(video_index, current_actions)
                break
            else:
                video_index = VIDEO_END_TIME
                gladded = True

        text2 = get_text(video_index)
        lines_text_2 = get_line_numbers_from_index(video_index)

        # webcam e.g.
        if text1 is None and text2 is None:
            continue

        # webcam e.g. coming
        elif text2 is None:
            video_index = do_bisec(index_backup, INTERVAL, 1, "linenumbers", False, also_reveal)
            next_video_index = video_index + 1
            text2 = get_text(video_index)
            lines_text_2 = get_line_numbers_from_index(video_index)
            text_backup = text2
            webcam_or_scrolling_or_zoom_found = True

        # webcam e.g. over
        elif text1 is None:
            video_index = do_bisec(index_backup, INTERVAL, 1, "text", True, also_reveal) + 1
            if get_text(video_index) != text_backup:
                pre_finished_actions += finalize_actions(video_index, current_actions)
                pre_finished_actions = merge_detector(pre_finished_actions)
                # do bisec
                for a in pre_finished_actions:
                    if a["type"] == "add" or a["type"] == "remove":
                        a["begin-frameindex"] = do_bisec(
                            a["begin-frameindex"], INTERVAL, TIME_ACCURACY, "text", False, also_reveal
                        )
                        a["end-frameindex"] = do_bisec(
                            a["end-frameindex"], -INTERVAL, TIME_ACCURACY, "text", False, also_reveal
                        )
                biseced_actions = merge_detector(pre_finished_actions)
                pre_finished_actions = []
                current_actions = []
                KNOWN_LINES_CACHE = [0]
                current_operation = "add"
                gladded = False
                webcam_or_scrolling_or_zoom_found = False
                webcam_or_scrolling_or_zoom_found_before = False
                next_video_index = None
                VIDEO_START_TIME = video_index

            text_backup = None
            continue

        # scrolling
        elif (
            lines_visible(video_index)
            and len(lines_text_1) > 0
            and len(lines_text_2) > 0
            and lines_text_1[0] != lines_text_2[0]
        ):
            video_index = do_bisec(index_backup, INTERVAL, 1, "linenumbers", False, also_reveal)
            next_video_index = video_index + 1
            text2 = get_text(video_index)
            lines_text_2 = get_line_numbers_from_index(video_index)
            webcam_or_scrolling_or_zoom_found = True

        # zooming
        elif did_zoom_happen(index_backup, video_index):
            video_index = do_bisec_zoom(index_backup, INTERVAL)
            next_video_index = video_index + 1
            text2 = get_text(video_index)
            lines_text_2 = get_line_numbers_from_index(video_index)
            webcam_or_scrolling_or_zoom_found = True

        # logic for determining whether last character should be a newline with the help of the KNOWN_LINES_CACHE.
        # by default disabled because faulty (since cache is changed continuously).
        # to enable it, just set runs_left = 2 and CHECK_LAST_NEWLINE = True
        global CHECK_LAST_NEWLINE
        CHECK_LAST_NEWLINE = False
        runs_left = 1
        new_actions = []
        while runs_left > 0:
            (
                current_ops,
                current_operation,
                pre_finished_actions,
                current_actions,
            ) = get_operations_between_two_texts(
                text1,
                lines_text_1,
                text2,
                lines_text_2,
                current_operation,
                pre_finished_actions,
                current_actions,
                video_index,
            )

            (
                new_actions,
                video_index,
                text1,
                text2,
                current_operation,
                pre_finished_actions,
                current_actions,
            ) = create_new_actions(
                video_index,
                text1,
                lines_text_1,
                text2,
                lines_text_2,
                current_ops,
                gladded,
                current_operation,
                pre_finished_actions,
                current_actions,
            )

            if also_reveal:
                if not CHECK_LAST_NEWLINE:
                    pre_finished_actions += reveal_manager(
                        video_index, new_actions, text1, text2, lines_text_2
                    )
                else:
                    pre_finished_actions += reveal_manager(
                        video_index, new_actions, text1, text2, lines_text_2, True
                    )
            CHECK_LAST_NEWLINE = True
            if runs_left >= 2:
                text2 = get_text(video_index)
                lines_text_2 = get_line_numbers_from_index(video_index)
            runs_left -= 1
        CHECK_LAST_NEWLINE = False

        if also_reveal and 0 <= video_index <= VIDEO_END_TIME:
            print(
                "\n---Find Actions---\nCurrent video_index",
                video_index,
                "/",
                VIDEO_END_TIME,
            )
            print("Current add/remove/reveal-actions:", current_actions)
            print("New add/remove/reveal-actions:", new_actions)
            # pre-finished actions are unoptimized actions (means not merged and no bisec done yet)
            # print("Pre-Finished add/remove/reveal-actions:", *pre_finished_actions, sep="\n")
            print("Pre-Finished add/remove/reveal-actions:", pre_finished_actions)

        # calculate text-shifts
        for na in new_actions:
            for cu in current_actions:
                if na["from"] <= cu["from"] and na["type"] == "add":
                    cu["from"] += len(na["what"])
                    cu["to"] += len(na["what"])
                elif na["from"] < cu["to"] and na["type"] == "add":
                    cu["to"] += len(na["what"])

                elif cu["from"] <= na["from"] and na["type"] == "remove":
                    na["from"] += len(cu["what"])
                    na["to"] += len(cu["what"])
                elif cu["from"] < na["to"] and na["type"] == "remove":
                    na["to"] += len(cu["what"])

        # merge new_actions
        i = 0
        while True:
            if i >= len(new_actions):
                break
            for cu in current_actions:
                if (
                    new_actions[i]["type"] == cu["type"]
                    and new_actions[i]["from"] == cu["to"]
                    and cu["type"] == "add"
                ):
                    cu["to"] = new_actions[i]["to"]
                    cu["what"] += new_actions[i]["what"]
                    cu["time_limit"] = video_index
                    new_actions.remove(new_actions[i])
                    i -= 1
                    break
                elif (
                    new_actions[i]["type"] == cu["type"]
                    and new_actions[i]["to"] == cu["from"]
                    and cu["type"] == "remove"
                ):
                    cu["from"] = new_actions[i]["from"]
                    cu["what"] = new_actions[i]["what"] + cu["what"]
                    cu["time_limit"] = video_index
                    new_actions.remove(new_actions[i])
                    i -= 1
                    break
            i += 1

        # add meta-data to completely new actions
        for na in new_actions:
            if na["type"] == "add":
                na["rowStart"] = get_line_numbers_from_index(video_index)[
                    text2[: na["from"]].count("\n")
                ]
                length = 0
                for line in text2.splitlines():
                    if length + len(line) + 1 <= na["from"]:
                        length += len(line) + 1
                    else:
                        break

                na["colStart"] = na["from"] - length + 1
                if index_backup in COL_OFFSET_CACHE:
                    na["colStart"] += COL_OFFSET_CACHE[index_backup]

            na["begin-frameindex"] = index_backup

            current_actions.append(na)

        pre_finished_actions += finalize_actions(video_index, current_actions, True)

    if also_reveal:
        for pfa in pre_finished_actions:
            if pfa["type"] == "reveal":
                pfa["begin-frameindex"] = pfa["frameindex"]

    pre_finished_actions = sorted(pre_finished_actions, key=lambda d: d["begin-frameindex"])
    pre_finished_actions = merge_detector(pre_finished_actions, True)

    actions_total = len(pre_finished_actions)
    actions_current = 1
    # do bisec
    for a in pre_finished_actions:
        if also_reveal:
            print("\nBisec action", actions_current, "/", actions_total)
        if a["type"] == "add" or a["type"] == "remove":
            a["begin-frameindex"] = do_bisec(
                a["begin-frameindex"],
                INTERVAL,
                TIME_ACCURACY,
                "text",
                False,
                also_reveal,
            )
            a["end-frameindex"] = do_bisec(
                a["end-frameindex"],
                -INTERVAL,
                TIME_ACCURACY,
                "text",
                False,
                also_reveal,
            )
        actions_current += 1
    pre_finished_actions += biseced_actions
    pre_finished_actions = sorted(pre_finished_actions, key=lambda d: d["begin-frameindex"])

    i = 0
    while True:
        if i >= len(pre_finished_actions):
            break
        if pre_finished_actions[i]["type"] == "reveal" and pre_finished_actions[i]["what"] == "":
            pre_finished_actions.remove(pre_finished_actions[i])
            continue
        i += 1

    # update timestamps
    for i in pre_finished_actions:
        seconds = int(math.floor(i["begin-frameindex"] / VIDEO_FPS))
        i["begin-second"] = seconds % 60
        i["begin-minute"] = math.floor(seconds / 60)
        i["begin-hour"] = math.floor(seconds / 3600)

        if "end-frameindex" in i:
            seconds = int(math.ceil(i["end-frameindex"] / VIDEO_FPS))
            i["end-second"] = seconds % 60
            i["end-minute"] = math.floor(seconds / 60)
            i["end-hour"] = math.floor(seconds / 3600)

    # actions = merge_detector(actions)

    if also_reveal:
        for pra in pre_finished_actions:
            if pra["type"] == "reveal":
                pra["frameindex"] = pra["begin-frameindex"]
                del pra["begin-frameindex"]
                pra["second"] = pra["begin-second"]
                del pra["begin-second"]
                pra["minute"] = pra["begin-minute"]
                del pra["begin-minute"]
                pra["hour"] = pra["begin-hour"]
                del pra["begin-hour"]

    VIDEO_START_TIME = ori_begin
    return pre_finished_actions


# main functionality for select actions
def find_select_actions():
    video_index = VIDEO_START_TIME
    global KNOWN_LINES_CACHE
    KNOWN_LINES_CACHE = [0]
    pre_finished_actions = []

    current_actions = []

    gladded = False

    global INTERVAL
    optimization = False
    ori_interval = INTERVAL
    while True:
        if video_index >= VIDEO_END_TIME:
            if gladded:
                video_index -= INTERVAL
                selections_total = len(current_actions)
                selections_current = 1
                for c in current_actions:
                    print(
                        "\nBisec selections", selections_current, "/", selections_total
                    )
                    if c["type"] == "select":
                        c["end-frameindex"] = do_bisec_select(
                            video_index,
                            -INTERVAL,
                            TIME_ACCURACY,
                            c["selected_chars"],
                            "select",
                            "end",
                            True,
                        )
                    else:
                        c["end-frameindex"] = do_bisec_select(
                            video_index,
                            -INTERVAL,
                            TIME_ACCURACY,
                            c["unselected_chars"],
                            "unselect",
                            "end",
                            True,
                        )
                    selections_current += 1
                add_timestamp_to_action(current_actions)
                pre_finished_actions += current_actions
                if 0 <= video_index <= VIDEO_END_TIME:
                    print(
                        "\n---Find Selections---\nCurrent video_index",
                        video_index,
                        "/",
                        VIDEO_END_TIME,
                    )
                    # prepare info for runtime-print
                    selections_for_printing = copy.deepcopy(pre_finished_actions)
                    for a in selections_for_printing:
                        what = ""
                        chars_type = "selected_chars" if a["type"] == "select" else "unselected_chars"
                        for char_index in enumerate(a[chars_type]):
                            what += a[chars_type][char_index[0]]["char"]
                        a["what"] = what
                        del a["selected_chars"]
                        if "unselected_chars" in a:
                            del a["unselected_chars"]
                        if "key" in a:
                            del a["key"]
                    # pre-finished actions are unoptimized actions (means not merged and no bisec done yet)
                    # print("Pre-Finished (un)selections:", *selections_for_printing, sep="\n")
                    print("Pre-Finished (un)selections:", selections_for_printing)
                break
            else:
                gladded = True

        if 0 <= video_index <= VIDEO_END_TIME:
            print(
                "\n---Find Selections---\nCurrent video_index",
                video_index,
                "/",
                VIDEO_END_TIME,
            )
        if not gladded:
            current_actions = calculate_offset_for_selections(
                video_index, current_actions
            )
        else:
            current_actions = calculate_offset_for_selections(
                video_index, current_actions, video_index - INTERVAL
            )
            video_index = VIDEO_END_TIME
        # TODO
        # split/merge actions

        current_selected_chars = check_what_selected(video_index)

        current_sets = create_sets(current_selected_chars, video_index)

        generate_keys(current_actions, current_sets)

        pre_finished_actions += death_of_unselection(current_actions, video_index)

        pre_finished_actions += birth_of_unselections(
            current_actions, current_sets, video_index
        )

        update_existing_actions(current_actions, current_sets)

        current_actions += birth_of_selections(current_sets, video_index)

        if 0 <= video_index <= VIDEO_END_TIME:
            print("Current (un)selections:", current_actions)
            # prepare info for runtime-print
            selections_for_printing = copy.deepcopy(pre_finished_actions)
            for a in selections_for_printing:
                what = ""
                chars_type = "selected_chars" if a["type"] == "select" else "unselected_chars"
                for char_index in enumerate(a[chars_type]):
                    what += a[chars_type][char_index[0]]["char"]
                a["what"] = what
                del a["selected_chars"]
                if "unselected_chars" in a:
                    del a["unselected_chars"]
                if "key" in a:
                    del a["key"]
            # pre-finished actions are unoptimized actions (means not merged and no bisec done yet)
            # print("Pre-Finished (un)selections:", *selections_for_printing, sep="\n")
            print("Pre-Finished (un)selections:", selections_for_printing)

        if optimization:
            optimization = False
            INTERVAL = ori_interval
        optimized_index = cache_hit_optimization(video_index, "plus")
        if video_index != optimized_index:
            INTERVAL = optimized_index - video_index
            video_index = optimized_index
            optimization = True
        else:
            video_index += INTERVAL

    for i in pre_finished_actions:
        if "key" in i:
            del i["key"]

    return pre_finished_actions


# test-flag
ARGS = None


# function to parse and interpret commandline arguments
def parse_args():
    global ARGS
    if ARGS is None:
        parser = argparse.ArgumentParser(description="Command Line Parser")

        video_arg_string = "--video-property-"
        action_arg_string = "--action-property-"
        user_args = []

        help_called = False
        for arg in sys.argv:
            if arg == "-h" or arg == "--help" or arg == "--h":
                help_called = True
                break
            elif len(arg) >= len(video_arg_string) and arg[0:17] == video_arg_string:
                user_args.append(arg.split("=")[0])
            elif len(arg) >= len(action_arg_string) and arg[0:18] == action_arg_string:
                user_args.append(arg.split("=")[0])

        parser.add_argument(
            "--name",
            type=str,
            metavar="",
            help='Name of the video eg.: "path/video.mp4"',
        )
        parser.add_argument(
            "--check-interval",
            type=int,
            metavar="",
            help="Defines the timestep the video is scanned (default = Video FPS)"
            " eg.: 1 -> Checks video every frame whether something changed "
            " (Hint: Too high Interval may lead to overlook some actions."
            " Too low Interval increases runtime a lot and may produce incorrect actions due to"
            " artifacts because more frames are considered)",
        )
        parser.add_argument(
            "--video-start-time",
            type=float,
            metavar="",
            help="Defines the second where the scan begins (default = 0) eg.: 1 -> Begin scan from second 1",
        )
        parser.add_argument(
            "--video-end-time",
            type=float,
            metavar="",
            help="Defines the second where the scan ends (default = last second of the video)"
            " eg.: 5 -> End scan at second 5",
        )
        parser.add_argument(
            "--pause-in-seconds",
            type=int,
            metavar="",
            help="Time in seconds after action ends if nothing happens (default = 10)"
            " eg.: 15 -> If program analyses an action but nothing changes within 15 seconds, cut action",
        )
        parser.add_argument(
            "--time-accuracy-in-frames",
            type=int,
            metavar="",
            help="How exact the borders of the actions should be calculated"
            " (default = VideoFPS / 2, most exact = 1, most inexact = check-interval)"
            " eg.: 12 -> If the original border is less than 12 frames away, this optimization-step ends",
        )
        parser.add_argument(
            "--crop-area-left",
            type=int,
            metavar="",
            help="Crops left side (default = 0)"
            " eg.: 0 -> nothing should be cropped"
            " (IMPORTANT: Consider line-numbers should stay visible inside the cropped area!)",
        )
        parser.add_argument(
            "--crop-area-right",
            type=int,
            metavar="",
            help="Crops right side (default = 0)"
            " eg.: 960 -> crop 960 pixels from the right side to the left(in a Full Hd video this crops away the"
            " right half of the frame)",
        )
        parser.add_argument(
            "--crop-area-top",
            type=int,
            metavar="",
            help="Crops top side (default = 0) eg.: 0 -> nothing should be cropped",
        )
        parser.add_argument(
            "--crop-area-bottom",
            type=int,
            metavar="",
            help="Crops bottom side (default = 0) eg.: 0 -> nothing should be cropped",
        )

        if not help_called:
            for i in user_args:
                parser.add_argument(i, type=str, metavar="")
        else:
            parser.add_argument(
                "--video-property-KEY=VALUE",
                metavar="",
                help="Custom information which will be added at the begin of the .json "
                ' eg.: --video-property-title="Cooles C Tutorial" ->'
                ' The key "title" will be added at the begin of the .json with value "Cooles C Tutorial"',
            )

            parser.add_argument(
                "--action-property-KEY=VALUE",
                metavar="",
                help="Custom information which will be added within every single Action in the .json "
                " eg.: action-property-file=main.c ->"
                ' The key "file" will be added in every single Action with value "main.c"',
            )
        ARGS = parser.parse_args()
    args = ARGS
    video_name = ""
    if args.name is not None:
        video_name = args.name

    if not os.path.isfile(video_name):
        print("Error: Video was not found!")
        exit(-1)

    global VIDEO, VIDEO_FPS, TIME_ACCURACY, INTERVAL, VIDEO_WIDTH, VIDEO_HEIGHT, CROP

    VIDEO = cv2.VideoCapture(video_name)

    VIDEO_FPS = int(VIDEO.get(cv2.CAP_PROP_FPS))

    TIME_ACCURACY = int(math.floor(VIDEO_FPS / 2))
    INTERVAL = VIDEO_FPS
    VIDEO_WIDTH = int(VIDEO.get(cv2.CAP_PROP_FRAME_WIDTH))
    VIDEO_HEIGHT = int(VIDEO.get(cv2.CAP_PROP_FRAME_HEIGHT))

    if args.time_accuracy_in_frames is not None:
        if args.time_accuracy_in_frames <= 0:
            print("Time Accuracy should be > 0")
            end_routine(-1)
        TIME_ACCURACY = args.time_accuracy_in_frames

    if TIME_ACCURACY > INTERVAL:
        TIME_ACCURACY = INTERVAL
        print(
            "Time Accuracy was set to Interval because more doesnt really make sense."
        )

    global VIDEO_START_TIME, VIDEO_END_TIME, PAUSE_IN_SECONDS
    global CROP_AREA_LEFT, CROP_AREA_RIGHT, CROP_AREA_TOP, CROP_AREA_BOTTOM

    VIDEO_END_TIME = int(VIDEO.get(cv2.CAP_PROP_FRAME_COUNT)) - 1

    if args.check_interval is not None:
        INTERVAL = args.check_interval

    if args.video_start_time is not None:
        if args.video_start_time < 0:
            print("Error: Video Start Time should be positive!")
            end_routine(-1)
        if args.video_start_time * VIDEO_FPS > VIDEO_END_TIME:
            print("Error: Video Start Time is greater than video length!")
            end_routine(-1)
        VIDEO_START_TIME = int(args.video_start_time * VIDEO_FPS)

    if args.video_end_time is not None:
        if args.video_end_time < 0:
            print("Error: Video End Time should be positive!")
            end_routine(-1)
        if args.video_end_time * VIDEO_FPS >= VIDEO_END_TIME:
            print("Error: Video End Time is greater than video length!")
            end_routine(-1)
        VIDEO_END_TIME = int(args.video_end_time * VIDEO_FPS)

    if VIDEO_START_TIME >= VIDEO_END_TIME:
        print("Error: Video Start Time is equal or greater than Video End Time!")
        end_routine(-1)

    if args.crop_area_left is not None:
        if args.crop_area_left < 0:
            print("Error: Crop Area Left should be positive!")
            end_routine(-1)
        CROP_AREA_LEFT = args.crop_area_left
        CROP = True

    if args.crop_area_right is not None:
        if args.crop_area_right < 0:
            print("Error: Crop Area Right should be positive!")
            end_routine(-1)
        CROP_AREA_RIGHT = args.crop_area_right
        CROP = True

    if args.crop_area_top is not None:
        if args.crop_area_top < 0:
            print("Error: Crop Area Top should be positive!")
            end_routine(-1)
        CROP_AREA_TOP = args.crop_area_top
        CROP = True

    if args.crop_area_bottom is not None:
        if args.crop_area_bottom < 0:
            print("Error: Crop Area Bottom should be positive!")
            end_routine(-1)
        CROP_AREA_BOTTOM = args.crop_area_bottom
        CROP = True

    if (
        CROP_AREA_LEFT + CROP_AREA_RIGHT >= VIDEO_WIDTH
        or CROP_AREA_TOP + CROP_AREA_BOTTOM >= VIDEO_HEIGHT
    ):
        print("Error: There was more cropped than should be")
        end_routine(-1)

    if args.pause_in_seconds is not None:
        PAUSE_IN_SECONDS = args.pause_in_seconds

    for arg in vars(args):
        if arg[:15] == "video_property_" or arg[:16] == "action_property_":
            USER_ARGS.append([arg, getattr(args, arg)])

    # read in ref-images for compare chars during the runtime
    if TEXT_MODE == "without_pytesseract":
        global REF_IMAGES
        for filename in os.scandir(REF_IMAGES_FOLDER):
            if filename.is_file():
                ref = cv2.imread(filename.path, cv2.IMREAD_GRAYSCALE)
                char_name = os.path.basename(filename).rsplit('.', 1)[0]

                # since chars may be special characters which are not allowed by the operating system
                # to be part of a filename, a little translation has to be done.
                # this is also the case if the operating system treats filenames as case-sensitive
                if len(char_name) == 2 and char_name[0] == 'u':
                    char_name = char_name[1].upper()
                elif char_name == "dot":
                    char_name = '.'
                elif char_name == "slash":
                    char_name = '/'
                elif char_name == "backslash":
                    char_name = '\\'
                elif char_name == "star":
                    char_name = '*'
                elif char_name == "colon":
                    char_name = ':'
                elif char_name == "questionmark":
                    char_name = '?'
                elif char_name == "greater":
                    char_name = '>'
                elif char_name == "smaller":
                    char_name = '<'
                elif char_name == "pipe":
                    char_name = '|'

                REF_IMAGES.append({"name": char_name, "image": ref})

    video_index = 0

    # find and save background color of editor used in video
    while True:
        if video_index >= int(VIDEO.get(cv2.CAP_PROP_FRAME_COUNT)) - 1:
            print("No Text found!")
            end_routine(-1)

        frame = get_frame(video_index, "select")

        if frame is not None and get_text(video_index, False):
            global BACKGROUND_COLOR
            BACKGROUND_COLOR = [frame[10, 0, 0], frame[10, 0, 1], frame[10, 0, 2]]

            frame = get_frame(video_index)
            global WHITE_BACKGROUND
            if cv2.countNonZero(frame) < 2880 * 1620 / 2:
                WHITE_BACKGROUND = False
            else:
                WHITE_BACKGROUND = True

            break
        video_index += VIDEO_FPS


def main():
    # specify folder of ref images
    global REF_IMAGES_FOLDER
    REF_IMAGES_FOLDER = "RefImagesTwo"

    # parse command line arguments
    parse_args()

    # find actions (reveal, add, remove)
    actions = find_add_remove_reveal_actions()
    print("---!!Final diff Actions!!---")
    for a in actions:
        print(a)

    # input()

    # find selections (select, unselect)
    selections = find_select_actions()
    print("---!!Final select Actions!!---")
    for s in selections:
        print(s)

    actions += selections

    # create final actions for .json
    final_information = create_final_actions(actions)

    print("Amen:")
    for i in final_information["actions"]:
        print(i)
    print("Create video_information.json:")
    with open("video_information.json", "w") as outfile:
        json.dump(final_information, outfile, indent=2)
    end_routine(0)


# set this to true if you want debug/further outputs including images (frames).
# you can use the function below whenever and wherever you want.
# at some locations in the code which might be interesting this function is already placed but commented out.
# feel free to comment it in (and set DEBUG_FLAG = True) in order to get
# a better feeling what the code is doing and how the program works
DEBUG_FLAG = False


def debug_function(ori_frame=None, boxes=None):
    # show frame
    if ori_frame is not None and boxes is None:
        frame = copy.deepcopy(ori_frame)
        frame = cv2.resize(frame, (2880, 1620), interpolation=cv2.INTER_AREA)
        cv2.imshow("", frame)
        cv2.waitKey(0)
    # print boxes
    elif ori_frame is None and boxes is not None:
        for b in boxes:
            print(b)
    # print boxes and show them on frame
    elif ori_frame is not None and boxes is not None:
        frame = copy.deepcopy(ori_frame)
        for b in boxes:
            print(b)
            frame = cv2.rectangle(
                frame, (b["left"], b["top"]), (b["right"], b["bottom"]), (0, 0, 255), 2
            )
        cv2.imshow("", cv2.resize(frame, (2880, 1620), interpolation=cv2.INTER_AREA))
        cv2.waitKey(0)


if __name__ == "__main__":
    main()
