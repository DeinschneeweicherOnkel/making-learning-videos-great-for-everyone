Just the old version of the program (January 2022) based on the ideas and descriptions of `Bachelorarbeit.pdf`.

The main difference to the current version (March 2023) of the `main_program.py` in the root folder is that multiple-action-recognition is not supported (e.g. add-actions in more than 1 line at once).

This folder also includes a semi-well trained dataset for the font roboto-mono for pytesseract-OCR.