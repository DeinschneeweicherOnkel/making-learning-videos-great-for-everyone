import math
import os
import sys
import json
import argparse

import cv2
import pytesseract
import numpy as np

#pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'


VIDEO = None
VIDEO_WIDTH = None
VIDEO_HEIGHT = None
ORIGIN_FRAME = None

PAUSE_IN_SECONDS = 10
VIDEO_FPS = None

INTERVAL = None
TEXT_CACHE = {}
LINES_CACHE = {}

TIME_ACCURACY = None


USER_ARGS = []
VIDEO_START_TIME = 0
VIDEO_END_TIME = None

CROP = False
CROP_AREA_LEFT = 0
CROP_AREA_RIGHT = 0
CROP_AREA_TOP = 0
CROP_AREA_BOTTOM = 0

def main():

    parse_args()

    action_neighbourhoods = create_action_neighbourhoods()
    splitted_actions = split_actions_neighbourhood(action_neighbourhoods)

    # actions_finished = [[79, 98, "add"], [235, 269, "add"], [377, 436, "remove"], [502, 575, "add"], [657, 702, "add"], [868, 893, "add"], [896, 907, "add"], [1121, 1146, "add"], [1257, 1373, "add"], [1406, 1525, "add"], [1553, 1593, "remove"], [1705, 1744, "add"], [1855, 1943, "add"], [2020, 2026, "add"], [2513, 2514, "add"], [2663, 2664, "add"], [2794, 2873, "add"]]
    #actions_finished = [[79, 98, "add"], [235, 269, "add"], [436, 377, "remove"], [502, 575, "add"], [657, 702, "add"], [868, 893, "add"], [896, 907, "add"], [1121, 1146, "add"], [1257, 1373, "add"], [1406, 1525, "add"], [1593, 1553, "remove"], [1705, 1744, "add"], [1855, 1943, "add"], [2020, 2026, "add"], [2513, 2514, "add"], [2663, 2664, "add"], [2794, 2873, "add"]]
    #actions_finished = [[79, 98, "add"], [235, 269, "add"]]
    #actions_finished = [{"type": "add","begin-frameindex":235, "end-frameindex": 273}]

    final_information = create_final_actions(splitted_actions)

    print("Amen:")
    for i in final_information["actions"]:
        print(i)
    print("Create .json:")
    with open("video_information.json", "w") as outfile:
        json.dump(final_information, outfile, indent=2)

    end_routine(0)

def create_action_neighbourhoods():
    print("Create Action-Neighbourhoods:")
    current_begin = VIDEO_START_TIME
    actions = []

    while True:
        action, current_begin = find_begin_of_an_action(current_begin)
        if action == None:
            break

        next_action, current_end = find_end_of_an_action(current_begin, action)

        if next_action == current_end:
            new_end, new_begin = check_transmissions_of_action_neighbourhoods(next_action, action)

            if new_end != None:
                next_action = new_begin
                current_end = new_end
        print("End of Action found at second:", round(current_end / VIDEO_FPS,1))
        actions.append({"type": action, "begin-frameindex": current_begin, "end-frameindex": current_end})
        print("Action added:", actions[-1])

        if current_begin == next_action:
            next_action += 1
        current_begin = next_action
    print("Action-Neighbourhoods were created successfully!")
    return actions

def split_actions_neighbourhood(actions):
    print("Split Action-Neighbourhoods:")
    actions_finished = []
    for action in actions:
        print("Current Action:", action)
        current_action = action["type"]
        index_begin_origin = int(action["begin-frameindex"])
        index_end_origin = int(action["end-frameindex"])

        current_interval = INTERVAL

        if current_action == "remove":
            temp = index_end_origin
            index_end_origin = index_begin_origin
            index_begin_origin = temp
            current_interval = -INTERVAL

        current_index_begin = index_begin_origin

        ##############################################################################################
        while True:

            current_index_begin = extract_text_begin(current_index_begin, index_end_origin, current_interval)
            if current_index_begin == None:
                break
            """
            print("current_index_begin:", current_index_begin)
            print("\nGo into extract_text_end with: current_index_begin =", current_index_begin, "index_end_origin =", index_end_origin, "current_interval =", current_interval, "\n")
            """
            next_action, current_index_end = extract_text_end(current_index_begin, index_end_origin, current_interval)
            """
            print("current_index_end:", current_index_end)
            print("\nGo into check_transmissions_cursor with: next_action =", next_action, "current_interval =", current_interval, "\n")
            """
            next_action = check_transmissions_cursor(next_action, current_interval)
            """
            print("current_index_end after check_transmissions_cursor:", current_index_end)
            print("END TRANS", next_action)
            """
            if translate_video_to_text(next_action) != translate_video_to_text(current_index_end):
                current_index_end = next_action

            if len(actions_finished) > 1 and actions_finished[-1][
                "begin-frameindex"] == current_index_end or current_index_begin == current_index_end:
                print("Endless-Loop detected - Discard Action")
                next_action += int(abs(current_interval) / current_interval)
            elif translate_video_to_text(current_index_begin) != translate_video_to_text(current_index_end):
                print("End found at second:", round(current_index_end / VIDEO_FPS, 1))
                actions_finished.append({"type": current_action, "begin-frameindex": current_index_begin, "end-frameindex": current_index_end})
                print("Action found:", actions_finished[-1])

            if current_action == "add" and current_index_end >= index_end_origin or current_action == "remove" and current_index_end <= index_end_origin:
                break
            current_index_begin = next_action
    print("All actions found!")
    return actions_finished


def find_begin_of_an_action(current_index):
    if current_index >= VIDEO_END_TIME:
        return None, None
    interval = INTERVAL
    origin_string, origin_lines = translate_video_to_text(current_index)
    scrolling = False
    while True:
        print(round(current_index/VIDEO_FPS,1), "/", round(VIDEO_END_TIME/VIDEO_FPS,1), "seconds")
        if current_index >= VIDEO_END_TIME:
            return None, None
        current_index += interval
        current_string, current_lines = translate_video_to_text(current_index)
        if current_string == None:
            return None, None

        if origin_lines[0] != current_lines[0]:
            origin_lines = current_lines
            origin_string = current_string
            origin_index = current_index
            scrolling = True
            continue

        if len(origin_string) != len(current_string):
            current_index -= interval
            break

    if scrolling:
        new_index = find_begin_of_new_line(origin_index, -interval)
        if translate_video_to_text(new_index) != translate_video_to_text(current_index):
            current_index = new_index

    index_begin = current_index

    action = "undefined"

    if len(current_string) > len(origin_string):
        action = "add"
    elif len(current_string) < len(origin_string):
        action = "remove"
    print("Action found:", "'"+action+"'", "Begins at second:", round(index_begin/VIDEO_FPS,1))
    return action, index_begin

def find_end_of_an_action(current_index, action):
    print("Looking for end of current action:")
    interval = INTERVAL
    before_string, origin_lines = translate_video_to_text(current_index)

    begin_index = current_index
    end_of_action_index = current_index

    pause = 0
    while True:
        print(round(current_index/VIDEO_FPS,1), "/", round(VIDEO_END_TIME/VIDEO_FPS,1), "seconds")
        current_index += interval

        current_string, current_lines = translate_video_to_text(current_index)

        if current_index >= VIDEO_END_TIME:
            return VIDEO_END_TIME, VIDEO_END_TIME

        if current_string == None or pause >= PAUSE_IN_SECONDS or \
                current_lines != origin_lines or \
                (action == "add" and len(current_string) < len(before_string) or
                 action == "remove" and len(current_string) > len(
                            before_string)):
            current_index -= interval

            if current_lines != origin_lines:
                current_index = find_begin_of_new_line(current_index, interval) + 1

                if translate_video_to_text(current_index - 1) != translate_video_to_text(end_of_action_index):
                    end_of_action_index = current_index - 1
            if current_index < begin_index:
                current_index = begin_index
            break

        if current_string.casefold() != before_string.casefold() and len(current_string) != len(before_string):
            pause = 0
            end_of_action_index = current_index

        else:
            pause += interval / VIDEO_FPS

        before_string = current_string
    return current_index, end_of_action_index

def check_transmissions_of_action_neighbourhoods(action_end, action):
    if action_end + INTERVAL >= VIDEO_END_TIME:
        return VIDEO_END_TIME, VIDEO_END_TIME
    print("End of Action found - checking transmission:")
    current_text, _ = translate_video_to_text(action_end)
    len_before = len(current_text)
    current_index = action_end
    new_end = current_index

    interval = 1

    while current_index < action_end + INTERVAL:
        print(str(round((current_index - action_end) / INTERVAL * 100, 1))+"%", end=" ")
        current_index += interval
        current_text, _ = translate_video_to_text(current_index)
        current_len = len(current_text)

        if action == "add" and current_len < len_before or action == "remove" and current_len > len_before:
            print("100%")
            return new_end, current_index - interval
        elif action == "add" and current_len > len_before or action == "remove" and current_len < len_before:
            new_end = current_index

        len_before = current_len
    print("100%")
    return None, None

def find_most_left_change(before_text, current_text, enable_stop_index = False):

    stop_index = len(current_text)
    if enable_stop_index:
        bef_i = len(before_text) - 1
        cur_i = len(current_text) - 1
        while bef_i >= 0 and cur_i >= 0:
            if before_text[bef_i].casefold() == current_text[cur_i].casefold():
                stop_index = bef_i
            else:
                break
            bef_i -= 1
            cur_i -= 1

    current_top_left_change_index = len(before_text)

    for i in range(0, len(current_text)):
        if i > stop_index:
            current_top_left_change_index = i
            break
        if i >= len(before_text):
            # text begin
            current_top_left_change_index = i
            break
        if before_text[i].casefold() != current_text[i].casefold():
            current_top_left_change_index = i
            break

    return current_top_left_change_index


def find_most_right_change(before_text, current_text, enable_stop_index = True):

    stop_index = -1
    if enable_stop_index:
        for i in range(0, len(before_text)):
            if i < len(current_text) and before_text[i].casefold() == current_text[i].casefold():
                stop_index = i
            else:
                break

    current_top_right_change_index = len(current_text) - 1
    before_text_i = len(before_text) - 1
    for current_text_i in range(len(current_text) - 1, -1, -1):

        if before_text_i >= 0 and current_text_i >= 0 and before_text[before_text_i].casefold() != current_text[
            current_text_i].casefold():
            current_top_right_change_index = current_text_i
            break

        if before_text_i <= stop_index:
            current_top_right_change_index = current_text_i
            break

        before_text_i -= 1

    return current_top_right_change_index


def end_routine(exit_value):
    VIDEO.release()
    cv2.destroyAllWindows()
    exit(exit_value)


def remove_last_char_if_certain(text, chars):
    current_chars_index = 0
    while len(text) > 0 and current_chars_index < len(chars):
        if chars[current_chars_index] == text[-1]:
            text = text.rstrip(chars[current_chars_index])
            current_chars_index = 0
            continue
        current_chars_index += 1

    return text


def extract_line_numbers(text, index):
    if index in LINES_CACHE:
        return LINES_CACHE[index]

    line_numbers = []
    if text == None:
        return []

    lines = text.split('\n')
    for line in lines:
        line_numbers.append("")
        for i in line:
            if i.isdigit():
                line_numbers[-1] += i
            else:
                # if i != ' ' and i != '\n':
                #    if len(line_numbers) > 1:
                #        line_numbers[-1] = line_numbers[-2] + 1
                #    else:
                #        line_numbers[-1] = 1
                break

        if line_numbers[-1] != "":
            line_numbers[-1] = int(line_numbers[-1])
        else:
            del line_numbers[-1]

    LINES_CACHE[index] = line_numbers
    return line_numbers



def remove_lines_numbers_from_text(text):
    new_text = ""
    i = 0
    while i < len(text):
        if text[i].isdigit():
            new_text += ''
        else:
            while i < len(text):
                new_text += text[i]
                if text[i] == '\n':
                    break
                i += 1
        i += 1
    return new_text

WHITE_BACKGROUND = None
def translate_video_to_text(index, remove_chars = [], remove_multiple_newlines = True):

    if index not in TEXT_CACHE:
        VIDEO.set(cv2.CAP_PROP_POS_FRAMES, index)
        ret, frame = VIDEO.read()

        if not ret:
            return None, None

        if CROP:
            frame = frame[CROP_AREA_TOP:VIDEO_HEIGHT-CROP_AREA_BOTTOM, CROP_AREA_LEFT:VIDEO_WIDTH-CROP_AREA_RIGHT]

        #https://nanonets.com/blog/ocr-with-tesseract/
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        _, frame = cv2.threshold(frame, 100, 255, cv2.THRESH_BINARY)

        frame = cv2.medianBlur(frame, 5)
        frame = cv2.resize(frame, (2880, 1620), interpolation=cv2.INTER_AREA)
        frame = cv2.dilate(frame, np.ones((3, 3), np.uint8), iterations=1)

        # frame = cv2.erode(frame, np.ones((6, 6), np.uint8), iterations=1)
        # frame = cv2.morphologyEx(frame, cv2.MORPH_OPEN, np.ones((3, 3), np.uint8))
        # frame = cv2.Canny(frame, 100, 200)

        global WHITE_BACKGROUND
        if WHITE_BACKGROUND == None:
            if cv2.countNonZero(frame) < (2880 * 1620) / 2:
                WHITE_BACKGROUND = False
            else:
                WHITE_BACKGROUND = True
        if WHITE_BACKGROUND == False:
            frame = cv2.bitwise_not(frame)

        custom_config_text = r'--oem 3 --psm 6''--tessdata-dir "C:\\Program Files\\Tesseract-OCR\\tessdata"'
        text = pytesseract.image_to_string(frame, lang="roboto_mono", config=custom_config_text)
        text = text.replace("", "")
        #lines = extract_line_numbers(text, index)
        # print(index, "Text:", text)
        # print("Lines:", lines)
        #cv2.imshow("", frame)
        #cv2.waitKey(0)

        #text = remove_last_char_if_certain(text, [' ', '|'])
        TEXT_CACHE[index] = text

    text = TEXT_CACHE[index]
    before_len = len(text)

    while remove_multiple_newlines:
        text = text.replace("\n\n", "\n")

        # no replaces took place
        if before_len == len(text):
            break

        before_len = len(text)

    i = 0
    while i < len(remove_chars):
        before_len = len(text)
        current_replace_char = remove_chars[i]

        while True:
            text = text.replace(current_replace_char, "")

            #no replaces took place
            if before_len == len(text):
                break
            else:
                before_len = len(text)
                i = -1

        i += 1

    return text, extract_line_numbers(TEXT_CACHE[index], index)


def extract_text_begin(index_begin, index_end, interval):
    print("Find begin of Action:")
    origin_text, _ = translate_video_to_text(index_begin)

    current_index = index_begin

    while True:
        current_index += interval
        current_text, _ = translate_video_to_text(current_index)


        if current_text is None or current_index not in range(index_begin, index_end + int(interval / abs(interval)),
                                                              int(interval / abs(interval))):
            print(round(index_end / VIDEO_FPS, 1), "/",
                  round(index_end / VIDEO_FPS, 1), "second")

            if current_text is None:
                return None
            current_index -= interval
            current_text, _ = translate_video_to_text(current_index)
            end_text, _ = translate_video_to_text(index_end)
            if current_text != end_text:
                begin = do_bisek(current_index, interval)
                print("Begin found at second:", round(begin / VIDEO_FPS, 1))
                return begin
            print("No Begin was found!")
            return None

        print(round(current_index / VIDEO_FPS, 1), "/",
              round(index_end / VIDEO_FPS, 1), "second")

        if (origin_text) != (current_text):
            print("Begin found at second:", round((current_index - interval) / VIDEO_FPS, 1))
            return current_index - interval


def did_curser_jump(current_text, init, update):

    if init:
        did_curser_jump.init = True
        did_curser_jump.before_text = current_text
        return False

    if did_curser_jump.init:
        did_curser_jump.init = False
        did_curser_jump.before_top_left = find_most_left_change(did_curser_jump.before_text, current_text)
        did_curser_jump.before_top_right = find_most_right_change(did_curser_jump.before_text, current_text)
        new_chars = did_curser_jump.before_top_right - did_curser_jump.before_top_left + 1

        """
        print("")
        print("index:", index)
        print("did_curser_jump_before_text:", did_curser_jump.before_text)
        print("current_text:", current_text)
        print("current_top_right_change_index:", did_curser_jump.before_top_right )
        print("current_len:", len(current_text), "before_len:", len(did_curser_jump.before_text), "new_chars:", new_chars)
        print("current_top_left_change_index:", did_curser_jump.before_top_left)
        print("")
        """

        if new_chars != len(current_text) - len(did_curser_jump.before_text):
            #print("Cursor jumped!")
            return True
        did_curser_jump.before_text = current_text
        return False

    if did_curser_jump.before_text == current_text:
        return False

    current_top_left_change_index = find_most_left_change(did_curser_jump.before_text, current_text)
    current_top_right_change_index = find_most_right_change(did_curser_jump.before_text, current_text)

    before_len = len(did_curser_jump.before_text)
    current_len = len(current_text)

    new_chars = current_top_right_change_index - current_top_left_change_index + 1

    """
    print("")
    print(index)
    print(did_curser_jump.before_text)
    print(current_text)
    print("current_top_right_change_index:", current_top_right_change_index)

    print("current_len:", current_len, "before_len:", before_len, "new_chars:", new_chars)
    print("current_top_left_change_index:", current_top_left_change_index, "before_top_right_change_index:",
          did_curser_jump.before_top_right)
    print("")
    """

    if current_len < before_len:
        #print("Error - Del detected")
        return True

    if current_top_left_change_index <= did_curser_jump.before_top_right:
        #print("Curser jumped left!")
        return True

    #if current_len - before_len == new_chars and did_curser_jump.before_top_right + 1 == current_top_left_change_index:
        #print("No curser jump")

    #else:
        #print("Curser jumped right!")
    #    return True

    if not current_len - before_len == new_chars or not did_curser_jump.before_top_right + 1 == current_top_left_change_index:
        return True

    if update:
        did_curser_jump.before_top_right = current_top_right_change_index
        did_curser_jump.before_text = current_text

    return False

def extract_text_end(index_begin, index_end, interval):
    print("Find end of Action:")
    before_text, _ = translate_video_to_text(index_begin)
    next_text, _ = translate_video_to_text(index_begin + interval)
    did_curser_jump(before_text, True, True)

    current_index = index_begin
    current_end_index = current_index + interval

    while True:

        current_index += interval
        current_text, _ = translate_video_to_text(current_index)

        if current_index not in range(index_begin, index_end + int(interval / abs(interval)),
                                      int(interval / abs(interval))) or current_text is None:
            print(round(current_end_index / VIDEO_FPS, 1), "/",
                  round(index_end / VIDEO_FPS, 1), "second")
            print("End found!")
            return index_end, current_end_index

        print(round(current_index / VIDEO_FPS, 1), "/",
              round(index_end / VIDEO_FPS, 1), "second")

        if before_text == current_text:
            continue

        if did_curser_jump(current_text, False, True):
            print("End found!")
            return current_index - interval, current_end_index

        before_text = current_text

        current_end_index = current_index




def find_begin_of_new_line(current_index, interval):
    _, before_lines = translate_video_to_text(current_index)

    while True:
        interval = int(math.ceil(abs(interval) / 2) * (interval / abs(interval)))

        _, current_lines = translate_video_to_text(current_index + interval)

        if current_lines == None:
            if abs(interval) == 1:
                return current_index

        elif (before_lines) == (current_lines):
            current_index += interval

        if abs(interval) == 1:
            break

    return current_index


def do_bisek(current_index, interval, criteria = ""):
    print("Do bisection to get exact border of action (Border Accuracy):")

    before_string, before_lines = translate_video_to_text(current_index)
    print("0%", end=" ")
    while True:
        current_string, current_lines = translate_video_to_text(current_index + interval)
        if current_string != before_string:
            break

        current_index += interval

    if abs(interval) <= TIME_ACCURACY:
        print("100%")
        return current_index

    current_progress = 100 / (INTERVAL / math.ceil(math.log(INTERVAL, 2)))
    progress_step = current_progress
    while True:
        interval = int(math.ceil(abs(interval) / 2) * (interval / abs(interval)))

        if current_progress < 100:
            print(str(round(current_progress, 1)) + "%", end=" ")
        else:
            print(str(round(current_progress, 1)) + "%")
        current_progress += progress_step
        current_string, current_lines = translate_video_to_text(current_index + interval)

        if current_string == None:
            if abs(interval) <= TIME_ACCURACY:
                if current_progress < 100:
                    print("100%")
                return current_index

        elif (before_string) == (current_string):
            current_index += interval

        elif criteria == "LenChange" and len(before_string) + 1 == len(current_string):
            current_index += interval
            break

        if abs(interval) <= TIME_ACCURACY:
            break
    if current_progress < 100:
        print("100%")
    return current_index

def check_transmissions_cursor(current_index, interval):
    if current_index >= VIDEO_END_TIME:
        return VIDEO_END_TIME
    print("Do cursor transmission check:")
    before_string, before_lines = translate_video_to_text(current_index)
    print("0%", end=" ")
    current_progress = 100 / (INTERVAL / math.ceil(math.log(INTERVAL, 2)))
    progress_step = current_progress
    while abs(interval) > 1:
        interval = int(math.ceil(abs(interval) / 2) * (interval / abs(interval)))

        if current_progress < 100:
            print(str(round(current_progress, 1)) + "%", end=" ")
        else:
            print(str(round(current_progress, 1)) + "%")
        current_progress += progress_step

        if current_index + interval < 0:
            continue

        current_string, current_lines = translate_video_to_text(current_index + interval)

        if current_string == None:
            if abs(interval) == 1:
                if current_progress < 100:
                    print("100%")
                return current_index

        elif before_lines == current_lines and not did_curser_jump(current_string, False, False):
            current_index += interval

    if current_progress < 100:
        print("100%")

    return current_index


def merge_detector(final_information):
    i = 0

    while i < len(final_information) - 1:
        if final_information[i]["type"] == final_information[i + 1]["type"] and \
            len(final_information[i]["what"]) + final_information[i]["colStart"] == final_information[i + 1]["colStart"] and \
            final_information[i]["rowStart"] == final_information[i + 1]["rowStart"] and \
            final_information[i]["end-frameindex"] - final_information[i + 1]["begin-frameindex"] < PAUSE_IN_SECONDS * VIDEO_FPS:
            final_information[i]["end-frameindex"] = final_information[i + 1]["end-frameindex"]
            final_information[i]["what"] += final_information[i + 1]["what"]
            del final_information[i + 1]
            continue
        i += 1

    return final_information

def sort_timestamps(final_information):
    for action in final_information:
        if action["begin-frameindex"] > action["end-frameindex"]:
            temp = action["begin-frameindex"]
            action["begin-frameindex"] = action["end-frameindex"]
            action["end-frameindex"] = temp
    return final_information

def optimize_timestamps(final_information):
    action_counter = 1
    for action in final_information:
        print("Optimize Action (Border Accuracy)", action_counter, "/", len(final_information))
        begin = action["begin-frameindex"]
        end = action["end-frameindex"]
        print("For Begin:")
        action["begin-frameindex"] = do_bisek(begin, INTERVAL)
        #print("Bisek Begin:", action["begin-frameindex"])
        print("For End:")
        action["end-frameindex"] = do_bisek(end, -INTERVAL)
        #print("Bisek End:", action["end-frameindex"])
        action_counter+=1
    return final_information

def create_final_actions(actions_finished):
    print("Create Final Information:")
    final_information = []

    action_property_args = []
    for arg in USER_ARGS:
        if arg[0][:16] == "action_property_":
            action_property_args.append([arg[0][16:], arg[1]])

    action_counter = 1
    for action in actions_finished:
        print("Finalize Action", action_counter, "/", len(actions_finished))
        action_counter += 1

        begin_string, begin_lines = translate_video_to_text(action["begin-frameindex"])
        end_string, end_lines = translate_video_to_text(action["end-frameindex"])
        begin_lines_texts = begin_string.split("\n")
        end_lines_texts = end_string.split("\n")

        current_line = 0
        nothing_found = False

        #Get different Lines
        while True:
            if current_line >= len(begin_lines_texts) or current_line >= len(end_lines_texts):
                nothing_found = True
                break
            if begin_lines_texts[current_line] != end_lines_texts[current_line]:
                break
            current_line += 1

        if nothing_found:
            continue

        most_left_change = find_most_left_change(begin_lines_texts[current_line], end_lines_texts[current_line])

        most_left_change2 = find_most_left_change(begin_lines_texts[current_line], end_lines_texts[current_line], True)

        if most_left_change != most_left_change2:
            interval = INTERVAL
            if action["type"] == "remove":
                interval = -interval
            test_index = do_bisek(action["begin-frameindex"], interval, "LenChange")
            test_text, _ = translate_video_to_text(test_index)
            test_text = test_text.split("\n")[current_line]
            if test_text == begin_lines_texts[current_line]:
                test_text, _ = translate_video_to_text(test_index + int(abs(interval) / interval))
                test_text = test_text.split("\n")[current_line]
            most_left_change = find_most_left_change(begin_lines_texts[current_line], test_text) - 1

        new_action = {"type": action["type"],
                        "begin-frameindex": action["begin-frameindex"],
                        "end-frameindex": action["end-frameindex"],
                        "begin-hour": 0,
                        "begin-minute": 0,
                        "begin-second": 0,
                        "end-hour": 0,
                        "end-minute": 0,
                        "end-second": 0,
                        "rowStart": end_lines[current_line],
                        "colStart": most_left_change + 1,
                        "what": end_lines_texts[current_line][most_left_change:
                                most_left_change + len(end_lines_texts[current_line]) - len(begin_lines_texts[current_line])]}

        for arg in action_property_args:
            new_action[arg[0]] = arg[1]

        if len(new_action["what"].replace(" ","")) == 0:
            continue

        print("Action finished:", new_action)

        final_information.append(new_action)

    final_information = merge_detector(final_information)
    final_information = sort_timestamps(final_information)
    final_information = optimize_timestamps(final_information)

    for i in final_information:
        seconds = int(math.floor(i["begin-frameindex"] / VIDEO_FPS))
        i["begin-second"] = seconds % 60
        i["begin-minute"] = math.floor(seconds / 60)
        i["begin-hour"] = math.floor(seconds / 3600)

        seconds = int(math.ceil(i["end-frameindex"] / VIDEO_FPS))
        i["end-second"] = seconds % 60
        i["end-minute"] = math.floor(seconds / 60)
        i["end-hour"] = math.floor(seconds / 3600)

    final_information_with_video_args = {}
    for arg in USER_ARGS:
        if arg[0][:15] == "video_property_":
            final_information_with_video_args[arg[0][15:]] = arg[1]

    final_information_with_video_args["actions"] = final_information

    return final_information_with_video_args

def parse_args():
    parser = argparse.ArgumentParser(description='Command Line Parser')

    video_arg_string = "--video-property-"
    action_arg_string = "--action-property-"
    user_args = []

    help_called = False
    for arg in sys.argv:
        if arg == '-h' or arg == '--help':
            help_called = True
            break
        elif len(arg) >= len(video_arg_string) and arg[0:17] == video_arg_string:
            user_args.append(arg.split("=")[0])
        elif len(arg) >= len(action_arg_string) and arg[0:18] == action_arg_string:
            user_args.append(arg.split("=")[0])

    parser.add_argument('--name', type=str, metavar='', help='Name of the video eg.: "path/video.mp4"')
    parser.add_argument('--check-interval', type=int, metavar='', help='Defines the timestep the video is scanned (default = Video FPS) eg.: 1 -> Checks video every frame whether something changed '
                                                                       '(Hint: Too high Interval may lead to overlook some actions. Too low Interval increases runtime a lot and may produce incorrect actions due to artifacts because more frames are considered)')
    parser.add_argument('--video-start-time', type=float, metavar='', help='Defines the second where the scan begins (default = 0) eg.: 1 -> Begin scan from second 1')
    parser.add_argument('--video-end-time', type=float, metavar='', help='Defines the second where the scan ends (default = last second of the video) eg.: 5 -> End scan at second 5')
    parser.add_argument('--pause-in-seconds', type=int, metavar='', help='Time in seconds after action ends if nothing happens (default = 10) eg.: 15 -> If program analyzes an action but nothing changes within 15 seconds, cut action')
    parser.add_argument('--time-accuracy-in-frames', type=int, metavar='', help='How exact the borders of the actions should be calculated (default = VideoFPS / 2, most exact = 1, most unexact = check-interval) eg.: 12 -> If the original border is less than 12 frames away, this optimizationstep ends')
    parser.add_argument('--crop-area-left', type=int, metavar='', help='Crops left side (default = 0) eg.: 0 -> nothing should be cropped (IMPORTANT: Consider linenumbers should stay visible inside the cropped area!)')
    parser.add_argument('--crop-area-right', type=int, metavar='', help='Crops right side (default = 0) eg.: 960 -> crop 960 pixels from the right side to the left (in a Full Hd video this crops away the right half of the frame)')
    parser.add_argument('--crop-area-top', type=int, metavar='', help='Crops top side (default = 0) eg.: 0 -> nothing should be cropped')
    parser.add_argument('--crop-area-bottom', type=int, metavar='', help='Crops bottom side (default = 0) eg.: 0 -> nothing should be cropped')

    if not help_called:
        for i in user_args:
            parser.add_argument(i, type=str, metavar='')
    else:
        parser.add_argument("--video-property-KEY=VALUE", metavar='',
                            help='Custom information which will be added at the begin of the .json '
                                 'eg.: --video-property-title="Cooles C Tutorial" -> The key "title" will be added at the begin of the .json with value "Cooles C Tutorial"')

        parser.add_argument("--action-property-KEY=VALUE", metavar='',
                            help='Custom information which will be added within every single Action in the .json '
                                 'eg.: action-property-file=main.c -> The key "file" will be added in every single Action with value "main.c"')
    args = parser.parse_args()

    video_name = ""
    if args.name is not None:
        video_name = args.name

    if not os.path.isfile(video_name):
        print("Error: Video was not found!")
        exit(-1)

    global VIDEO, VIDEO_FPS, TIME_ACCURACY, INTERVAL, VIDEO_WIDTH, VIDEO_HEIGHT, CROP, ORIGIN_FRAME

    VIDEO = cv2.VideoCapture(video_name)

    VIDEO_FPS = int(VIDEO.get(cv2.CAP_PROP_FPS))

    TIME_ACCURACY = int(math.floor(VIDEO_FPS / 2))
    INTERVAL = VIDEO_FPS
    VIDEO_WIDTH = int(VIDEO.get(cv2.CAP_PROP_FRAME_WIDTH))
    VIDEO_HEIGHT = int(VIDEO.get(cv2.CAP_PROP_FRAME_HEIGHT))

    ret, ORIGIN_FRAME = VIDEO.read()

    if args.time_accuracy_in_frames is not None:
        if args.time_accuracy_in_frames <= 0:
            print("Time Accuracy should be > 0")
            end_routine(-1)
        TIME_ACCURACY = args.time_accuracy_in_frames

    if TIME_ACCURACY > INTERVAL:
        TIME_ACCURACY = INTERVAL
        print("Time Accuracy was set to Interval because more doesnt really make sense.")

    global VIDEO_START_TIME, VIDEO_END_TIME, CROP_AREA_LEFT, CROP_AREA_RIGHT, CROP_AREA_TOP, CROP_AREA_BOTTOM, PAUSE_IN_SECONDS

    VIDEO_END_TIME = int(VIDEO.get(cv2.CAP_PROP_FRAME_COUNT)) - 1

    if args.check_interval is not None:
        INTERVAL = args.check_interval

    if args.video_start_time is not None:
        if args.video_start_time < 0:
            print("Error: Video Start Time should be positive!")
            end_routine(-1)
        if args.video_start_time * VIDEO_FPS > VIDEO_END_TIME:
            print("Error: Video Start Time is greater than video length!")
            end_routine(-1)
        VIDEO_START_TIME = int(args.video_start_time * VIDEO_FPS)

    if args.video_end_time is not None:
        if args.video_end_time < 0:
            print("Error: Video End Time should be positive!")
            end_routine(-1)
        if args.video_end_time * VIDEO_FPS >= VIDEO_END_TIME:
            print("Error: Video End Time is greater than video length!")
            end_routine(-1)
        VIDEO_END_TIME = int(args.video_end_time * VIDEO_FPS)

    if VIDEO_START_TIME >= VIDEO_END_TIME:
        print("Error: Video Start Time is equal or greater than Video End Time!")
        end_routine(-1)

    if args.crop_area_left is not None:
        if args.crop_area_left < 0:
            print("Error: Crop Area Left should be positive!")
            end_routine(-1)
        CROP_AREA_LEFT = args.crop_area_left
        CROP = True

    if args.crop_area_right is not None:
        if args.crop_area_right < 0:
            print("Error: Crop Area Right should be positive!")
            end_routine(-1)
        CROP_AREA_RIGHT = args.crop_area_right
        CROP = True

    if args.crop_area_top is not None:
        if args.crop_area_top < 0:
            print("Error: Crop Area Top should be positive!")
            end_routine(-1)
        CROP_AREA_TOP = args.crop_area_top
        CROP = True

    if args.crop_area_bottom is not None:
        if args.crop_area_bottom < 0:
            print("Error: Crop Area Bottom should be positive!")
            end_routine(-1)
        CROP_AREA_BOTTOM = args.crop_area_bottom
        CROP = True

    if CROP_AREA_LEFT + CROP_AREA_RIGHT >= VIDEO_WIDTH or CROP_AREA_TOP + CROP_AREA_BOTTOM >= VIDEO_HEIGHT:
        print("Error: There was more cropped than should be")
        end_routine(-1)

    if args.pause_in_seconds is not None:
        PAUSE_IN_SECONDS = args.pause_in_seconds

    for arg in vars(args):
        if arg[:15] == "video_property_" or arg[:16] == "action_property_":
            USER_ARGS.append([arg, getattr(args, arg)])

if __name__ == '__main__':
    main()
