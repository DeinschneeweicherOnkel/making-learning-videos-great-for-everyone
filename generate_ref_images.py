import main_program


# generates ref images from given frame and given text
# video_index -> which frame of video should be used as a reference
# video_name -> name of video with reference frame
# expected_text -> text including line-numbers without newlines and without whitespaces on reference frame
# location_of_the_output_folder -> where the reference images should be saved
def generate_ref_images(video_index, video_name, expected_text, location_of_the_output_folder):

    main_program.VIDEO = main_program.cv2.VideoCapture(video_name)

    if main_program.VIDEO is None:
        print("Video was not found!")
        exit(-1)

    main_program.TEXT_MODE = "with_pytesseract"

    main_program.VIDEO_WIDTH = int(main_program.VIDEO.get(main_program.cv2.CAP_PROP_FRAME_WIDTH))
    main_program.VIDEO_HEIGHT = int(main_program.VIDEO.get(main_program.cv2.CAP_PROP_FRAME_HEIGHT))

    frame = main_program.get_frame(video_index)
    if frame is None:
        print("video_index was not valid (for this video!)")
        main_program.end_routine(-1)

    boxes = main_program.get_char_positions(frame)

    if len(boxes) != len(expected_text):
        print("Expected text must be as long as chars found in reference-frame!"
              "\nLenght of expected text was:", len(expected_text),
              "\nAmount of boxes found was:", len(boxes))
        main_program.end_routine(-1)

    box_index = 0
    for b in boxes:
        current_char_frame = frame[b["top"]: b["bottom"], b["left"]: b["right"]]

        filename = expected_text[box_index]

        # since chars may be special characters which are not allowed by the operating system
        # to be part of a filename, a little translation has to be done.
        # this is also the case if the operating system treats filenames as case-sensitive
        if filename.isupper():
            filename = 'u' + filename
        elif filename == '.':
            filename = "dot"
        elif filename == '/':
            filename = "slash"
        elif filename == '\\':
            filename = "backslash"
        elif filename == '*':
            filename = "star"
        elif filename == ':':
            filename = "colon"
        elif filename == '?':
            filename = "questionmark"
        elif filename == '>':
            filename = "greater"
        elif filename == '<':
            filename = "smaller"
        elif filename == '|':
            filename = "pipe"

        main_program.cv2.imwrite(location_of_the_output_folder + filename + '.png', current_char_frame)
        box_index += 1

        #print("Assigned letter:", expected_text[box_index])
        #cv2.imshow("", current_char_frame)
        #cv2.waitKey(0)

    print("Reference Images were created!")


def main():
    # general example of usage:
    """generate_ref_images(0, "RefVideo", "ThetextinthevideoWithLinenumbersButWithoutWhitespaces", "RefImagesNew")"""

    # real examples of usage:
    """generate_ref_images(101,
                        "Beispielvideos/reference_snap-1_diff-0.mp4",
                        "1!''#$%&'()*+,-./20123456789:;<=>?3@ABCDEFGHIJKLMNO4PQRSTUVWXYZ[\\]"
                        "^_5`abcdefghijklmno6pqrstuvwxyz{|}~",
                        '/home/lukas/PycharmProjects/bachelorarbeit/RefImagesTwo/')"""
    """generate_ref_images(2873,
                        "Beispielvideos/LongVideo.mp4",
                        "1#include<stdio.h>23intmain(void){4unsignedsandwiches=2;5sandwiches=sandwiches+1;//"
                        "three6sandwiches=sandwiches-1;//two"
                        "7sandwiches=sandwiches*2;//four8sandwiches=(sandwiches+2)/2;"
                        "9printf(''%d'',sandwiches);10}",
                        '/home/lukas/PycharmProjects/bachelorarbeit/RefImagesOne/')"""

    main_program.end_routine(0)


if __name__ == '__main__':
    main()
