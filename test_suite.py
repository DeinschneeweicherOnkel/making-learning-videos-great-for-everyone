import argparse
import unittest

import main_program as main

# resets all parameters, global variables and caches
def reset_main_program():
    main.KNOWN_LINES_CACHE.clear()
    main.KNOWN_LINES_CACHE = [0]
    main.VIDEO = None
    main.VIDEO_WIDTH = None
    main.VIDEO_HEIGHT = None
    main.PAUSE_IN_SECONDS = 10
    main.VIDEO_FPS = None

    main.INTERVAL = None
    main.FRAME_CACHE_ORIGINAL.clear()
    main.FRAME_CACHE_ORIGINAL = {}
    main.FRAME_CACHE_FOR_TEXT.clear()
    main.FRAME_CACHE_FOR_TEXT = {}
    main.PIVOT_CACHE.clear()
    main.PIVOT_CACHE = {}
    main.CURSOR_POSITION_CACHE.clear()
    main.CURSOR_POSITION_CACHE = {}
    main.BOX_DATA_CACHE_BLANK.clear()
    main.BOX_DATA_CACHE_BLANK = {}
    main.BOX_DATA_CACHE_DEFAULT.clear()
    main.BOX_DATA_CACHE_DEFAULT = {}
    main.LINE_NUMBERS_CACHE.clear()
    main.LINE_NUMBERS_CACHE = {}
    main.TEXT_CACHE_WITH_LINE_NUMBERS.clear()
    main.TEXT_CACHE_WITH_LINE_NUMBERS = {}
    main.TEXT_CACHE_WITHOUT_LINE_NUMBERS.clear()
    main.TEXT_CACHE_WITHOUT_LINE_NUMBERS = {}
    main.CACHE_SEL.clear()
    main.CACHE_SEL = {}
    main.FRAME_CACHE_FOR_SEL.clear()
    main.FRAME_CACHE_FOR_SEL = {}
    main.TIME_ACCURACY = None

    main.USER_ARGS.clear()
    main.USER_ARGS = []
    main.VIDEO_START_TIME = 0
    main.VIDEO_END_TIME = None

    main.CROP = False
    main.CROP_AREA_LEFT = 0
    main.CROP_AREA_RIGHT = 0
    main.CROP_AREA_TOP = 0
    main.CROP_AREA_BOTTOM = 0
    main.ARGS = None

    main.BACKGROUND_COLOR = None
    main.WHITE_BACKGROUND = None

    main.REF_IMAGES_FOLDER = None
    main.REF_IMAGES.clear()
    main.REF_IMAGES = []
    main.CHECK_LAST_NEWLINE = False
    main.DEBUG_FLAG = False
    main.INDEX_BEFORE_ZOOM = None
    main.LINE_NUMBERS_VISIBLE_CACHE.clear()
    main.LINE_NUMBERS_VISIBLE_CACHE = {}
    main.COL_OFFSET_CACHE.clear()
    main.COL_OFFSET_CACHE = {}
    main.CURSOR_INDEX_CACHE.clear()
    main.CURSOR_INDEX_CACHE = {}

# execute all testcases
if __name__ == '__main__':
    unittest.main()

#####################################################################################
#####################################################################################
# SAMPLE TESTCASES
# only tests single frames but only takes few seconds
# should be executed before TestRealActions()
#####################################################################################
#####################################################################################
class TestOnlySamples(unittest.TestCase):
    def test_frameTest1(self):
        reset_main_program()

        expected = "g\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\nv"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/scroll-test_snap-2_diff-0-1.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(20))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(20))

    def test_ReferenceFrame(self):
        reset_main_program()
        expected = "1 !"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/reference_snap-1_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(3, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(3))

    def test_ReferenceFrame2(self):
        reset_main_program()
        expected = "1 !\"#$%&\'()*+,-./\n" \
                   "20123456789:;<=>?\n" \
                   "3@ABCDEFGHIJKLMNO\n" \
                   "4PQRSTUVWXYZ[\]^_"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/reference_snap-1_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(68, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(68))

    def test_ReferenceFrame3(self):
        reset_main_program()
        expected = "1 !\"#$%&\'()*+,-./\n" \
                   "2"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/reference_snap-1_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(18, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(18))

    def test_ReferenceTotal(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': ' !"#$%&\'()*+,-./\n'
                                            '0123456789:;<=>?\n'
                                            '@ABCDEFGHIJKLMNO\n'
                                            'PQRSTUVWXYZ[\]^_\n'
                                            '`abcdefghijklmno\n'
                                            'pqrstuvwxyz{|}~', 'begin-frameindex': 2, 'rowStart': 1, 'colStart': 1,
                     'end-frameindex': 101,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 5}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/reference_snap-1_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []
        self.assertEqual(expected, main.find_select_actions())

    def test_frameTest2(self):
        reset_main_program()

        expected = "#include <stdio.h>\n\nint main(void) {\n  unsigned sandwiches = 2;\n  sandwiches = sandwiches + 1; // three\n  sandwiches = sandwiches - \n}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(1344))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(1344))

    def test_frameTest3(self):
        reset_main_program()

        expected = "#include <stdio.h>\n\nint main(void) {\n\n}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(25))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(25))

    def test_frameTest4(self):
        reset_main_program()

        expected = "void printCountdown(unsigned start)\n{\n  for (; start >= 0; start--)\n  {\n    printf(\"%d\\n\", start);\n  }\n}\n\n\nint main()\n{\n  printf(\"--- Countdown 1 ---\\n\");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf(\"%d\\n\", i);"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/zoom.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(0))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(0))

    def test_frameTest5(self):
        reset_main_program()

        expected = "main()\n\nintf(\"--- Countdown 1 -"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/zoom.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(25))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(25))

    def test_frameTest6(self):
        reset_main_program()

        expected = []

        args = argparse.Namespace()
        args.name = 'Beispielvideos/dummy-lesson-merged.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.check_what_selected(0))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(0))

    def test_frameTest7(self):
        reset_main_program()

        expected = "halli\n  hallo  du"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/dummy-lesson-merged.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(25))
        # SELECT
        expected = [{'char': ' ', 'char_index': 7, 'rowStart': 2},
                    {'char': ' ', 'char_index': 8, 'rowStart': 2},
                    {'char': 'd', 'char_index': 9, 'rowStart': 2},
                    {'char': 'u', 'char_index': 10, 'rowStart': 2}]
        self.assertEqual(expected, main.check_what_selected(25))

    def test_frameTest8(self):
        reset_main_program()

        expected = [{'char': 'l', 'rowStart': 1, 'char_index': 2}, {'char': 'l', 'rowStart': 2, 'char_index': 4},
                    {'char': ' ', 'rowStart': 2, 'char_index': 7}, {'char': ' ', 'rowStart': 2, 'char_index': 8},
                    {'char': 'd', 'rowStart': 2, 'char_index': 9}, {'char': 'u', 'rowStart': 2, 'char_index': 10}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/dummy-lesson-merged.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.check_what_selected(27))
        # SELECT
        expected = [{'char': 'l', 'char_index': 2, 'rowStart': 1}, {'char': 'l', 'char_index': 4, 'rowStart': 2},
                    {'char': ' ', 'char_index': 7, 'rowStart': 2},
                    {'char': ' ', 'char_index': 8, 'rowStart': 2},
                    {'char': 'd', 'char_index': 9, 'rowStart': 2},
                    {'char': 'u', 'char_index': 10, 'rowStart': 2}]
        self.assertEqual(expected, main.check_what_selected(27))

    def test_frameTest9(self):
        reset_main_program()

        expected = [{'char': 'h', 'rowStart': 1, 'char_index': 0}, {'char': 'l', 'rowStart': 1, 'char_index': 2},
                    {'char': 'l', 'rowStart': 1, 'char_index': 3}, {'char': ' ', 'rowStart': 2, 'char_index': 0},
                    {'char': ' ', 'rowStart': 2, 'char_index': 1}, {'char': 'l', 'rowStart': 2, 'char_index': 4},
                    {'char': 'l', 'rowStart': 2, 'char_index': 5}, {'char': ' ', 'rowStart': 2, 'char_index': 7},
                    {'char': ' ', 'rowStart': 2, 'char_index': 8}, {'char': 'd', 'rowStart': 2, 'char_index': 9},
                    {'char': 'u', 'rowStart': 2, 'char_index': 10}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/dummy-lesson-merged.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.check_what_selected(31))
        # SELECT
        expected = [{'char': 'h', 'char_index': 0, 'rowStart': 1},
                    {'char': 'l', 'char_index': 2, 'rowStart': 1},
                    {'char': 'l', 'char_index': 3, 'rowStart': 1},
                    {'char': ' ', 'char_index': 0, 'rowStart': 2},
                    {'char': ' ', 'char_index': 1, 'rowStart': 2},
                    {'char': 'l', 'char_index': 4, 'rowStart': 2},
                    {'char': 'l', 'char_index': 5, 'rowStart': 2},
                    {'char': ' ', 'char_index': 7, 'rowStart': 2},
                    {'char': ' ', 'char_index': 8, 'rowStart': 2},
                    {'char': 'd', 'char_index': 9, 'rowStart': 2},
                    {'char': 'u', 'char_index': 10, 'rowStart': 2}]
        self.assertEqual(expected, main.check_what_selected(31))

    def test_frameTest10(self):
        reset_main_program()

        expected = "#include <stdio.h>\n\nint main(void) {\n  unsigned sandwiches = 2;\n  sandwiches = sandwiches + 1; // three\n  sandwiches = sandwiches - 1; // two\n  sandwiches = sandwiches * 2; // four\n  sandwiches = sandwiches + 2 / 2;\n}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(2397))
        # SELECT
        expected = [{'char': 's', 'char_index': 15, 'rowStart': 8},
                    {'char': 'a', 'char_index': 16, 'rowStart': 8},
                    {'char': 'n', 'char_index': 17, 'rowStart': 8},
                    {'char': 'd', 'char_index': 18, 'rowStart': 8},
                    {'char': 'w', 'char_index': 19, 'rowStart': 8},
                    {'char': 'i', 'char_index': 20, 'rowStart': 8}]
        self.assertEqual(expected, main.check_what_selected(2397))

    def test_frameTest11(self):
        reset_main_program()

        expected = "#include <stdio.h>\n\nint main(void) {\n  unsigned sandwiches = 2;\n  sandwiches = sandwiches + 1; // three\n  sandwiches = sandwiches - 1; // two\n  sandwiches = sandwiches * 2; // four\n  sandwiches = sandwiches + 2 / 2;\n}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(2191))
        # SELECT
        expected = [{'char': '2', 'char_index': 28, 'rowStart': 8},
                    {'char': ' ', 'char_index': 29, 'rowStart': 8},
                    {'char': '/', 'char_index': 30, 'rowStart': 8},
                    {'char': ' ', 'char_index': 31, 'rowStart': 8},
                    {'char': '2', 'char_index': 32, 'rowStart': 8}]
        self.assertEqual(expected, main.check_what_selected(2191))

    def test_frameTest12(self):
        reset_main_program()

        expected = "#include <stdio.h>\n\nint main(void) {\n  1 + 2;\n}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(98))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(98))

    def test_frameTest13(self):
        reset_main_program()

        expected = [{'char': 's', 'rowStart': 8, 'char_index': 15}, {'char': 'a', 'rowStart': 8, 'char_index': 16},
                    {'char': 'n', 'rowStart': 8, 'char_index': 17}, {'char': 'd', 'rowStart': 8, 'char_index': 18},
                    {'char': 'w', 'rowStart': 8, 'char_index': 19}, {'char': 'i', 'rowStart': 8, 'char_index': 20},
                    {'char': 'c', 'rowStart': 8, 'char_index': 21}, {'char': 'h', 'rowStart': 8, 'char_index': 22},
                    {'char': 'e', 'rowStart': 8, 'char_index': 23}, {'char': 's', 'rowStart': 8, 'char_index': 24},
                    {'char': ' ', 'rowStart': 8, 'char_index': 25}, {'char': '+', 'rowStart': 8, 'char_index': 26},
                    {'char': ' ', 'rowStart': 8, 'char_index': 27}, {'char': '2', 'rowStart': 8, 'char_index': 28}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.check_what_selected(2441))
        # SELECT
        expected = [{'char': 's', 'char_index': 15, 'rowStart': 8},
                    {'char': 'a', 'char_index': 16, 'rowStart': 8},
                    {'char': 'n', 'char_index': 17, 'rowStart': 8},
                    {'char': 'd', 'char_index': 18, 'rowStart': 8},
                    {'char': 'w', 'char_index': 19, 'rowStart': 8},
                    {'char': 'i', 'char_index': 20, 'rowStart': 8},
                    {'char': 'c', 'char_index': 21, 'rowStart': 8},
                    {'char': 'h', 'char_index': 22, 'rowStart': 8},
                    {'char': 'e', 'char_index': 23, 'rowStart': 8},
                    {'char': 's', 'char_index': 24, 'rowStart': 8},
                    {'char': ' ', 'char_index': 25, 'rowStart': 8},
                    {'char': '+', 'char_index': 26, 'rowStart': 8},
                    {'char': ' ', 'char_index': 27, 'rowStart': 8},
                    {'char': '2', 'char_index': 28, 'rowStart': 8},
                    ]
        self.assertEqual(expected, main.check_what_selected(2441))

    def test_frameTest14(self):
        reset_main_program()

        expected = "{\n  for (; start >= 0; start--)\n  {\n    printf(\"%d\\n\", start);\n  }\n}\n\n\nint main()\n{\n  printf(\"--- Countdown 1 ---\\n\");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf(\"%d\\n\", i);\n  }"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-3_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(6))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(6))

    def test_frameTest15(self):
        reset_main_program()

        expected = "n\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz\na\nb\n"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/scroll-test_snap-2_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(51))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(51))

    def test_frameTest16(self):
        reset_main_program()

        expected = "\nvoid printCountdown(int start)\n{\n  for (; start >= 0; start-int main()\n{\n  printf(\"--- Countdown 1 ---\\n\");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf(\"%d\\n\", i);\n  }\n\n\n  printf(\"--- Countdown 2 ---\\n\");\n\n  for (int i = 10; i >= 0; i--)"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/Webcam1_short.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(351))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(351))

    def test_frameTest17(self):
        reset_main_program()

        expected = "main(void)\n\nintf(\"--- Countdown 1 -"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/zoom.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(43))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(43))

    def test_frameTest18(self):
        reset_main_program()

        expected = "1#include <stdio.h>\n" \
                   "2\n" \
                   "3int main(void) {\n" \
                   "4  unsigned sandwiches = 2;\n" \
                   "5  sandwiches = sandwiches + 1; \n" \
                   "6}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(1121, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(1121))

    def test_frameTest19(self):
        reset_main_program()

        expected = "1#include <stdio.h>\n" \
                   "2\n" \
                   "3int main(void) {\n" \
                   "4  unsigned sandwiches = 2;\n" \
                   "5  sandwiches = sandwiches + 1; // three\n" \
                   "6  sandwiches = sandwiches - 1; // two\n" \
                   "7  sandwiches = sandwiches * 2; // four\n" \
                   "8  sandwiches = (sandwiches + 2) / 2;\n" \
                   "9  printf(\"%d\", \n" \
                   "10}"

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(2834, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(2834))

    def test_frameTest20(self):
        reset_main_program()

        expected = '1#include <stdio.h>\n2\n3int main()\n4{\n5  printf("--- Countdown 1 ---\\n");\n6\n7  for (int i = 10; i >= 0; i--)\n8  {\n9    printf("%d\\n", i);\n10  }\n11\n12\n13  printf("--- Countdown 2 ---\\n");\n14\n15  for (int i = 10; i >= 0; i--)\n16  {'

        args = argparse.Namespace()
        args.name = 'Beispielvideos/webcam3.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(300, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(300))

    def test_frameTest21(self):
        reset_main_program()

        expected = '  {\n    printf("%d\\n", start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  '
        args = argparse.Namespace()
        args.name = 'Beispielvideos/zoom.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.get_text(3, False))
        # SELECT
        expected = []
        self.assertEqual(expected, main.check_what_selected(3))


#####################################################################################
#####################################################################################
# ACTION TESTCASES
# takes longer but tests real actions
# should be executed after basic TestOnlySamples()
#####################################################################################
#####################################################################################
class TestRealActions(unittest.TestCase):

    #@unittest.skip
    def test_FullMergedVideo(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': '#include <stdio.h>\n\nint main(void) {\n\n}', 'rowStart': 1, 'colStart': 1,
                  'begin-frameindex': 24, 'end-frameindex': 25, 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1},
                 {'type': 'add', 'what': '  1 + 2;', 'rowStart': 4, 'colStart': 1, 'begin-frameindex': 71,
                  'end-frameindex': 98, 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 2, 'end-hour': 0, 'end-minute': 0, 'end-second': 4},
                 {'type': 'add', 'what': 'int result = ', 'rowStart': 4, 'colStart': 3, 'begin-frameindex': 235,
                  'end-frameindex': 273, 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 9, 'end-hour': 0, 'end-minute': 0, 'end-second': 11},
                 {'type': 'remove', 'what': 'int result = 1 + 2;', 'begin-frameindex': 377, 'rowStart': 4,
                  'colStart': 3,
                  'end-frameindex': 438, 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 15, 'end-hour': 0, 'end-minute': 0, 'end-second': 18},
                 {'type': 'add', 'what': 'unsigned sandwiches = 2;\n  sandwiches + 1;', 'rowStart': 4, 'colStart': 3,
                  'begin-frameindex': 502, 'end-frameindex': 702, 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 20, 'end-hour': 0, 'end-minute': 0, 'end-second': 29},
                 {'type': 'add', 'what': 'sandwiches = ', 'rowStart': 5, 'colStart': 3, 'begin-frameindex': 868,
                  'end-frameindex': 911, 'note:': 'Solved with cursor', 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 34, 'end-hour': 0, 'end-minute': 0, 'end-second': 37},
                 {'type': 'add',
                  'what': ' // three\n  sandwiches = sandwiches - 1; // two\n  sandwiches = sandwiches + sandwiches;',
                  'rowStart': 5, 'colStart': 31, 'begin-frameindex': 1118, 'end-frameindex': 1525, 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 44, 'end-hour': 0, 'end-minute': 1, 'end-second': 1},
                 {'type': 'remove', 'what': '+ sandwiches;', 'begin-frameindex': 1553, 'rowStart': 7, 'colStart': 27,
                  'end-frameindex': 1593, 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 2, 'end-hour': 0, 'end-minute': 1, 'end-second': 4},
                 {'type': 'add', 'what': '* 2; // four\n  sandwiches = sandwiches / 2;', 'rowStart': 7, 'colStart': 27,
                  'begin-frameindex': 1705, 'end-frameindex': 1943, 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 8, 'end-hour': 0, 'end-minute': 1, 'end-second': 18},
                 {'type': 'add', 'what': '+ 2 ', 'rowStart': 8, 'colStart': 27, 'begin-frameindex': 2020,
                  'end-frameindex': 2029, 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 20, 'end-hour': 0, 'end-minute': 1, 'end-second': 22},
                 {'type': 'add', 'what': '(', 'rowStart': 8, 'colStart': 16, 'begin-frameindex': 2513,
                  'end-frameindex': 2514, 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 40, 'end-hour': 0, 'end-minute': 1, 'end-second': 41},
                 {'type': 'add', 'what': ')', 'rowStart': 8, 'colStart': 31, 'begin-frameindex': 2663,
                  'end-frameindex': 2664, 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 46, 'end-hour': 0, 'end-minute': 1, 'end-second': 47},
                 {'type': 'add', 'what': '\n  printf("%d", sandwiches);', 'rowStart': 8, 'colStart': 37,
                  'begin-frameindex': 2783, 'end-frameindex': 2873, 'note:': 'Solved with cursor', 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 51, 'end-hour': 0, 'end-minute': 1, 'end-second': 55}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/LongVideo.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesOne'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = [{'type': 'select', 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 38, 'end-hour': 0, 'end-minute': 0, 'end-second': 44, 'begin-frameindex': 957, 'colStart': 16, 'end-frameindex': 1097, 'rowStart': 5, 'selected_chars': [{'char': 's', 'rowStart': 5, 'char_index': 15}, {'char': 'a', 'rowStart': 5, 'char_index': 16}, {'char': 'n', 'rowStart': 5, 'char_index': 17}, {'char': 'd', 'rowStart': 5, 'char_index': 18}, {'char': 'w', 'rowStart': 5, 'char_index': 19}, {'char': 'i', 'rowStart': 5, 'char_index': 20}, {'char': 'c', 'rowStart': 5, 'char_index': 21}, {'char': 'h', 'rowStart': 5, 'char_index': 22}, {'char': 'e', 'rowStart': 5, 'char_index': 23}, {'char': 's', 'rowStart': 5, 'char_index': 24}, {'char': ' ', 'rowStart': 5, 'char_index': 25}, {'char': '+', 'rowStart': 5, 'char_index': 26}, {'char': ' ', 'rowStart': 5, 'char_index': 27}, {'char': '1', 'rowStart': 5, 'char_index': 28}]},
                {'type': 'unselect', 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 43, 'end-hour': 0, 'end-minute': 0, 'end-second': 44, 'begin-frameindex': 1097, 'colStart': 16, 'end-frameindex': 1098, 'rowStart': 5, 'selected_chars': '', 'unselected_chars': [{'char': 's', 'rowStart': 5, 'char_index': 15}, {'char': 'a', 'rowStart': 5, 'char_index': 16}, {'char': 'n', 'rowStart': 5, 'char_index': 17}, {'char': 'd', 'rowStart': 5, 'char_index': 18}, {'char': 'w', 'rowStart': 5, 'char_index': 19}, {'char': 'i', 'rowStart': 5, 'char_index': 20}, {'char': 'c', 'rowStart': 5, 'char_index': 21}, {'char': 'h', 'rowStart': 5, 'char_index': 22}, {'char': 'e', 'rowStart': 5, 'char_index': 23}, {'char': 's', 'rowStart': 5, 'char_index': 24}, {'char': ' ', 'rowStart': 5, 'char_index': 25}, {'char': '+', 'rowStart': 5, 'char_index': 26}, {'char': ' ', 'rowStart': 5, 'char_index': 27}, {'char': '1', 'rowStart': 5, 'char_index': 28}]},
                {'type': 'select', 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 26, 'end-hour': 0, 'end-minute': 1, 'end-second': 32, 'begin-frameindex': 2171, 'colStart': 29, 'end-frameindex': 2297, 'rowStart': 8, 'selected_chars': [{'char': '2', 'rowStart': 8, 'char_index': 28}, {'char': ' ', 'rowStart': 8, 'char_index': 29}, {'char': '/', 'rowStart': 8, 'char_index': 30}, {'char': ' ', 'rowStart': 8, 'char_index': 31}, {'char': '2', 'rowStart': 8, 'char_index': 32}]},
                {'type': 'unselect', 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 31, 'end-hour': 0, 'end-minute': 1, 'end-second': 32, 'begin-frameindex': 2297, 'colStart': 29, 'end-frameindex': 2298, 'rowStart': 8, 'selected_chars': '', 'unselected_chars': [{'char': '2', 'rowStart': 8, 'char_index': 28}, {'char': ' ', 'rowStart': 8, 'char_index': 29}, {'char': '/', 'rowStart': 8, 'char_index': 30}, {'char': ' ', 'rowStart': 8, 'char_index': 31}, {'char': '2', 'rowStart': 8, 'char_index': 32}]},
                {'type': 'select', 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 35, 'end-hour': 0, 'end-minute': 1, 'end-second': 38, 'begin-frameindex': 2378, 'colStart': 16, 'end-frameindex': 2447, 'rowStart': 8, 'selected_chars': [{'char': 's', 'rowStart': 8, 'char_index': 15}, {'char': 'a', 'rowStart': 8, 'char_index': 16}, {'char': 'n', 'rowStart': 8, 'char_index': 17}, {'char': 'd', 'rowStart': 8, 'char_index': 18}, {'char': 'w', 'rowStart': 8, 'char_index': 19}, {'char': 'i', 'rowStart': 8, 'char_index': 20}, {'char': 'c', 'rowStart': 8, 'char_index': 21}, {'char': 'h', 'rowStart': 8, 'char_index': 22}, {'char': 'e', 'rowStart': 8, 'char_index': 23}, {'char': 's', 'rowStart': 8, 'char_index': 24}, {'char': ' ', 'rowStart': 8, 'char_index': 25}, {'char': '+', 'rowStart': 8, 'char_index': 26}, {'char': ' ', 'rowStart': 8, 'char_index': 27}, {'char': '2', 'rowStart': 8, 'char_index': 28}]},
                {'type': 'unselect', 'begin-hour': 0, 'begin-minute': 1, 'begin-second': 37, 'end-hour': 0, 'end-minute': 1, 'end-second': 38, 'begin-frameindex': 2447, 'colStart': 16, 'end-frameindex': 2448, 'rowStart': 8, 'selected_chars': '', 'unselected_chars': [{'char': 's', 'rowStart': 8, 'char_index': 15}, {'char': 'a', 'rowStart': 8, 'char_index': 16}, {'char': 'n', 'rowStart': 8, 'char_index': 17}, {'char': 'd', 'rowStart': 8, 'char_index': 18}, {'char': 'w', 'rowStart': 8, 'char_index': 19}, {'char': 'i', 'rowStart': 8, 'char_index': 20}, {'char': 'c', 'rowStart': 8, 'char_index': 21}, {'char': 'h', 'rowStart': 8, 'char_index': 22}, {'char': 'e', 'rowStart': 8, 'char_index': 23}, {'char': 's', 'rowStart': 8, 'char_index': 24}, {'char': ' ', 'rowStart': 8, 'char_index': 25}, {'char': '+', 'rowStart': 8, 'char_index': 26}, {'char': ' ', 'rowStart': 8, 'char_index': 27}, {'char': '2', 'rowStart': 8, 'char_index': 28}]}]

        self.assertEqual(expected, main.find_select_actions())

    def test_SelectionMovingCauseAdd(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 0, 'rowStart': 1, 'what': '#include <stdio.h>\n\nint main(void) {\n  2 / 2\n}', 'hour': 0, 'minute': 0, 'second': 0},
                    {'type': 'add', 'what': 'unsigned sandwiches = 2;\n  sandwiches + 1;', 'rowStart': 4, 'colStart': 3, 'begin-frameindex': 62, 'end-frameindex': 262,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 2, 'end-hour': 0, 'end-minute': 0, 'end-second': 11}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/SelectionMoving.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = [{'type': 'select', 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 14, 'begin-frameindex': 2, 'colStart': 3, 'end-frameindex': 348, 'rowStart': 4, 'selected_chars': [{'char': '2', 'rowStart': 5, 'char_index': 17}, {'char': ' ', 'rowStart': 5, 'char_index': 18}, {'char': '/', 'rowStart': 5, 'char_index': 19}, {'char': ' ', 'rowStart': 5, 'char_index': 20}, {'char': '2', 'rowStart': 5, 'char_index': 21}]},
                     {'type': 'unselect', 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 13, 'end-hour': 0, 'end-minute': 0, 'end-second': 14, 'begin-frameindex': 348, 'colStart': 18, 'end-frameindex': 349, 'rowStart': 5, 'selected_chars': '', 'unselected_chars': [{'char': '2', 'rowStart': 5, 'char_index': 17}, {'char': ' ', 'rowStart': 5, 'char_index': 18}, {'char': '/', 'rowStart': 5, 'char_index': 19}, {'char': ' ', 'rowStart': 5, 'char_index': 20}, {'char': '2', 'rowStart': 5, 'char_index': 21}]}]

        self.assertEqual(expected, main.find_select_actions())


    def test_SelectionMovingCauseRemove(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 0, 'rowStart': 1, 'what': '#include <stdio.h>\n\nint main(void) {\n  unsigned sandwiches = 2;\n  sandwiches + 1;2 / 2\n}',
                     'hour': 0, 'minute': 0, 'second': 0},
                    {'type': 'remove', 'what': 'unsigned sandwiches = 2;\n  sandwiches + 1;', 'rowStart': 4, 'colStart': 3, 'begin-frameindex': 106, 'end-frameindex': 306,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 4, 'end-hour': 0, 'end-minute': 0, 'end-second': 13}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/SelectionMovingReverse.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = [{'begin-frameindex': 19, 'colStart': 18, 'end-frameindex': 351, 'rowStart': 5,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 15,
                      'selected_chars': [{'char': '2', 'char_index': 2, 'rowStart': 4}, {'char': ' ', 'char_index': 3, 'rowStart': 4}, {'char': '/', 'char_index': 4, 'rowStart': 4}, {'char': ' ', 'char_index': 5, 'rowStart': 4}, {'char': '2', 'char_index': 6, 'rowStart': 4}], 'type': 'select'},
                     {'begin-frameindex': 351, 'colStart': 3, 'end-frameindex': 366, 'rowStart': 4,
                      'begin-hour': 0, 'begin-minute': 0, 'begin-second': 14, 'end-hour': 0, 'end-minute': 0, 'end-second': 15,
                      'selected_chars': '', 'type': 'unselect', 'unselected_chars': [{'char': '2', 'char_index': 2, 'rowStart': 4}, {'char': ' ', 'char_index': 3, 'rowStart': 4}, {'char': '/', 'char_index': 4, 'rowStart': 4}, {'char': ' ', 'char_index': 5, 'rowStart': 4}, {'char': '2', 'char_index': 6, 'rowStart': 4}]}]

        self.assertEqual(expected, main.find_select_actions())


    def test_TwoSelectionsAtOnce(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 0, 'rowStart': 1, 'what': '#include <stdio.h>\n\nint main(void) {\n  unsigned sandwiches = 2;\n  sandwiches = sandwiches + 1;\n} sandwiches = sandwiches + 1;',
                     'hour': 0, 'minute': 0, 'second': 0}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/DoubleSelection.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = [{'type': 'select', 'begin-frameindex': 40, 'colStart': 16, 'end-frameindex': 180, 'rowStart': 5,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 1, 'end-hour': 0, 'end-minute': 0, 'end-second': 8,
                     'selected_chars': [{'char': 's', 'rowStart': 5, 'char_index': 15}, {'char': 'a', 'rowStart': 5, 'char_index': 16}, {'char': 'n', 'rowStart': 5, 'char_index': 17}, {'char': 'd', 'rowStart': 5, 'char_index': 18}, {'char': 'w', 'rowStart': 5, 'char_index': 19}, {'char': 'i', 'rowStart': 5, 'char_index': 20}, {'char': 'c', 'rowStart': 5, 'char_index': 21}, {'char': 'h', 'rowStart': 5, 'char_index': 22}, {'char': 'e', 'rowStart': 5, 'char_index': 23}, {'char': 's', 'rowStart': 5, 'char_index': 24}, {'char': ' ', 'rowStart': 5, 'char_index': 25}, {'char': '+', 'rowStart': 5, 'char_index': 26}, {'char': ' ', 'rowStart': 5, 'char_index': 27}, {'char': '1', 'rowStart': 5, 'char_index': 28}]},

                    {'type': 'unselect', 'begin-frameindex': 180, 'colStart': 16, 'end-frameindex': 181, 'rowStart': 5,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 7, 'end-hour': 0, 'end-minute': 0, 'end-second': 8,
                     'selected_chars': '', 'unselected_chars': [{'char': 's', 'rowStart': 5, 'char_index': 15}, {'char': 'a', 'rowStart': 5, 'char_index': 16}, {'char': 'n', 'rowStart': 5, 'char_index': 17}, {'char': 'd', 'rowStart': 5, 'char_index': 18}, {'char': 'w', 'rowStart': 5, 'char_index': 19}, {'char': 'i', 'rowStart': 5, 'char_index': 20}, {'char': 'c', 'rowStart': 5, 'char_index': 21}, {'char': 'h', 'rowStart': 5, 'char_index': 22}, {'char': 'e', 'rowStart': 5, 'char_index': 23}, {'char': 's', 'rowStart': 5, 'char_index': 24}, {'char': ' ', 'rowStart': 5, 'char_index': 25}, {'char': '+', 'rowStart': 5, 'char_index': 26}, {'char': ' ', 'rowStart': 5, 'char_index': 27}, {'char': '1', 'rowStart': 5, 'char_index': 28}]},

                    {'type': 'select', 'begin-frameindex': 80, 'colStart': 16, 'end-frameindex': 213, 'rowStart': 6,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 3, 'end-hour': 0, 'end-minute': 0, 'end-second': 9,
                     'selected_chars': [{'char': 's', 'rowStart': 6, 'char_index': 15}, {'char': 'a', 'rowStart': 6, 'char_index': 16}, {'char': 'n', 'rowStart': 6, 'char_index': 17}, {'char': 'd', 'rowStart': 6, 'char_index': 18}, {'char': 'w', 'rowStart': 6, 'char_index': 19}, {'char': 'i', 'rowStart': 6, 'char_index': 20}, {'char': 'c', 'rowStart': 6, 'char_index': 21}, {'char': 'h', 'rowStart': 6, 'char_index': 22}, {'char': 'e', 'rowStart': 6, 'char_index': 23}, {'char': 's', 'rowStart': 6, 'char_index': 24}, {'char': ' ', 'rowStart': 6, 'char_index': 25}, {'char': '+', 'rowStart': 6, 'char_index': 26}, {'char': ' ', 'rowStart': 6, 'char_index': 27}, {'char': '1', 'rowStart': 6, 'char_index': 28}]},

                    {'type': 'unselect', 'begin-frameindex': 213, 'colStart': 16, 'end-frameindex': 214, 'rowStart': 6,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 8, 'end-hour': 0, 'end-minute': 0, 'end-second': 9,
                     'selected_chars': '', 'unselected_chars': [{'char': 's', 'rowStart': 6, 'char_index': 15}, {'char': 'a', 'rowStart': 6, 'char_index': 16}, {'char': 'n', 'rowStart': 6, 'char_index': 17}, {'char': 'd', 'rowStart': 6, 'char_index': 18}, {'char': 'w', 'rowStart': 6, 'char_index': 19}, {'char': 'i', 'rowStart': 6, 'char_index': 20}, {'char': 'c', 'rowStart': 6, 'char_index': 21}, {'char': 'h', 'rowStart': 6, 'char_index': 22}, {'char': 'e', 'rowStart': 6, 'char_index': 23}, {'char': 's', 'rowStart': 6, 'char_index': 24}, {'char': ' ', 'rowStart': 6, 'char_index': 25}, {'char': '+', 'rowStart': 6, 'char_index': 26}, {'char': ' ', 'rowStart': 6, 'char_index': 27}, {'char': '1', 'rowStart': 6, 'char_index': 28}]}]

        self.assertEqual(expected, main.find_select_actions())


    def test_AddTextAndMultiSelect(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': 'halli\n  hallo  du', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 18,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0,
                     'end-hour': 0, 'end-minute': 0, 'end-second': 1}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/dummy-lesson-merged.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = [{'type': 'select','begin-frameindex': 21, 'colStart': 8, 'end-frameindex': 31, 'rowStart': 2,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 2,
                     'selected_chars': [{'char': ' ', 'rowStart': 2, 'char_index': 7}, {'char': ' ', 'rowStart': 2, 'char_index': 8},{'char': 'd', 'rowStart': 2, 'char_index': 9}, {'char': 'u', 'rowStart': 2, 'char_index': 10}]},

                    {'type': 'select', 'begin-frameindex': 29, 'colStart': 1, 'end-frameindex': 31, 'rowStart': 1,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 1, 'end-hour': 0, 'end-minute': 0,
                     'end-second': 2,
                     'selected_chars': [{'char': 'h', 'rowStart': 1, 'char_index': 0}]},

                    {'type': 'select', 'begin-frameindex': 26, 'colStart': 3, 'end-frameindex': 31, 'rowStart': 1,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 1, 'end-hour': 0, 'end-minute': 0,
                     'end-second': 2,
                     'selected_chars': [{'char': 'l', 'rowStart': 1, 'char_index': 2},
                                        {'char': 'l', 'rowStart': 1, 'char_index': 3}]},

                    {'type': 'select', 'begin-frameindex': 29, 'colStart': 1, 'end-frameindex': 31, 'rowStart': 2,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 1, 'end-hour': 0, 'end-minute': 0,
                     'end-second': 2,
                     'selected_chars': [{'char': ' ', 'rowStart': 2, 'char_index': 0},
                                        {'char': ' ', 'rowStart': 2, 'char_index': 1}]},

                    {'type': 'select', 'begin-frameindex': 26, 'colStart': 5, 'end-frameindex': 31, 'rowStart': 2,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 1, 'end-hour': 0, 'end-minute': 0,
                     'end-second': 2,
                     'selected_chars': [{'char': 'l', 'rowStart': 2, 'char_index': 4},
                                        {'char': 'l', 'rowStart': 2, 'char_index': 5}]},
                    ]

        self.assertEqual(expected, main.find_select_actions())
    #@unittest.skip
    def test_ScrollTest(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': 'a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz\na\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 104,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 5}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/scroll-test_snap-1_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_ScrollTest2(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 0, 'rowStart': 38, 'what': 'l\nm\nn\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz\n',
                     'hour': 0, 'minute': 0, 'second': 0},
                    {'type': 'reveal', 'frameindex': 52, 'rowStart': 4, 'what': 'doe\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz\na\nb\n',
                     'hour': 0, 'minute': 0, 'second': 2},
                    {'type': 'add', 'what': 's this work?', 'rowStart': 4, 'colStart': 4, 'begin-frameindex': 52, 'end-frameindex': 64,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 2, 'end-hour': 0, 'end-minute': 0, 'end-second': 3}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/scroll-test_snap-2_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_ScrollTest3(self):
        reset_main_program()
        expected = [{'type': 'add',
                         'what': 'a\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz',
                         'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 52,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 3}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/scroll-test_snap-1_diff-0-1.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_ScrollTest4(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 23, 'rowStart': 4, 'what': 'd\ne\nf\ng\nh\ni\nj\nk\nl\nm\nn\no\np\nq\nr\ns\nt\nu\nv\nw\nx\ny\nz\n',
                     'hour': 0, 'minute': 0, 'second': 0},
                    {'type': 'add', 'what': 'oes this work?', 'rowStart': 4, 'colStart': 2, 'begin-frameindex': 24, 'end-frameindex': 38,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 2}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/scroll-test_snap-2_diff-0-1.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_Long1(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': '#include <stdio.h>\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }\n\n\n  printf("--- Countdown 2 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }',
                     'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 241,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 10},
                {'type': 'add', 'what': '  return 0;\n}', 'rowStart': 20, 'colStart': 1, 'begin-frameindex': 247, 'end-frameindex': 258,
                 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 9, 'end-hour': 0, 'end-minute': 0, 'end-second': 11}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-1_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())
    def test_Long2(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 19, 'rowStart': 3, 'what': 'int main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }\n\n\n  printf("--- Countdown 2 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }\n\n  return 0;\n}\n',
                     'hour': 0, 'minute': 0, 'second': 0},
                    {'type': 'add', 'what': '\nvoid printCountdown(int start)\n{\n  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\n',
                     'rowStart': 3, 'colStart': 1, 'begin-frameindex': 19, 'end-frameindex': 122,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 5}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-2_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_Long3(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 6, 'rowStart': 3, 'what':
            '\n'
            'void printCountdown(int start)\n'
            '{\n  for (; start >= 0; start--)\n'
            '  {\n'
            '    printf("%d\\n", start);\n'
            '  }\n'
            '}\n'
            '\n'
            '\nint main()\n'
            '{\n'
            '  printf("--- Countdown 1 ---\\n");\n'
            '\n'
            '  for (int i = 10; i >= 0; i--)\n'
            '  {\n'
            '    printf("%d\\n", i);\n'
            '  }',
            'hour': 0, 'minute': 0, 'second': 0},

{'type': 'reveal', 'frameindex': 9, 'rowStart': 23, 'what': '  printf("--- Countdown 2 ---\\n");', 'hour': 0, 'minute': 0, 'second': 0},
{'type': 'reveal', 'frameindex': 14, 'rowStart': 25, 'what': '  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }', 'hour': 0, 'minute': 0, 'second': 0},
{'type': 'add', 'what': '  printf("--- Countdown 3 ---\\n");\n  printCountdown(10);\n', 'rowStart': 30, 'colStart': 1,
 'begin-frameindex': 20, 'end-frameindex': 76,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 4}]
        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-3_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_Long4(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 27, 'rowStart': 4, 'what':
                'void printCountdown(int start)\n'
                '{\n'
                '  for (; start >= 0; start--)\n'
                '  {\n'
                '    printf("%d\\n", start);\n'
                '  }\n'
                '}\n'
                '\n'
                '\n'
                'int main()\n'
                '{\n'
                '  printf("--- Countdown 1 ---\\n");\n'
                '\n'
                '  for (int i = 10; i >= 0; i--)\n'
                '  {\n'
                '    printf("%d\\n", i);\n'
                '  }\n'
                '\n'
                '\n'
                '  printf("--- Countdown 2 ---\\n");\n'
                '\n'
                '  for (int i = 10; i >= 0; i--)\n'
                '  {\n'
                '    printf("%d\\n", i);\n'
                '  }\n'
                '\n'
                '  printf("--- Countdown 3 ---\\n");\n'
                '  printCountdown(10);\n',
                'hour': 0, 'minute': 0, 'second': 1},

        {'type': 'remove', 'what': 'int', 'begin-frameindex': 50, 'rowStart': 4, 'colStart': 21, 'end-frameindex': 53,
         'begin-hour': 0, 'begin-minute': 0, 'begin-second': 2, 'end-hour': 0, 'end-minute': 0, 'end-second': 3}]
        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-4_diff-0.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_Long5(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 0, 'rowStart': 4, 'what':
            'void printCountdown( start)\n'
            '{\n'
            '  for (; start >= 0; start--)\n'
            '  {\n'
            '    printf("%d\\n", start);\n'
            '  }\n'
            '}\n'
            '\n'
            '\n'
            'int main()\n'
            '{\n'
            '  printf("--- Countdown 1 ---\\n");\n'
            '\n'
            '  for (int i = 10; i >= 0; i--)\n'
            '  {\n'
            '    printf("%d\\n", i);',
            'hour': 0, 'minute': 0, 'second': 0},

{'type': 'add', 'what': 'unsigned', 'rowStart': 4, 'colStart': 21, 'begin-frameindex': 0, 'end-frameindex': 8,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-4_diff-1.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_Long6(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'frameindex': 0, 'rowStart': 4, 'what':
            'void printCountdown(unsigned start)\n'
            '{\n'
            '  for (; start >= 0; start--)\n'
            '  {\n'
            '    printf("%d\\n", start);\n'
            '  }\n'
            '}\n'
            '\n'
            '\n'
            'int main()\n'
            '{\n'
            '  printf("--- Countdown 1 ---\\n");\n'
            '\n'
            '  for (int i = 10; i >= 0; i--)\n'
            '  {\n'
            '    printf("%d\\n", i);',
            'hour': 0, 'minute': 0, 'second': 0},
{'type': 'add', 'what': 'void', 'rowStart': 13, 'colStart': 10, 'begin-frameindex': 17, 'end-frameindex': 21,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1}]
        args = argparse.Namespace()
        args.name = 'Beispielvideos/long_snap-4_diff-2.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_webcam1_Long(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': '#include <stdio.h>\n'
                                            '\n'
                                            'int main()\n'
                                            '{\n'
                                            '  printf("--- Countdown 1 ---\\n");\n'
                                            '\n'
                                            '  for (int i = 10; i >= 0; i--)\n'
                                            '  {\n'
                                            '    printf("%d\\n", i);\n'
                                            '  }\n'
                                            '\n'
                                            '\n'
                                            '  printf("--- Countdown 2 ---\\n");\n'
                                            '\n'
                                            '  for (int i = 10; i >= 0; i--)\n'
                                            '  {\n'
                                            '    printf("%d\\n", i);\n'
                                            '  }', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 241,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 10},
{'type': 'add', 'what':
    '  return 0;\n}',
 'rowStart': 20, 'colStart': 1, 'begin-frameindex': 247, 'end-frameindex': 258,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 9, 'end-hour': 0, 'end-minute': 0, 'end-second': 11},
{'type': 'add', 'what':
    '\n'
    'void printCountdown(int start)\n'
    '{\n'
    '  for (; start >= 0; start--)\n'
    '  {\n'
    '    printf("%d\\n", start);\n'
    '  }\n'
    '}\n'
    '\n'
    '\n', 'rowStart': 3, 'colStart': 1, 'begin-frameindex': 369, 'end-frameindex': 472,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 14, 'end-hour': 0, 'end-minute': 0, 'end-second': 19}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/Webcam1_long.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_webcam1_Short(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': '#include <stdio.h>\n'
                                            '\n'
                                            'int main()\n'
                                            '{\n'
                                            '  printf("--- Countdown 1 ---\\n");\n'
                                            '\n'
                                            '  for (int i = 10; i >= 0; i--)\n'
                                            '  {\n'
                                            '    printf("%d\\n", i);\n'
                                            '  }\n'
                                            '\n'
                                            '\n'
                                            '  printf("--- Countdown 2 ---\\n");\n'
                                            '\n'
                                            '  for (int i = 10; i >= 0; i--)\n'
                                            '  {\n'
                                            '    printf("%d\\n", i);\n'
                                            '  }', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 241,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 10},
{'type': 'add', 'what':
    '  return 0;\n}',
 'rowStart': 20, 'colStart': 1, 'begin-frameindex': 247, 'end-frameindex': 258,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 9, 'end-hour': 0, 'end-minute': 0, 'end-second': 11},
{'type': 'add', 'what':
    '\n'
    'void printCountdown(int start)\n'
    '{\n'
    '  for (; start >= 0; start--)\n'
    '  {\n'
    '    printf("%d\\n", start);\n'
    '  }\n'
    '}\n'
    '\n'
    '\n', 'rowStart': 3, 'colStart': 1, 'begin-frameindex': 290, 'end-frameindex': 393,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 11, 'end-hour': 0, 'end-minute': 0, 'end-second': 16}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/Webcam1_short.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_webcam2_Long(self):
        reset_main_program()
        expected = [    {'type': 'add', 'what':
                '#include <stdio.h>\n'
                '\n'
                'int main()\n'
                '{\n'
                '  printf("--- Countdown 1 ---\\n");\n'
                '\n'
                '  for (int i = 10; i >= 0; i--)\n'
                '  {\n'
                '    printf("%d\\n", i);\n'
                '  }\n'
                '\n'
                '\n'
                '  printf("--- Countdown 2 ---\\n");\n'
                '\n'
                '  for (int i = 10; i >= 0; i--)\n'
                '  {\n'
                '    printf("%d\\n", i);\n'
                '  }',
     'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 241,
    'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 10},
    {'type': 'add', 'what': '  return 0;\n}', 'rowStart': 20, 'colStart': 1, 'begin-frameindex': 247,
     'end-frameindex': 258,
     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 9, 'end-hour': 0, 'end-minute': 0, 'end-second': 11},
    {'type': 'reveal', 'frameindex': 356, 'rowStart': 3,
     'what': '\n'
             'void printCountdown(int start)\n'
             '{\n'
             '  for (; start >= 0; start--)\n'
             '  {\n'
             '    printf("%d\\n", start);\n'
             '  }\n'
             '}\n'
             '\n'
             '\n'
             'int main()\n'
             '{\n'
             '  printf("--- Countdown 1 ---\\n");\n'
             '\n'
             '  for (int i = 10; i >= 0; i--)\n'
             '  {\n'
             '    printf("%d\\n", i);\n'
             '  }',
     'hour': 0, 'minute': 0, 'second': 14},
    {'type': 'reveal', 'frameindex': 359, 'rowStart': 23, 'what': '  printf("--- Countdown 2 ---\\n");',
     'hour': 0, 'minute': 0, 'second': 14},
    {'type': 'reveal', 'frameindex': 364, 'rowStart': 25,
     'what': '  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }',
     'hour': 0, 'minute': 0, 'second': 14},
    {'type': 'add', 'what': '  printf("--- Countdown 3 ---\\n");\n  printCountdown(10);\n', 'rowStart': 30,
     'colStart': 1, 'begin-frameindex': 370, 'end-frameindex': 426,
     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 14, 'end-hour': 0, 'end-minute': 0, 'end-second': 18}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/Webcam2_long.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())
    def test_webcam2_Short(self):
        reset_main_program()
        expected = [    {'type': 'add', 'what':
                '#include <stdio.h>\n'
                '\n'
                'int main()\n'
                '{\n'
                '  printf("--- Countdown 1 ---\\n");\n'
                '\n'
                '  for (int i = 10; i >= 0; i--)\n'
                '  {\n'
                '    printf("%d\\n", i);\n'
                '  }\n'
                '\n'
                '\n'
                '  printf("--- Countdown 2 ---\\n");\n'
                '\n'
                '  for (int i = 10; i >= 0; i--)\n'
                '  {\n'
                '    printf("%d\\n", i);\n'
                '  }',
     'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1, 'end-frameindex': 241,
     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 10},
    {'type': 'add', 'what': '  return 0;\n}', 'rowStart': 20, 'colStart': 1, 'begin-frameindex': 247,
     'end-frameindex': 258,
     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 9, 'end-hour': 0, 'end-minute': 0, 'end-second': 11},
    {'type': 'reveal', 'frameindex': 287, 'rowStart': 3,
     'what': '\n'
             'void printCountdown(int start)\n'
             '{\n'
             '  for (; start >= 0; start--)\n'
             '  {\n'
             '    printf("%d\\n", start);\n'
             '  }\n'
             '}\n'
             '\n'
             '\n'
             'int main()\n'
             '{\n'
             '  printf("--- Countdown 1 ---\\n");\n'
             '\n'
             '  for (int i = 10; i >= 0; i--)\n'
             '  {\n'
             '    printf("%d\\n", i);\n'
             '  }',
     'hour': 0, 'minute': 0, 'second': 11},
    {'type': 'reveal', 'frameindex': 290, 'rowStart': 23, 'what': '  printf("--- Countdown 2 ---\\n");',
     'hour': 0, 'minute': 0, 'second': 11},
    {'type': 'reveal', 'frameindex': 295, 'rowStart': 25,
     'what': '  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }',
     'hour': 0, 'minute': 0, 'second': 11},
    {'type': 'add', 'what': '  printf("--- Countdown 3 ---\\n");\n  printCountdown(10);\n', 'rowStart': 30,
     'colStart': 1, 'begin-frameindex': 301, 'end-frameindex': 357,
     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 12, 'end-hour': 0, 'end-minute': 0, 'end-second': 15}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/Webcam2_short.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_Remove_more_lines(self):
        reset_main_program()
        expected = [{'type': 'reveal', 'rowStart': 3, 'what': '\nvoid printCountdown(int start)\n{\n  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {', 'frameindex': 0,
                     'hour': 0, 'minute': 0, 'second': 0},
{'type': 'remove', 'what': '\nvoid printCountdown(int start)\n{\n  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\n', 'begin-frameindex': 1, 'rowStart': 3, 'colStart': 1, 'end-frameindex': 104,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 5},
{'type': 'reveal', 'rowStart': 14, 'what': '    printf("%d\\n", i);\n  }\n\n\n  printf("--- Countdown 2 ---\\n");', 'frameindex': 10,
 'hour': 0, 'minute': 0, 'second': 0},
{'type': 'reveal', 'rowStart': 17, 'what': '\n  for (int i = 10; i >= 0; i--)', 'frameindex': 41,
 'hour': 0, 'minute': 0, 'second': 1},
{'type': 'reveal', 'rowStart': 17, 'what': '  {\n    printf("%d\\n", i);', 'frameindex': 73,
 'hour': 0, 'minute': 0, 'second': 2},
{'type': 'reveal', 'rowStart': 18, 'what': '  }', 'frameindex': 104,
 'hour': 0, 'minute': 0, 'second': 4},
{'type': 'reveal', 'rowStart': 20, 'what': '  return 0;\n}', 'frameindex': 107,
 'hour': 0, 'minute': 0, 'second': 4}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/RemoveMultipleLines.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())

    def test_WebcamAtBegin(self):
        reset_main_program()
        expected = [{'type': 'add', 'what': '#include <stdio.h>\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }\n\n\n  printf("--- Countdown 2 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);\n  }', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 87, 'end-frameindex': 327,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 3, 'end-hour': 0, 'end-minute': 0, 'end-second': 14},
{'type': 'add', 'what': '  return 0;\n}', 'rowStart': 20, 'colStart': 1, 'begin-frameindex': 333, 'end-frameindex': 344,
 'begin-hour': 0, 'begin-minute': 0, 'begin-second': 13, 'end-hour': 0, 'end-minute': 0, 'end-second': 14}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/webcam3.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())
        # SELECT
        expected = []

        self.assertEqual(expected, main.find_select_actions())


    def test_zoom(self):
        reset_main_program()

        expected = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/zoom.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        #self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(0, 'void printCountdown(unsigned start)\n{\n  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);',
        #                                                                      '{\n  for (; start >= 0; Start--)\n  {\n     printf("%d\\n",  Start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1  ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n', [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]))
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(1, main.get_text(0),
                                                                              main.get_text(1), [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]))

        expected = [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
        #self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(0, '{\n  for (; start >= 0; Start--)\n  {\n     printf("%d\\n",  Start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1  ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n',
        #                                                                      '  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {', [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]))
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(2,
                                                                              main.get_text(1),
                                                                              main.get_text(2),
                                                                              [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
                                                                               16, 17, 18]))

        expected = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        #self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(0, '  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {',
        #                                                                      '  {\n    printf("%d\\n",  Start);\n  }\n} \n\n\nint main()\n{\n  printf("--- Countdown 1  ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  ', [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]))
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(3,
                                                                              main.get_text(2),
                                                                              main.get_text(3),
                                                                              [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16,
                                                                               17, 18]))

        expected = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(4,
                                                                              main.get_text(3),
                                                                              main.get_text(4),
                                                                              [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]))

        expected = [8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(5,
                                                                              main.get_text(4),
                                                                              main.get_text(5),
                                                                              [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]))

        expected = [8, 9, 10, 11, 12, 13, 14, 15, 16]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(6,
                                                                              main.get_text(5),
                                                                              main.get_text(6),
                                                                              [8, 9, 10, 11, 12, 13, 14, 15, 16, 17]))

        expected = [9, 10, 11, 12, 13, 14, 15, 16]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(7,
                                                                              main.get_text(6),
                                                                              main.get_text(7),
                                                                              [8, 9, 10, 11, 12, 13, 14, 15, 16]))

        expected = [9, 10, 11, 12, 13, 14, 15, 16]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(8,
                                                                              main.get_text(7),
                                                                              main.get_text(8),
                                                                              [9, 10, 11, 12, 13, 14, 15, 16]))

        expected = [13, 14, 15, 16]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(9,
                                                                              main.get_text(8),
                                                                              main.get_text(9),
                                                                              [9, 10, 11, 12, 13, 14, 15, 16]))

        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(10,
                                                                              main.get_text(9),
                                                                              main.get_text(10),
                                                                              [13, 14, 15, 16]))

        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(11,
                                                                              main.get_text(10),
                                                                              main.get_text(11),
                                                                              [13, 14, 15]))

        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(12,
                                                                              main.get_text(11),
                                                                              main.get_text(12),
                                                                              [13, 14, 15]))
        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(13,
                                                                              main.get_text(12),
                                                                              main.get_text(13),
                                                                              [13, 14, 15]))
        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(14,
                                                                              main.get_text(13),
                                                                              main.get_text(14),
                                                                              [13, 14, 15]))
        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(15,
                                                                              main.get_text(14),
                                                                              main.get_text(15),
                                                                              [13, 14, 15]))
        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(16,
                                                                              main.get_text(15),
                                                                              main.get_text(16),
                                                                              [13, 14, 15]))
        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(17,
                                                                              main.get_text(16),
                                                                              main.get_text(17),
                                                                              [13, 14, 15]))

        expected = [13, 14, 15]
        self.assertEqual(expected, main.get_line_numbers_from_index_with_zoom(18,
                                                                              main.get_text(37),
                                                                              main.get_text(38),
                                                                              [13, 14, 15]))
        expected = [{'type': 'reveal', 'rowStart': 4,
                     'what': 'void printCountdown(unsigned start)\n{\n  for (; start >= 0; start--)\n  {\n    printf("%d\\n", start);\n  }\n}\n\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");\n\n  for (int i = 10; i >= 0; i--)\n  {\n    printf("%d\\n", i);',
                     'frameindex': 0,
                     'hour': 0, 'minute': 0, 'second': 0},
                    {'type': 'add', 'what': 'void', 'rowStart': 13, 'colStart': 12, 'begin-frameindex': 38,
                     'end-frameindex': 42,
                     'begin-hour': 0, 'begin-minute': 0, 'begin-second': 1, 'end-hour': 0, 'end-minute': 0, 'end-second': 2}]
        self.assertEqual(expected, main.find_add_remove_reveal_actions())

        # SELECT
        expected = []
        self.assertEqual(expected, main.find_select_actions())

    def test_MultipleAdds(self):
        reset_main_program()

        expected = [
            {'type': 'add', 'what': 'a\n\nc\nd', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 0,
             'end-frameindex': 10,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1},
            {'type': 'add', 'what': 'b', 'rowStart': 2, 'colStart': 1, 'begin-frameindex': 8, 'end-frameindex': 9,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1},
            {'type': 'add', 'what': 'bcd\nefg', 'rowStart': 1, 'colStart': 2, 'begin-frameindex': 14,
             'end-frameindex': 21,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1},
            {'type': 'add', 'what': 'd\nefghijklm', 'rowStart': 3, 'colStart': 2, 'begin-frameindex': 14,
             'end-frameindex': 25,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 1}
        ]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/MultipleTyping.mp4'
        args.check_interval = 1
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = None
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())

        # SELECT
        expected = []
        self.assertEqual(expected, main.find_select_actions())

    def test_NoExceed(self):
        reset_main_program()

        expected = [
            {'type': 'add', 'what': '#include <stdio.h>\n\nint main()\n{\n  printf("--- Countdown 1 ---\\n");', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1,
             'end-frameindex': 155,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 7}]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/TimeLimitNotExceeded.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = 5
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())

        # SELECT
        expected = []
        self.assertEqual(expected, main.find_select_actions())

    def test_Exceed(self):
        reset_main_program()

        expected = [
            {'type': 'add', 'what': '#include <stdio.h>\n\nint main()\n{\n  print', 'rowStart': 1, 'colStart': 1, 'begin-frameindex': 1,
             'end-frameindex': 41,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 0, 'end-hour': 0, 'end-minute': 0, 'end-second': 2},
            {'type': 'add', 'what': 'f("--- Countdown 1 ---\\n");', 'rowStart': 5, 'colStart': 8,
             'begin-frameindex': 200,
             'end-frameindex': 227,
             'begin-hour': 0, 'begin-minute': 0, 'begin-second': 8, 'end-hour': 0, 'end-minute': 0, 'end-second': 10}
        ]

        args = argparse.Namespace()
        args.name = 'Beispielvideos/TimeLimitExceeded.mp4'
        args.check_interval = None
        args.video_start_time = None
        args.video_end_time = None
        args.pause_in_seconds = 5
        args.time_accuracy_in_frames = 1
        args.crop_area_left = None
        args.crop_area_right = None
        args.crop_area_top = None
        args.crop_area_bottom = None

        main.REF_IMAGES_FOLDER = 'RefImagesTwo'
        main.ARGS = args
        main.parse_args()
        self.assertEqual(expected, main.find_add_remove_reveal_actions())

        # SELECT
        expected = []
        self.assertEqual(expected, main.find_select_actions())