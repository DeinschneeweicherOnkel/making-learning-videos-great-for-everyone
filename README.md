# A project to determine, analyse and log Actions of a text-editor in a video.

**Actions** are classifiable into `add` text, `remove` text, `reveal` text, `select` text and `unselect` text.

Also scroll-, zoom- and webcam-recognition is supported.

Make sure a reference-folder with corresponding font-reference-images to the font of the text used in desired video is chosen in `main`-function.

For further details, all `.py`-files of the root folder are documented by comments.

**With `python main_program.py --h` a help-text with basic information about the program and its possible commandline-arguments (with detailed description) can be displayed.**

## Files and Folders

### main_program.py
The main analysis-program itself.

### generate_ref_images.py
With this program it is possible to produce reference-images which later are used for character-/text-recognition.
For further information look at the comments inside of `generate_ref_images.py`.

### test_suite.py
File with unit-testcases to test main_program.py.

### video_information_dummy-lesson.json
Example-analysis of `Beispielvideos/dummy-lesson-merged.mp4` with the following program-call:

`python main_program.py --name=Beispielvideos/dummy-lesson-merged.mp4 --video-property-filename=dummy-lesson-merged.mp4 --video-property-title="Kinda best memevideo ever" --action-property-hint="Check out @Badudu1 and @create_with_code on YouTube"`.

### /Beispielvideos
Contains a few videos as examples to demonstrate the functionality of the program (also used by the test_suite.py).

### /RefImagesOne and /RefImagesTwo
Each contains reference-images of a certain font.
These are needed to make the character-/text-recognition at all.
One can generate them with `generate_ref_images.py`.

### /old_2022_version
Contains the first version of this program/work/thesis (from January 2022).
Mainly has personal nostalgic reasons that it is here.

## Examples of usage
**With `python main_program.py --h` a help-text with basic information about the program and further commandline-arguments (with detailed description) can be displayed.**

---

`python main_program.py --name="Beispielvideos/dummy-lesson-merged.mp4"`

Executes the analysis-program for video `dummy-lesson-merged.mp4` with default parameters.

---

`python main_program.py --name="Beispielvideos/zoom.mp4" --video-start-time=2 --video-end-time=4`

Executes the analysis-program for video `zoom.mp4` but just runs it from video-second 2 until video-second 4.

---

`python main_program.py --name="Beispielvideos/zoom.mp4" --crop-area-left=2 --crop-area-right=3 --crop-area-top=4 --crop-area-bottom=5`

Executes the analysis-program for video `zoom.mp4` but crops left side by 2 pixel, right side by 3 pixel, top by 4 pixel and bottom by 5 pixel.

---

`python main_program.py --name="Beispielvideos/zoom.mp4" --check-interval=1`

Executes the analysis-program for video `zoom.mp4` with an interval of 1 which means that every frame is analysed.
`--check-interval=2` would check every second frame of video.
